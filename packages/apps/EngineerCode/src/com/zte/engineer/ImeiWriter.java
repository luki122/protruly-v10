
package com.zte.engineer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncResult;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemProperties;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;
import com.mediatek.telephony.TelephonyManagerEx;

public class ImeiWriter extends Activity implements OnClickListener {
    private static final String TAG = "ImeiWriter";

    // { Please do not modified the block values.
    private static final int IMEI_LENGTH = 15;

    // refer to revision 3.3.25
    private static final String OP_READ = "0";
    private static final String OP_WRITE = "1";
    private static final String TYPE_SIM1 = "7";
    private static final String TYPE_SIM2 = "10";
    private static final String TYPE_SIM3 = "11";
    private static final String TYPE_SIM4 = "12";
    // }

    private Button btnSIM1, btnSIM2 = null;
    private EditText etSIM1, etSIM2 = null;
    private ProgressDialog mProgressDlg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.write_imei);

        initUI();

        loadCurrentIMEI();
    }

    private void loadCurrentIMEI() {
        // Read the current imei. But do not take effect now.
        //new IMEIOperateTask().execute(OP_READ, "0");
        //new IMEIOperateTask().execute(OP_READ, "1");

        if ("true".equals(SystemProperties.get("ro.mediatek.gemini_support"))) {
            final String mIMEI1 = TelephonyManagerEx.getDefault().getDeviceId(PhoneConstants.SIM_ID_1);
            final String mIMEI2 = TelephonyManagerEx.getDefault().getDeviceId(PhoneConstants.SIM_ID_2);
            
            etSIM1.setText(mIMEI1);
            etSIM2.setText(mIMEI2);
        } else { // Single sim card phone.
            TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
            final String mIMEI = telephonyManager.getDeviceId();
            etSIM1.setText(mIMEI);
        }
    }

    private void initUI() {
        String simNum = SystemProperties.get("persist.gemini.sim_num", "");
        Log.d(TAG, "simNum = " + simNum);
        btnSIM1 = (Button) findViewById(R.id.bt1);
        btnSIM2 = (Button) findViewById(R.id.bt2);

        etSIM1 = (EditText) findViewById(R.id.et1);
        etSIM2 = (EditText) findViewById(R.id.et2);

        btnSIM1.setEnabled(false);
        btnSIM2.setEnabled(false);
        btnSIM1.setOnClickListener(this);
        btnSIM2.setOnClickListener(this);

        if (mProgressDlg == null) {
            mProgressDlg = new ProgressDialog(this);
            mProgressDlg.setCancelable(false);
            mProgressDlg.setCanceledOnTouchOutside(false);
        }

        if (!("true".equals(SystemProperties.get("ro.mediatek.gemini_support")))) {
            etSIM2.setVisibility(View.GONE);
            btnSIM2.setVisibility(View.GONE);
        }

        etSIM1.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView arg0, int arg1, KeyEvent arg2) {
                return false;
            }

        });

        etSIM2.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView arg0, int arg1, KeyEvent arg2) {
                return false;
            }

        });

        etSIM1.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                if (arg0 != null && arg0.toString().length() == IMEI_LENGTH) {
                    btnSIM1.setEnabled(true);
                } else {
                    btnSIM1.setEnabled(false);
                }
            }
        });

        etSIM2.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                if (arg0 != null && arg0.toString().length() == IMEI_LENGTH) {
                    btnSIM2.setEnabled(true);
                } else {
                    btnSIM2.setEnabled(false);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt1:
                Log.e(TAG, "bt1 click...");
                String subStr1 = etSIM1.getText().toString();
                Log.e(TAG, "subStr1 = " + subStr1);
                //putExtra("IMEI", subStr1);
                //putExtra("SIM_ID", 0); // sim 1
                //if (isTaskIdle) {
                    new IMEIOperateTask().execute(OP_WRITE, "0", subStr1);
                /*} else {
                    Toast.makeText(this, R.string.imei_op_task_busy
                            , Toast.LENGTH_SHORT).show();
                }*/
                break;

            case R.id.bt2:
                Log.e(TAG, "bt2 click...");
                String subStr2 = etSIM2.getText().toString();
                //Log.e("gqt124", "subStr = " + subStr2);
                Log.e(TAG, "subStr2 = " + subStr2);
                //intent2.putExtra("IMEI", subStr2);
                //intent2.putExtra("SIM_ID", 1); // sim 2
                //if (isTaskIdle) {
                    new IMEIOperateTask().execute(OP_WRITE, "1", subStr2);
                //}
                break;

            default:
                break;
        }
    }

    private static final int EVENT_CHECK_EGMR = 0x1001;
    private static final int EVENT_READ_IMEI = 0x1009;
    private static final int EVENT_WRITE_IMEI = 0x1010;
    private static final int FAIL_NOT_SUPPORT_EGMR = 0x3030;
    private static final int FAIL_OTHER_RESON = 0x3031;
    private boolean isCheckingEGMR = false;
    private boolean isSupportEGMR = false;
    private boolean is3GSwitching = false; // TODO, set FAQ08662

    //private Object mLockObj = new Object();
    private  Handler mResponseHander = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            AsyncResult ar = (AsyncResult) msg.obj;
            switch (msg.what) {
                case EVENT_CHECK_EGMR:
                    Log.d(TAG, "handleMessage() EVENT_CHECK_EGMR, ar=" + ar);
                    if (ar != null && ar.exception == null) {
                        isSupportEGMR = true;
                    } else {
                        isSupportEGMR = false;
                    }
                    //Log.d(TAG, "[handleMessage] checkCmdArray[1]=" + checkCmdArray[1]);
                    isCheckingEGMR = false;
                    break;
                case EVENT_READ_IMEI:
                    Log.w(TAG, "handleMessage() EVENT_READ_IMEI, ar=" + ar);
                    //Log.d(TAG, "[handleMessage] readCmdArray[1]=" + readCmdArray[1]);
                    if (ar != null && ar.exception == null) {
                        Toast.makeText(ImeiWriter.this
                                , R.string.read_imei_finished,Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ImeiWriter.this
                                , R.string.read_imei_failed,Toast.LENGTH_SHORT).show();
                    }
                    break;
                case EVENT_WRITE_IMEI:
                    Log.w(TAG, "handleMessage() EVENT_WRITE_IMEI, ar=" + ar);
                    //Log.d(TAG, "[handleMessage] writeCmdArray[1]=" + writeCmdArray[1]);
                    if (ar != null && ar.exception == null) {
                        Toast.makeText(ImeiWriter.this
                                , R.string.write_imei_finished, Toast.LENGTH_SHORT).show();
                        // read again.
                        /*new IMEIOperateTask().execute(OP_READ
                                , String.valueOf(msg.arg2));*/

                        /*AlertDialog.Builder dialog = new Builder(ImeiWriter.this);
                        dialog.setTitle(R.string.written_success_title);
                        dialog.setMessage(R.string.written_success_msg);
                        dialog.setNegativeButton(R.string.set_imei_ok, null);
                        dialog.create().show();*/
                    } else {
                        Toast.makeText(ImeiWriter.this
                                , R.string.write_imei_failed, Toast.LENGTH_SHORT).show();
                    }
                    break;
                case FAIL_NOT_SUPPORT_EGMR:
                case FAIL_OTHER_RESON:
                    Toast.makeText(ImeiWriter.this,
                            R.string.imei_operation_failed,Toast.LENGTH_SHORT).show();
                    break;
                default:
                    break;
            }
        }
    };

    private class IMEIOperateTask extends AsyncTask<String, Void, Message> {

        /**
         * Return the IMEI string of the sim, or null.
         * @param simIndex
         * from 0 to 3.
         */
        private Message readIMEI(int simIndex) {
            String cmd = "AT+EGMR=";
            final String op = OP_READ;
            final String type;
            cmd += op + ",";
            if (simIndex == 0) {
                type = TYPE_SIM1;
            } else if (simIndex == 1) {
                type = TYPE_SIM2;
            } else if (simIndex == 2) {
                type = TYPE_SIM3;
            } else if (simIndex == 3) {
                type = TYPE_SIM4;
            } else {
                throw new RuntimeException("Wrong sim index, please use 0-3");
            }
            cmd += type;
            Log.d(TAG, "[readIMEI] cmd [" + cmd + "]");
            String [] readCmdArray = new String[2];
            readCmdArray[0] = cmd;
            readCmdArray[1] = "";

            Message msg = mResponseHander.obtainMessage(EVENT_READ_IMEI);
            msg.arg1 = Integer.parseInt(op);
            msg.arg2 = simIndex;
            PhoneFactory.getPhone(simIndex).invokeOemRilRequestStrings(
                    readCmdArray, msg);
            return msg;
        }

        /**
         * Return the IMEI string of the sim, or null.
         * @param simIndex
         * from 0 to 3.
         */
        private Message writeIMEI(int simIndex, String newImei) {
            for(int i = 0; i < newImei.length(); i++) {
                if (!Character.isDigit(newImei.charAt(i))) {
                    throw new RuntimeException("Wrong imei string");
                }
            }

            //String [] cmdStr = {"AT+EGMR=1,", "+EGMR"};
            //imeiString[0] = "AT+EGMR=1,7,\"" + subStr + "\"";\r
            String cmd = "AT+EGMR=";
            final String op = OP_WRITE;
            final String type;
            cmd += op + ",";
            if (simIndex == 0) {
                type = TYPE_SIM1;
            } else if (simIndex == 1) {
                type = TYPE_SIM2;
            } else if (simIndex == 2) {
                type = TYPE_SIM3;
            } else if (simIndex == 3) {
                type = TYPE_SIM4;
            } else {
                throw new RuntimeException("Wrong sim index, please use 0-3");
            }
            cmd += type;
            cmd += "," + "\"" + newImei + "\"";
            Log.d(TAG, "[writeIMEI] cmd [" + cmd + "]");
            String [] writeCmdArray = new String[2];
            writeCmdArray[0] = cmd;
            writeCmdArray[1] = "";

            Message msg = mResponseHander.obtainMessage(EVENT_WRITE_IMEI);
            msg.arg1 = Integer.parseInt(op);
            msg.arg2 = simIndex;

            PhoneFactory.getPhone(simIndex).invokeOemRilRequestStrings(
                    writeCmdArray, msg);
            return msg;
        }

        private void checkEGMRSupported(int simIndex) {
            isCheckingEGMR = true;
            String [] checkCmdArray = new String[2];
            checkCmdArray[0] = "AT+EGMR=?";
            checkCmdArray[1] = "";
            PhoneFactory.getPhone(simIndex).invokeOemRilRequestStrings(
                    checkCmdArray, mResponseHander.obtainMessage(EVENT_CHECK_EGMR));
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mProgressDlg != null) {
                mProgressDlg.show();
            }
            //isTaskIdle = false;
        }

        @Override
        protected Message doInBackground(String... params) {
            // Check if the commands is supported.
            final int operator = Integer.parseInt(params[0]);
            final int simIndex = Integer.parseInt(params[1]);
            Log.d(TAG, "doInBackground() params :" + params);
            if (!isSupportEGMR) { // I only check at the first time in the page.
                checkEGMRSupported(simIndex);
            }
            Log.d(TAG, "doInBackground(), isCheckingEGMR="
                    + isCheckingEGMR);
            while(isCheckingEGMR) {
                try {
                    Thread.currentThread().sleep(60);
                } catch (InterruptedException e) {
                    Log.e(TAG, "IMEIOperateTask meet exception!!", e);
                }
            }
            if (isCancelled()) {
                isSupportEGMR = false;
                return null;
            }
            if (!isSupportEGMR) {
                Message msg = new Message();
                msg.what = FAIL_NOT_SUPPORT_EGMR;
                return msg;
            }

            // Run the commands.
            Message msg = null;
            if (operator == 0) { // read
                msg = readIMEI(simIndex);
            } else if (operator == 1) { // write
                final String newImei = params[2];
                msg = writeIMEI(simIndex, newImei);
            }
            return msg;
        }

        @Override
        protected void onCancelled() {
            isCheckingEGMR = false;
            super.onCancelled();
            if (mProgressDlg != null && mProgressDlg.isShowing()) {
                mProgressDlg.dismiss();
            }
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Message result) {
            if (result == null) {
                // on error
                Log.d(TAG, "onPostExecute() result is null!");
                // Is cancelled or invalid result.
                Message msg = mResponseHander.obtainMessage(FAIL_OTHER_RESON);
                msg.sendToTarget();
            } else if (result.what == FAIL_NOT_SUPPORT_EGMR) { // not support cmd.
                // Error not support EGMR
                Log.d(TAG, "onPostExecute(), fail because not support EGMR cmd!");
                mResponseHander.handleMessage(result);
            } else {
                Log.d(TAG, "onPostExecute(), begin to handle EGMR cmd result!");
                //mResponseHander.handleMessage(result);
            }
            if (mProgressDlg != null && mProgressDlg.isShowing()) {
                mProgressDlg.dismiss();
            }
            //isTaskIdle = true;
            super.onPostExecute(result);
        }
    }

    

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause...");
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy...");
        if (mProgressDlg != null) {
            if (mProgressDlg.isShowing()) {
                mProgressDlg.dismiss();
            }
            mProgressDlg = null;
        }
        super.onDestroy();
    }
}
