/*
 * Copyright (C) 2016 Fourier Semiconductor Inc.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <signal.h>

#if !(defined(WIN32) || defined(_X64))
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#else

#ifndef UNICODE
#define UNICODE 1
#endif
// link with Ws2_32.lib
#pragma comment(lib,"Ws2_32.lib")

#include <winsock2.h>    /*  sockets */
#include <windows.h>
#include <ws2tcpip.h>

#ifndef INET6_ADDRSTRLEN
#define INET6_ADDRSTRLEN 46
#endif

#define SHUT_RD   SD_RECEIVE 
#define SHUT_WR   SD_SEND 
#define SHUT_RDWR SD_BOTH 

#define _FIONREAD FIONREAD

#endif

#define MAXSOCKET_BACKLOG 3

static int g_socketListen = -1;
static int g_socketActive = -1;

typedef void (*sighandler_t)(int);
    
void FsSocketExit() {
    struct linger l;
    l.l_onoff  = 1;
    l.l_linger = 0;

    if(g_socketListen > 0) {
        shutdown(g_socketListen, SHUT_RDWR);
    }

    if(g_socketActive > 0) {
        setsockopt(g_socketActive, SOL_SOCKET, SO_LINGER, &l, sizeof(l));
        shutdown(g_socketActive, SHUT_RDWR);
        close(g_socketActive);
        g_socketActive = -1;
    }
    _exit(0);
}

static void SocketSigHandler(int sig) {
    (void) signal(SIGINT, SIG_DFL);
    FsSocketExit();
}

static void SocketExitHandler() {
    FsSocketExit();
}

int SocketClientInit(char *pServer) {
    char *pHost;
    char *pPort;
    struct sockaddr_in sin;
    struct hostent *host;
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    int port;
    
    if (pServer==0) {
       FsSocketExit();
    }

    pPort = strchr ( pServer , ':');
    if (pPort == NULL)
    {
       fprintf (stderr, "Invalid servername: %s, should be host:port\n", pServer);
       return -1;
    }
    pHost=pServer;
    *pPort ='\0'; //terminate
    pPort++;
    port=atoi(pPort);

    host = gethostbyname(pHost);
    if(!host) {
        fprintf (stderr, "Invalid host name: %s\n", pHost);
        return -1;
    }

    if(port == 0) {
        fprintf (stderr, "Invalid port number.\n");
        return -1;
    }

    memcpy(&sin.sin_addr.s_addr, host->h_addr, host->h_length);
    sin.sin_family = AF_INET;
    sin.sin_port = htons(port);

    if (connect(sockfd, (struct sockaddr *)&sin, sizeof(sin)) < 0)
    {
        fprintf(stderr,"fail to connect %s:%d\n", pHost, port);
        return -1;
    }

    atexit(SocketExitHandler);
    (void) signal(SIGINT, SocketSigHandler);

    return sockfd;
    
}

int SocketServerListen(char *serverPort) {
    int port;
    char strHost[64];
    char strClient[INET6_ADDRSTRLEN];

    struct sockaddr_in serverAddr;
    struct sockaddr_in clientAddr;
    socklen_t clientAddrLen;

    port = atoi(serverPort);
    if(port == 0) {
        fprintf (stderr, "Invalid port number.\n");
        return -1;
    }

    if(-1 == gethostname(strHost, sizeof(strHost))) {
        fprintf (stderr, "Error, gethostname.\n");
        return -1;
    }

    atexit(SocketExitHandler);
    (void) signal(SIGINT, SocketSigHandler);

    memset(&serverAddr, 0, sizeof(serverAddr));
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(port);

    serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);

    fprintf(stdout, "Listening to %s:%d\n", strHost, port);

    g_socketListen = socket(AF_INET, SOCK_STREAM, 0);
    if(g_socketListen == -1){
        fprintf(stderr, "Failed to create listen socket\n");
        return -1;
    }

    if(bind(g_socketListen, (struct sockaddr*) &serverAddr, sizeof(serverAddr)) == -1){
        fprintf(stderr, "Bind error\n");
        return -1;
    }

    if(listen(g_socketListen, MAXSOCKET_BACKLOG) == -1){
        fprintf(stderr, "Listen error\n");
        return -1;
    }

    clientAddrLen = sizeof(clientAddr);
    g_socketActive = accept(g_socketListen, (struct sockaddr*) &clientAddr, &clientAddrLen);

    inet_ntop(AF_INET, &clientAddr.sin_addr.s_addr, strClient, sizeof(strClient));
    printf("Received connection from client %s\n", strClient);

    close(g_socketListen);

    return (g_socketActive);
}

