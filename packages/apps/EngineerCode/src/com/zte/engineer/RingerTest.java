
package com.zte.engineer;

import java.io.IOException;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;
import android.widget.TextView;
import android.media.RingtoneManager;
import android.view.KeyEvent;
import android.provider.Settings;
public class RingerTest extends ZteActivity {
    private static final String TAG = "RingerTest";
    private MediaPlayer mMediaP = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // hide title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.singlebuttonview);

        TextView mTextView = (TextView) findViewById(R.id.singlebutton_textview);
        mTextView.setText(R.string.ringer);

        ((Button) findViewById(R.id.btnPass)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnFail)).setOnClickListener(this);
        Button player = (Button) findViewById(R.id.singlebutton_play_button);
		player.setText(R.string.play_ringer);
        player.setVisibility(View.VISIBLE);
        ((Button) findViewById(R.id.btnPass)).setEnabled(false);
        player.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
		Settings.System.putInt(getContentResolver(), "AEON_KEY_TEST", 1);
        mMediaP = MediaPlayer.create(RingerTest.this, R.raw.backroad);
        mMediaP.setOnErrorListener(new OnErrorListener() {
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.e(TAG, "Error occurred while playing audio.");
                mp.stop();
                mp.release();
                return true;
            }
        });

        /*
         * new Thread() { public void run() { Log.d(TAG, "public void run()");
         * try { mMediaP.prepare(); Log.d(TAG, "mMediaP.prepare()"); } catch
         * (IllegalStateException e) { // TODO Auto-generated catch block
         * e.printStackTrace(); } catch (IOException e) { // TODO Auto-generated
         * catch block e.printStackTrace(); } mMediaP.setLooping(true);
         * mMediaP.start(); Log.d(TAG, "mMediaP.start()"); } }.start();
         */
    }

    @Override
    protected void onPause() {
        super.onPause();
		Settings.System.putInt(getContentResolver(), "AEON_KEY_TEST", 0);
        if (mMediaP != null) {
            // mMediaP.reset();
            mMediaP.stop();
            mMediaP.release();
        }
        Log.d(TAG, "onPause");
    }

    private void playMusic() {
        Log.e("yuanwenchao", "Enter palyMusic ");
        String externalSdPath = null;
        String externalSdPath2 = null;
        if (Build.VERSION.SDK_INT == 23) {
            externalSdPath = "/mnt/m_external_sd";
            externalSdPath2 = "/mnt/m_internal_storage";
        } else { // KK/L/L1/M
            externalSdPath = "/mnt/sdcard2";
            externalSdPath2 = "/mnt/sdcard";
        }
        String testFileName = "test.mp3";

        try {
            mMediaP.reset();
            mMediaP.setDataSource(externalSdPath
                    + "/" + testFileName);
            mMediaP.prepare();
            mMediaP.start();
            ((Button) findViewById(R.id.btnPass)).setEnabled(true);
            // Log.e("yuanwenchao","mMediaP.getAudioSessionId() = "+mMediaP.getAudioSessionId());
        } catch (Exception e) {
            Log.w(TAG, "Have not found test.mp3 in path:" + externalSdPath);
            e.printStackTrace();
			try{
				mMediaP.reset();
				mMediaP.setDataSource(this,
                    RingtoneManager.getActualDefaultRingtoneUri(this, RingtoneManager.TYPE_RINGTONE));
				mMediaP.prepare();
				mMediaP.start();
				((Button)findViewById(R.id.btnPass)).setEnabled(true);
			}catch(Exception exception){
           
				((Button) findViewById(R.id.btnPass)).setEnabled(false);
			}
            e.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        if (mMediaP != null) {
            mMediaP.release();
        }
        Log.d(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public void finishSelf(int result) {
        super.finishSelf(result);
    }

    @Override
    public void onClick(View arg0) {
        switch (arg0.getId()) {
            case R.id.btnPass:
                finishSelf(RESULT_PASS);
                break;
            case R.id.btnFail:
                finishSelf(RESULT_FAIL);
                break;
            case R.id.singlebutton_play_button:
                playMusic();
                // ((Button)findViewById(R.id.btnPass)).setEnabled(true);
                break;
            default:
                finishSelf(RESULT_PASS);
                break;
        }
    }

}
