1. 代码移植请参照同目录下的 porting_guide.txt 文件进行修改，移植时注意使用最新的版本。

2. 请注意不要将客户的特定命令修改到ZteEngineerCode.java中，请仅修改到它在Dialer的拷贝中。

3. ZteEngineerCode.java 中定义了基本的测试指令。

4. AutoTest.java是自动测试的入口类，EngineerCode.java 是手动测试的入口类。

5. 若某个测试项有问题，而且它跟驱动相关，请跟驱动同事检查驱动是否已具备相关功能，如红外、霍尔。

6. 每次比较重要的提交请修改 history.txt和 android:versionCode，android:versionName 
    并记录对应的修改情况。

7. 可移植性：当前主要适配的是安卓 MTK平台5.0,5.1,6.0，目标是在尽量少修改此工程代码的情况下能移植。