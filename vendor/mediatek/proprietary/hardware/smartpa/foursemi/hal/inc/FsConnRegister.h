/* 
* Copyright (C) 2016 Fourier Semiconductor Inc. 
*
*/
#ifndef FSCONNREGISER_H
#define FSCONNREGISER_H

#ifdef __cplusplus
extern "C" {
#endif

// Register connection
int FsConnRegister(char* strTarget);

// Get global active fd
int FsConnGetFd();

// Get active device string
char* FsConnGetDeviceStr();

#if !( defined(WIN32) || defined(_X64) )
/******************* I2C interface for Android ********************/
int __FsI2CWriteRead(int fd, int nWriteBytes, const uint8_t* writeData,
                                    int nReadBytes, uint8_t *readData, unsigned int *pError);
int __FsI2CWrite(int fd, int size, uint8_t *buffer, unsigned int *pError);
int __FsI2CInit(char *devpath);
int __FsI2cVersion(char *buffer, int fd);

#else
/******************* I2C interface for Windows platform ********************/
#define uint8_t unsigned char

int __FsWindowsI2CWriteRead(int fd, int nWriteBytes, const uint8_t* writeData,
                                    int nReadBytes, uint8_t *readData, unsigned int *pError);

int __FsWindowsI2CWrite(int fd, int size, uint8_t *buffer, unsigned int *pError);

int __FsWindowsI2CInit(char *devpath);

int __FsWindowsI2cVersion(char *buffer, int fd);
#endif


#ifdef __cplusplus
}
#endif

#endif
