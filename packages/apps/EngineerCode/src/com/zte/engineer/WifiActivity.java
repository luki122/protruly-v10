
package com.zte.engineer;

import java.util.List;

import android.app.Activity;
import android.net.wifi.ScanResult;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ListView;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import android.widget.SimpleAdapter;
import android.util.Log;
import android.content.Intent;
import android.net.wifi.WifiConfiguration;
import android.widget.AdapterView.OnItemClickListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.EditText;
import android.os.Handler;

public class WifiActivity extends Activity implements OnItemClickListener, OnClickListener {
    /** Called when the activity is first created. */
    private TextView allNetWork;
    private TextView wifistatus = null;
    private Button scan;
    private Button start;
    private Button stop;
    private ListView listview = null;
    private List<Map<String, Object>> listItems;
    // private Map<String,Object> listItem;
    private Button check;
    private WifiAdmin mWifiAdmin;
    private Handler handDelay;
    private List<ScanResult> list;
    private ScanResult mScanResult;
    LinearLayout passwordLayout;
    private StringBuffer sb = new StringBuffer();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wifi_test_new);
        mWifiAdmin = new WifiAdmin(WifiActivity.this);

        init();
    }

    public void init() {
        allNetWork = (TextView) findViewById(R.id.allNetWork);
        scan = (Button) findViewById(R.id.scan);
        start = (Button) findViewById(R.id.start);
        stop = (Button) findViewById(R.id.stop);
        check = (Button) findViewById(R.id.check);
        scan.setOnClickListener(this);
        start.setOnClickListener(this);
        stop.setOnClickListener(this);
        check.setOnClickListener(this);
        handDelay = new Handler();
        handDelay.postDelayed(run, 1000);

    }

    Runnable run = new Runnable() {
        public void run() {
            getAllNetWorkList();
        }
    };

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.scan:
                getAllNetWorkList();
                break;
            case R.id.start:
                mWifiAdmin.openWifi();
                Toast.makeText(WifiActivity.this,
                        getString(R.string.tips_wifi_status) + mWifiAdmin.checkState()
                        , Toast.LENGTH_LONG).show();
                break;
            case R.id.stop:
                mWifiAdmin.closeWifi();
                Toast.makeText(WifiActivity.this,
                        getString(R.string.tips_wifi_status) + mWifiAdmin.checkState()
                        , Toast.LENGTH_LONG).show();
                break;
            case R.id.check:
                Intent intent = new Intent("android.settings.WIFI_SETTINGS");
                WifiActivity.this.startActivity(intent);
                /*
                 * mWifiAdmin.startScan(); list=mWifiAdmin.getWifiList();
                 * if(listview == null) listview =
                 * (ListView)findViewById(R.id.showAllNetWort); if(wifistatus ==
                 * null) wifistatus = (TextView) findViewById(R.id.wifistatus);
                 * listview.setVisibility(View.GONE);
                 * wifistatus.setVisibility(View.VISIBLE);
                 * allNetWork.setText("The status of the WiFi network : \n");
                 * wifistatus.setText("WIFI is already " +
                 * mWifiAdmin.checkState(
                 * )+" \n "+"The search to the "+list.size()+" WiFi" );
                 */
                // Toast.makeText(WifiActivity.this,
                // "The WiFi status is : "+mWifiAdmin.checkState(), 1).show();
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
        Log.e("yuanwenchao", "Enter here");
        // mWifiAdmin.getWifiList().get(position).capabilities.contains();
        passwordLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.wifi_input_password,
                null);
        final EditText password = (EditText) passwordLayout.findViewById(R.id.password);

        new AlertDialog.Builder(this)
                .setIcon(R.drawable.miaomiao)
                .setTitle("please input password !")
                .setView(passwordLayout)
                .setPositiveButton("finish", new android.content.DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mWifiAdmin.connetionWifiConfiguration(position, password.getText()
                                .toString().trim());
                        Log.e("yuanwenchao", "position = " + position + "  password.toString() = "
                                + password.getText().toString().trim());
                    }
                })
                .setNegativeButton("cancle", new android.content.DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub

                    }
                }).create().show();

    }

    public void getAllNetWorkList() {

        if (listview == null) {
            listview = (ListView) findViewById(R.id.showAllNetWort);
            listview.setOnItemClickListener(this);
        }

        if (wifistatus == null)
            wifistatus = (TextView) findViewById(R.id.wifistatus);

        /*
         * if(sb!=null){ sb=new StringBuffer(); }
         */

        mWifiAdmin.startScan();
        Log.e("yuanwenchao", "mWifiAdmin.getConfiguration = " + mWifiAdmin.getConfiguration());

        list = mWifiAdmin.getWifiList();
        listItems = new ArrayList<Map<String, Object>>();
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {

                mScanResult = list.get(i);
                Map<String, Object> listItem = new HashMap<String, Object>();
                listItem.put("bssid", "(" + mScanResult.BSSID + ")");
                listItem.put("ssid", mScanResult.SSID);
                listItem.put("capabilities", mScanResult.capabilities);
                listItem.put("frequency", mScanResult.frequency + " MHz");
                if (-40 <= mScanResult.level)
                    listItem.put("image", R.drawable.ic_qs_wifi_4);
                else if (-65 <= mScanResult.level && -40 >= mScanResult.level)
                    listItem.put("image", R.drawable.ic_qs_wifi_3);
                else if (-95 <= mScanResult.level && -65 >= mScanResult.level)
                    listItem.put("image", R.drawable.ic_qs_wifi_2);
                else
                    listItem.put("image", R.drawable.ic_qs_wifi_1);
                listItem.put("level", mScanResult.level + " dBm");
                listItems.add(listItem);

                /*
                 * sb=sb.append(mScanResult.BSSID+"  ").append(mScanResult.SSID+
                 * "   ")
                 * .append(mScanResult.capabilities+"   ").append(mScanResult
                 * .frequency+"   ") .append(mScanResult.level+"\n\n");
                 */
            }
            Log.e("yuanwenchao", "listItems.size() = " + listItems.size());

            SimpleAdapter simpleAdapter = new SimpleAdapter(this, listItems, R.layout.simple_item,
                    new String[] {
                            "bssid", "ssid", "capabilities", "frequency", "image", "level"
                    }, new int[] {
                            R.id.wifi_bssid, R.id.wifi_ssid, R.id.wifi_capabilities,
                            R.id.wifi_frequency, R.id.image, R.id.wifi_level
                    });
            listview.setVisibility(View.VISIBLE);
            wifistatus.setVisibility(View.GONE);
            listview.setAdapter(simpleAdapter);
            allNetWork.setText(R.string.msg_scan_wifi);
            // allNetWork.setText("Scan to WiFi network : \n"+sb.toString());
        } else {
            allNetWork.setText(R.string.msg_wifi_not_found);
        }
    }
}
