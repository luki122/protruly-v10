LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_CFLAGS := -DLOG_TAG="\"AEE/EXT\""

LOCAL_SRC_FILES:= aee_ext.c

LOCAL_SHARED_LIBRARIES := liblog

LOCAL_MODULE = libaed_ext
include $(BUILD_SHARED_LIBRARY)