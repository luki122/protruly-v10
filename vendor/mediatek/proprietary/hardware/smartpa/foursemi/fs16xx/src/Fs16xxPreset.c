/* 
* Copyright (C) 2016 Fourier Semiconductor Inc. 
*
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "FsPrint.h"
#include "Fs16xx.h"
#include "Fs16xx_regs.h"
#include "Fs16xx_Custom.h"
#include "Fs16xxPreset.h"

static Fs16xx_preset gFs16xxPresets[(int)fs16xx_preset_type_max];
static char const *gFs16xxPresetName[(int)fs16xx_preset_type_max] = {
    "fs16xx_music.preset",
    "fs16xx_voice.preset",
    "fs16xx_ringtone.preset",
};

#define CRC16_TABLE_SIZE 256
const unsigned short gCrc16Polynomial = 0xA001;
static unsigned short gCrc16Table[CRC16_TABLE_SIZE];

#define PRESET_VALID    1
#define PRESET_INVALID    0

/* Initialize presets should only be called once in fs16xx_init
*/
Fs16xx_Error_t fs16xx_initialize_presets() {
    unsigned short value;
    unsigned short temp, i;
    unsigned char j;
    memset(gFs16xxPresets, 0, sizeof(Fs16xx_preset) * (int)fs16xx_preset_type_max);

    // Initialize CRC16 table
    for (i = 0; i < CRC16_TABLE_SIZE; ++i)
    {
        value = 0;
        temp = i;
        for (j = 0; j < 8; ++j)
        {
            if (((value ^ temp) & 0x0001) != 0)
            {
                value = (unsigned short)((value >> 1) ^ gCrc16Polynomial);
            }
            else
            {
                value >>= 1;
            }
            temp >>= 1;
        }
        gCrc16Table[i] = value;
    }
    return Fs16xx_Error_OK;
}

Fs16xx_Error_t fs16xx_deinitialize_presets() {
    int i;
    for(i = 0; i < (int)fs16xx_preset_type_max; i++) {
        if(PRESET_VALID == gFs16xxPresets[i].valid) {
            if(gFs16xxPresets[i].preset_header.data) {
                gFs16xxPresets[i].valid = PRESET_INVALID;
                free(gFs16xxPresets[i].preset_header.data);
            }
        }
    }

    return Fs16xx_Error_OK;
}

Fs16xx_Error_t fs16xx_load_presets() {
    FILE *fp;
    int i, count, data_len, buf_size;
    char preset_name[PRESET_NAME_SIZE];
    Fs16xx_Error_t err = Fs16xx_Error_OK;
    unsigned short *ptr_data;
    unsigned short checksum;

    // load_presets should only be called once after initialization
    for(i = 0; i < (int)fs16xx_preset_type_max; i++) {
        if(gFs16xxPresets[i].valid) {
            PRINT_ERROR("%s invalid status, presets already loaded.", __func__);
            return Fs16xx_Error_StateInvalid;
        }
    }

    for(i = 0; i < (int)fs16xx_preset_type_max; i++) {
        snprintf(preset_name, PRESET_NAME_SIZE, "%s%s",
                    FS16XX_PARAM_DIR, gFs16xxPresetName[i]);
        fp = fopen(preset_name, "rb");
        if (!fp)
        {
            PRINT_ERROR("%s load preset [%s] failed.", __func__, preset_name);
            err = Fs16xx_Error_Invalid_Preset;
            break;
        }

        // Read header
        count = fread( (void*)&(gFs16xxPresets[i].preset_header), 1, 
            (sizeof(Fs16xx_preset_header_t) - sizeof(unsigned short *)), fp);
        if(count != (sizeof(Fs16xx_preset_header_t) - sizeof(unsigned short *))) {
            PRINT_ERROR("%s load preset header [%s] failed.", __func__, preset_name);
            err = Fs16xx_Error_Invalid_Preset;
            fclose(fp);
            break;
        }

        data_len = gFs16xxPresets[i].preset_header.data_len;
        if(data_len <= 0 || (data_len % 2) != 0) {
            PRINT_ERROR("%s preset header [%s] validation checking failed. data_len=%d", __func__, preset_name, data_len);
            err = Fs16xx_Error_Invalid_Preset;
            fclose(fp);
            break;
        }

        buf_size = data_len * sizeof(unsigned short);
        // Read data
        ptr_data = (unsigned short *)malloc(buf_size);
        if (!ptr_data) {
            PRINT_ERROR("%s failed to allocate %d bytes.\n", __func__, buf_size);
            err = Fs16xx_Error_Invalid_Preset;
            fclose(fp);

            gFs16xxPresets[i].preset_header.data = 0;
            break;
        }

        count = fread((void*)ptr_data, 1, buf_size, fp);
        fclose(fp);
        if(count != buf_size) {
            PRINT_ERROR("%s load preset data [%s] failed. count=%d", __func__, preset_name, count);
            err = Fs16xx_Error_Invalid_Preset;

            gFs16xxPresets[i].preset_header.data = 0;
            free(ptr_data);
            break;
        }

        checksum = fs16xx_calc_checksum(ptr_data, data_len);
        if(checksum != gFs16xxPresets[i].preset_header.crc16) {
            PRINT_ERROR("%s preset data [%s] checksum not match. checksum=0x%04X", __func__, preset_name, checksum);
            err = Fs16xx_Error_Invalid_Preset;
            
            gFs16xxPresets[i].preset_header.data = 0;
            free(ptr_data);
            break;
        }

        gFs16xxPresets[i].preset_header.data = ptr_data;
        gFs16xxPresets[i].valid = PRESET_VALID;

        DEBUGPRINT("%s load preset %s successfully.", __func__, preset_name);
        
    }
    
    return err;
}

unsigned short fs16xx_calc_checksum(unsigned short *data, int len) {
    unsigned short crc = 0;
    unsigned char b, index;
    int i;
    if(len <= 0) return 0;
    
    for (i = 0; i < len; i++)
    {
        b = (unsigned char)(data[i] & 0xFF);
        index = (unsigned char)(crc ^ b);
        crc = (unsigned short)((crc >> 8) ^ gCrc16Table[index]);

        b = (unsigned char)((data[i] >> 8) & 0xFF);
        index = (unsigned char)(crc ^ b);
        crc = (unsigned short)((crc >> 8) ^ gCrc16Table[index]);
    }
    return crc;
}

Fs16xx_Error_t fs16xx_set_preset(Fs16xx_devId_t id, fs16xx_preset_type preset) {
    int i, data_len;
    Fs16xx_Error_t err = Fs16xx_Error_OK;
    unsigned short *ptr_data = 0;
    unsigned short valPwr, valDspCtrl;
    unsigned short valDbg;

    DEBUGPRINT("%s set preset %d.", __func__, preset);

    if (Fs16xx_Error_OK != fs16xx_handle_is_open(id)) {
        PRINT_ERROR("%s device not opened.", __func__);
        return Fs16xx_Error_NotOpen;
    }
    
    switch(preset) {
        case fs16xx_music:
            if(PRESET_VALID == gFs16xxPresets[(int)fs16xx_music].valid) {
                data_len = gFs16xxPresets[(int)fs16xx_music].preset_header.data_len;
                ptr_data = gFs16xxPresets[(int)fs16xx_music].preset_header.data;
            } else {
                err = Fs16xx_Error_Invalid_Preset;
            }
            break;
        case fs16xx_voice:
            if(PRESET_VALID == gFs16xxPresets[(int)fs16xx_voice].valid) {
                data_len = gFs16xxPresets[(int)fs16xx_voice].preset_header.data_len;
                ptr_data = gFs16xxPresets[(int)fs16xx_voice].preset_header.data;
            } else {
                err = Fs16xx_Error_Invalid_Preset;
            }
            break;
        case fs16xx_ringtone:
            if(PRESET_VALID == gFs16xxPresets[(int)fs16xx_ringtone].valid) {
                data_len = gFs16xxPresets[(int)fs16xx_ringtone].preset_header.data_len;
                ptr_data = gFs16xxPresets[(int)fs16xx_ringtone].preset_header.data;
            } else {
                err = Fs16xx_Error_Invalid_Preset;
            }
            break;
        default:
            err = Fs16xx_Error_Invalid_Preset;
            PRINT_ERROR("%s preset not defined.", __func__);
            break;
    }

    DEBUGPRINT("%s data_len=%d ptr_data=%p.", __func__, data_len, ptr_data);

    if(Fs16xx_Error_OK == err && ptr_data) {
        // Amp off
        fs16xx_read_register16(id, FS16XX_SYSCTRL_REG, &valPwr);
        fs16xx_read_register16(id, FS16XX_DSPCTRL_REG, &valDspCtrl);
        fs16xx_write_register16(id, FS16XX_SYSCTRL_REG, valPwr & (~FS16XX_SYSCTRL_REG_AMPE));
        Sleep(30);

        //if(FS16XX_DSP_ENABLE_ALL_MASK != valDspCtrl) {
            fs16xx_write_register16(id, FS16XX_DSPCTRL_REG, valDspCtrl & (~FS16XX_DSPCTRL_REG_EQEN));
        //}

        for (i = 0; i < data_len; i += 2) {
    		err |= fs16xx_write_register16(id, (unsigned char)(0xFF & ptr_data[i]), ptr_data[i + 1]);
        }

        fs16xx_write_register16(id, FS16XX_DSPCTRL_REG, FS16XX_DSP_ENABLE_ALL_MASK);
        fs16xx_write_register16(id, FS16XX_SYSCTRL_REG, valPwr);
    } else {
        PRINT_ERROR("%s set preset error!", __func__);
    }

    DEBUGPRINT("%s exit err=%d.", __func__, err);
    return err;
}

