package com.mediatek.deviceregister;

public class Const {

    public static final String TAG_PREFIX = "DeviceRegister/";

    public static final String ACTION_BOOTCOMPLETED = "android.intent.action.BOOT_COMPLETED";

    public static final String ACTION_PRE_BOOT_COMPLETED =
            "android.intent.action.PRE_BOOT_COMPLETED";

    //Broadcast when network is OK (then can begin to register)
    public static final String ACTION_SMS_STATE_CHANGED =
            "android.provider.Telephony.SMS_STATE_CHANGED";

    // Broadcast from SMS framework, when Server returns register result
    public static final String ACTION_CT_CONFIRMED_MESSAGE =
            "android.telephony.sms.CDMA_REG_SMS_ACTION";

    // Broadcast when register SMS has been sent successfully
    public static final String ACTION_MESSAGE_SEND =
            "com.mediatek.deviceregister.MESSAGE_SEND";

    public static final String ACTION_CDMA_CARD_ESN_OR_MEID =
            "android.telephony.sms.CDMA_CARD_ESN_OR_MEID";
    public static final String ACTION_SHUTDOWN_IPO =
            "android.intent.action.ACTION_SHUTDOWN_IPO";

    public static final String ACTION_SIM_STATE_CHANGED =
            "android.intent.action.SIM_STATE_CHANGED";

    public static final String KEY_ICC_STATE = "ss";

    public static final String VALUE_ICC_LOADED = "LOADED";

    public static final String ACTION_SIM_INSERVICE = "com.mediatek.deviceregister.SIM_INSERVICE";

    public static final String ACTION_IMEI_MEID = "intent_action_imei_meid";

    // Shared preferences to store flag whether need to listen feasible broadcast
    public static final String PRE_KEY_NOT_FIRST_SUBINFO = "pref_key_not_first_subinfo";
    public static final String PRE_KEY_RECEIVED_CARD_MEID_OR_ESN = "pref_key_received_card_meid_or_esn";
    public static final String PRE_KEY_CARD_MEID_OR_ESN = "pref_key_card_meid_or_esn";

    public static final String VALUE_DEFAULT_IMSI = "000000000000000";

    public static final int ONE_SECOND = 1000;

    public static final String VALUE_EMPTY = "";
}
