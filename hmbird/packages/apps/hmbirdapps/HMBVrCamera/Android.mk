LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := HMBVrCamera
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED


LOCAL_OVERRIDES_PACKAGES := Camera

#LOCAL_MULTILIB := 64
#
#LOCAL_PREBUILT_JNI_LIBS :=  \
#            lib64/libjni_jpegdecoder_vr.so \
#            lib64/libjni_logo_decoder.so


LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

include $(BUILD_PREBUILT)

