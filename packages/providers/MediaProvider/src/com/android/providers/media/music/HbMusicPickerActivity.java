/*
* Copyright (C) 2014 MediaTek Inc.
* Modification based on code covered by the mentioned copyright
* and/or permission notice(s).
*/
/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.providers.media.music;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Locale;

import android.app.ListActivity;
import android.content.AsyncQueryHandler;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.CharArrayBuffer;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
//Not Required
//import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.SectionIndexer;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import hb.app.HbActivity;
import hb.app.HbListActivity;
import hb.widget.HbIndexBar;
import hb.widget.HbIndexBar.Letter;
import hb.widget.HbIndexBar.OnSelectListener;
import hb.widget.HbIndexBar.OnTouchStateChangedListener;
import hb.widget.HbIndexBar.TouchState;

import com.android.providers.media.FullActivityBase;
import com.android.providers.media.HbRingtonePickerActivity;
import com.android.providers.media.HbRingtonePickerActivity.ViewHolder;
import com.android.providers.media.MtkLog;
import com.android.providers.media.R;
import com.mediatek.drm.OmaDrmClient;
import com.mediatek.drm.OmaDrmStore;

/**
 * Activity allowing the user to select a music track on the device, and
 * return it to its caller.  The music picker user interface is fairly
 * extensive, providing information about each track like the music
 * application (title, author, album, duration), as well as the ability to
 * previous tracks and sort them in different orders.
 * 
 * <p>This class also illustrates how you can load data from a content
 * provider asynchronously, providing a good UI while doing so, perform
 * indexing of the content for use inside of a {@link FastScrollView}, and
 * perform filtering of the data as the user presses keys.
 */
public class HbMusicPickerActivity extends FullActivityBase
        implements MediaPlayer.OnCompletionListener,
        MusicUtils.Defs, OnItemClickListener {
    //Not Required
    //static final boolean DBG = false;
    static final String TAG = "MusicPicker";
    
    /** Holds the previous state of the list, to restore after the async
     * query has completed. */
    static final String LIST_STATE_KEY = "liststate";
    /** Remember whether the list last had focus for restoring its state. */
    static final String FOCUS_KEY = "focused";
    /** Remember the last ordering mode for restoring state. */
    static final String SORT_MODE_KEY = "sortMode";

    /// M: focus selected position on current playlist.
    static final String SELECTED_POS = "selectedpos";
    /// M: Drm level will decide whether different DRM files can be shown in MusicPicker list.
    static final String DRM_LEVEL = "drmlevel";

    /** Arbitrary number, doesn't matter since we only do one query type. */
    static final int MY_QUERY_TOKEN = 42;
    
    /** Menu item to sort the music list by track title. */
    static final int TRACK_MENU = Menu.FIRST;
    /** Menu item to sort the music list by album title. */
    static final int ALBUM_MENU = Menu.FIRST + 1;
    /** Menu item to sort the music list by artist name. */
    static final int ARTIST_MENU = Menu.FIRST + 2;

    /** These are the columns in the music cursor that we are interested in. */
    static final String[] CURSOR_COLS = new String[] {
            MediaStore.Audio.Media._ID,
            MediaStore.Audio.Media.TITLE,
            /// M: add for chinese sorting
            MediaStore.Audio.Media.TITLE_PINYIN_KEY,
            MediaStore.Audio.Albums.ALBUM_PINYIN_KEY,
            MediaStore.Audio.Artists.ARTIST_PINYIN_KEY,
            MediaStore.Audio.Media.DATA,
            MediaStore.Audio.Media.ALBUM,
            MediaStore.Audio.Media.ARTIST,
            MediaStore.Audio.Media.ARTIST_ID,
            MediaStore.Audio.Media.DURATION,
            MediaStore.Audio.Media.TRACK,
            /// M: To determine whether the Drm file.
            MediaStore.Audio.Media.IS_DRM,
            /// M: To determine whether the Drm method.
            MediaStore.Audio.Media.DRM_METHOD
    };
    
    /** Formatting optimization to avoid creating many temporary objects. */
    static StringBuilder sFormatBuilder = new StringBuilder();
    /** Formatting optimization to avoid creating many temporary objects. */
    static Formatter sFormatter = new Formatter(sFormatBuilder, Locale.getDefault());
    /** Formatting optimization to avoid creating many temporary objects. */
    static final Object[] sTimeArgs = new Object[5];

    /** Uri to the directory of all music being displayed. */
    Uri mBaseUri;
    
    /** This is the adapter used to display all of the tracks. */
    TrackListAdapter mAdapter;
    /** Our instance of QueryHandler used to perform async background queries. */
    QueryHandler mQueryHandler;
    
    /** Used to keep track of the last scroll state of the list. */
    Parcelable mListState = null;
    /** Used to keep track of whether the list last had focus. */
    boolean mListHasFocus;
    
    /** The current cursor on the music that is being displayed. */
    Cursor mCursor;
    /** The actual sort order the user has selected. */
    int mSortMode = -1;
    /** SQL order by string describing the currently selected sort order. */
    String mSortOrder;

    /// M: we show empty view, so mark these not used. {@
    /** Container of the in-screen progress indicator, to be able to hide it
     * when done loading the initial cursor. */
    // View mProgressContainer;
    /** Container of the list view hierarchy, to be able to show it when done
     * loading the initial cursor. */
    // View mListContainer;
    /** Which track row ID the user has last selected. */
    long mSelectedId = -1;
    /** Completel Uri that the user has last selected. */
    Uri mSelectedUri;
    
    /** If >= 0, we are currently playing a track for preview, and this is its
     * row ID. */
    long mPlayingId = -1;
    
    /** This is used for playing previews of the music files. */
    MediaPlayer mMediaPlayer;

    /// M: Store the previous position,otherwise the assignment of -1.
    int mPrevSelectedPos = -1;
    /// M: Store the current selected position,otherwise the assignment of -1.
    int mSelectedPos = -1;

    /// M: Drm level from starting intent.
    int mDrmLevel = -1;

    /// M: Drm manager client.
    private OmaDrmClient mDrmClient = null;

    /// M: Indicate whether the BroadReceiver is registered
    private boolean mIsBroadcastReg = false;
    
    private LayoutInflater layoutInflater;
    /**
     * A special implementation of SimpleCursorAdapter that knows how to bind
     * our cursor data to our list item structure, and takes care of other
     * advanced features such as indexing and filtering.
     */
    class TrackListAdapter extends SimpleCursorAdapter
            implements SectionIndexer {
        final ListView mListView;
        
        private final StringBuilder mBuilder = new StringBuilder();
        private final String mUnknownArtist;
        private final String mUnknownAlbum;

        private int mIdIdx;
        private int mTitleIdx;
        private int mArtistIdx;
        private int mAlbumIdx;
        private int mDurationIdx;
        /// M: IsDrm columns.
        private int mIsDrmIdx;
        /// M: DrmMethod columns.
        private int mDrmMethodIdx;

        private boolean mLoading = true;
        private int mIndexerSortMode;
        private MusicAlphabetIndexer mIndexer;
        
        TrackListAdapter(Context context, ListView listView, int layout,
                String[] from, int[] to) {
            super(context, layout, null, from, to);
            mListView = listView;
            mUnknownArtist = context.getString(R.string.unknown_artist_name);
            mUnknownAlbum = context.getString(R.string.unknown_album_name);
        }

        /**
         * The mLoading flag is set while we are performing a background
         * query, to avoid displaying the "No music" empty view during
         * this time.
         */
        public void setLoading(boolean loading) {
            mLoading = loading;
        }

        @Override
        public boolean isEmpty() {
            if (mLoading) {
                // We don't want the empty state to show when loading.
                return false;
            } else {
                return super.isEmpty();
            }
        }
        
        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View v = layoutInflater.inflate(R.layout.music_picker_item, parent, false);
            ViewHolder viewHodel = ViewHolder.getViewHodel(v);
            viewHodel.buffer1 = new CharArrayBuffer(100);
            v.setTag(viewHodel);
            return v;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            ViewHolder viewHodel = (ViewHolder) view.getTag();
            
            cursor.copyStringToBuffer(mTitleIdx, viewHodel.buffer1);
            viewHodel.textView.setText(viewHodel.buffer1.data, 0, viewHodel.buffer1.sizeCopied);
            
            final StringBuilder builder = mBuilder;
            builder.delete(0, builder.length());

            String name = cursor.getString(mAlbumIdx);
            if (name == null || name.equals("<unknown>")) {
                builder.append(mUnknownAlbum);
            } else {
                builder.append(name);
            }
            builder.append('\n');
            name = cursor.getString(mArtistIdx);
            if (name == null || name.equals("<unknown>")) {
                builder.append(mUnknownArtist);
            } else {
                builder.append(name);
            }
            
            // Update the checkbox of the item, based on which the user last
            // selected.  Note that doing it this way means we must have the
            // list view update all of its items when the selected item
            // changes.
            final long id = cursor.getLong(mIdIdx);
            viewHodel.radioButton.setChecked(id == mSelectedId);
        }

        /**
         * This method is called whenever we receive a new cursor due to
         * an async query, and must take care of plugging the new one in
         * to the adapter.
         */
        @Override
        public void changeCursor(Cursor cursor) {
            super.changeCursor(cursor);
            HbMusicPickerActivity.this.mCursor = cursor;
            
            if (cursor != null) {
                // Retrieve indices of the various columns we are interested in.
                mIdIdx = cursor.getColumnIndex(MediaStore.Audio.Media._ID);
                mTitleIdx = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
                mArtistIdx = cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
                mAlbumIdx = cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM);
                mDurationIdx = cursor.getColumnIndex(MediaStore.Audio.Media.DURATION);
                /// M: Retrieve indices of the IS_DRM and DRM_METHOD @{
                mIsDrmIdx = cursor.getColumnIndex(MediaStore.Audio.Media.IS_DRM);
                mDrmMethodIdx = cursor.getColumnIndex(MediaStore.Audio.Media.DRM_METHOD);
                /// @}

                // If the sort mode has changed, or we haven't yet created an
                // indexer one, then create a new one that is indexing the
                // appropriate column based on the sort mode.
                if (mIndexerSortMode != mSortMode || mIndexer == null) {
                    mIndexerSortMode = mSortMode;
                    /// M: Add for Chinese sort with title, artist or album.
                    int idx = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE_PINYIN_KEY);
                    switch (mIndexerSortMode) {
                        case ARTIST_MENU:
                            idx = cursor.getColumnIndexOrThrow
                            (MediaStore.Audio.Artists.ARTIST_PINYIN_KEY);
                            break;
                        case ALBUM_MENU:
                            idx = cursor.getColumnIndexOrThrow
                            (MediaStore.Audio.Albums.ALBUM_PINYIN_KEY);
                            break;
                    }
                    mIndexer = new MusicAlphabetIndexer(cursor, idx,
                            getResources().getString(R.string.fast_scroll_alphabet));
                    
                // If we have a valid indexer, but the cursor has changed since
                // its last use, then point it to the current cursor.
                } else {
                    mIndexer.setCursor(cursor);
                }
                
            }
            /// M: Show empty view instead.
            // Ensure that the list is shown (and initial progress indicator
            // hidden) in case this is the first cursor we have gotten.
            // makeListShown();          	
        }

        /**
         * This method is called from a background thread by the list view
         * when the user has typed a letter that should result in a filtering
         * of the displayed items.  It returns a Cursor, when will then be
         * handed to changeCursor.
         */
        @Override
        public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
            return doQuery(true, constraint.toString());
        }

        public int getPositionForSection(int section) {
            Cursor cursor = getCursor();
            if (cursor == null) {
                // No cursor, the section doesn't exist so just return 0
                return 0;
            }
            /// M: Indexer error check.
            if (mIndexer != null) {
                return mIndexer.getPositionForSection(section);
            }
            /// M: no indexer , just return 0.
            return 0;
        }

        public int getSectionForPosition(int position) {
            /// M: Indexer error check.
            if (mIndexer != null) {
                return mIndexer.getSectionForPosition(position);
            }
          /// M: no indexer , just return 0.
            return 0;
        }

        public Object[] getSections() {
            if (mIndexer != null) {
                return mIndexer.getSections();
            }
            return null;
        }
    }

    /**
     * This is our specialization of AsyncQueryHandler applies new cursors
     * to our state as they become available.
     */
    private final class QueryHandler extends AsyncQueryHandler {
        public QueryHandler(Context context) {
            super(context.getContentResolver());
        }

        @Override
        protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
            if (cursor != null) {
                if (!isFinishing()) {
                    /// M: when media file be empty,show the empty view
                    MusicUtils.emptyShow(listView, HbMusicPickerActivity.this);

                    // Update the adapter: we are no longer loading, and have
                    // a new cursor for it.
                    mAdapter.setLoading(false);
                    mAdapter.changeCursor(cursor);
                    ///M: when listview has item showed ,let the ProgressBar@{
                    if (listView.getCount() != 0) {
                        setProgressBarIndeterminateVisibility(false);
                    }
                    /// @}
                    // Now that the cursor is populated again, it's possible to
                    // restore the list state
                    if (mListState != null) {
                        listView.onRestoreInstanceState(mListState);
                        if (mListHasFocus) {
                            listView.requestFocus();
                        }
                        mListHasFocus = false;
                        mListState = null;
                    }
                    /// M: when query out audios, hide error ui.
                    MusicUtils.hideDatabaseError(HbMusicPickerActivity.this);
                    HbMusicPickerActivity.this.setTitle(R.string.local_audio);
                } else {
                    cursor.close();
                }
            }
        }
    }
    
    private int mType;
    
    private ListView listView;
    
    /**
     * Called when the activity is first created.
     * @param icicle
     */
    @Override
    public void onCreate(Bundle icicle) {
    	requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
    	
        super.onCreate(icicle);
        layoutInflater = LayoutInflater.from(this);
        setContentView(R.layout.music_picker);
        listView = (ListView)findViewById(android.R.id.list);
        
		mType = getIntent().getIntExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, -1);
        Uri mExistingUri = getIntent().getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI);
        if(mExistingUri!=null && mExistingUri.toString().contains(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI.toString())){
        	mSelectedId = ContentUris.parseId(mExistingUri);        	
        }
        
        
        /// M: set the music style Audio.
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        int sortMode = TRACK_MENU;
        if (icicle == null) {
            mSelectedUri = getIntent().getParcelableExtra(
                    RingtoneManager.EXTRA_RINGTONE_EXISTING_URI);
            /// M: Retrieve the Drmlevel from intent @{
            mDrmLevel = getIntent().getIntExtra(OmaDrmStore.DrmExtra.EXTRA_DRM_LEVEL, -1);
            /// @}
        } else {
            mSelectedUri = (Uri)icicle.getParcelable(
                    RingtoneManager.EXTRA_RINGTONE_EXISTING_URI);
            // Retrieve list state. This will be applied after the
            // QueryHandler has run.
            mListState = icicle.getParcelable(LIST_STATE_KEY);
            mListHasFocus = icicle.getBoolean(FOCUS_KEY);
            sortMode = icicle.getInt(SORT_MODE_KEY, sortMode);
            /// M: Returns the value associated with the
            ///given key(SELECTED_POS|DRM_LEVEL), or defaultValue
            // if no mapping of the desired type exists for the given key. @{
            mPrevSelectedPos = icicle.getInt(SELECTED_POS, -1);
            mDrmLevel = icicle.getInt(DRM_LEVEL, -1);
            /// @}
        }
        if (Intent.ACTION_GET_CONTENT.equals(getIntent().getAction())) {
            mBaseUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        } else {
            mBaseUri = getIntent().getData();
            if (mBaseUri == null) {
                finish();
                return;
            }
        }
        
        //Not required
        
        listView.setOnItemClickListener(this);
        
        /// M: Creates a DrmManagerClient.
        if (MusicFeatureOption.IS_SUPPORT_DRM) {
            mDrmClient = new OmaDrmClient(this);
        }
        /// M: add for chinese sorting
        mSortOrder = MediaStore.Audio.Media.TITLE_PINYIN_KEY;


        listView.setItemsCanFocus(false);
        
        mAdapter = new TrackListAdapter(this, listView,
                R.layout.music_picker_item, new String[] {},
                new int[] {});

        listView.setAdapter(mAdapter);
        
        listView.setTextFilterEnabled(true);

        // We manually save/restore the listview state
        listView.setSaveEnabled(false);

        mQueryHandler = new QueryHandler(this);
        
        // If there is a currently selected Uri, then try to determine who
        // it is.
        if (mSelectedUri != null) {
            Uri.Builder builder = mSelectedUri.buildUpon();
            String path = mSelectedUri.getEncodedPath();
            int idx = path.lastIndexOf('/');
            if (idx >= 0) {
                path = path.substring(0, idx);
            }
            builder.encodedPath(path);
            Uri baseSelectedUri = builder.build();
            if (baseSelectedUri.equals(mBaseUri)) {
                // If the base Uri of the selected Uri is the same as our
                // content's base Uri, then use the selection!
                mSelectedId = ContentUris.parseId(mSelectedUri);
            }
        }

        /// M: add IntentFilter action and register a Listener for SD card status changed @{
        IntentFilter f = new IntentFilter();
        f.addAction(Intent.ACTION_MEDIA_SCANNER_STARTED);
        f.addAction(Intent.ACTION_MEDIA_SCANNER_FINISHED);
        f.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        f.addAction(Intent.ACTION_MEDIA_MOUNTED);
        f.addDataScheme("file");
        registerReceiver(mScanListener, f);
        /// @}
        mIsBroadcastReg = true;
        setSortMode(sortMode);
    }

    @Override public void onRestart() {
        super.onRestart();
        doQuery(false, null);
    }

    /// M: refresh listview when activity display again. @{
    @Override public void onResume() {
        super.onResume();
        listView.invalidateViews();
    }
    /// @}


    @Override protected void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
        // Save list state in the bundle so we can restore it after the
        // QueryHandler has run
        icicle.putParcelable(LIST_STATE_KEY, listView.onSaveInstanceState());
        icicle.putBoolean(FOCUS_KEY, listView.hasFocus());
        icicle.putInt(SORT_MODE_KEY, mSortMode);
        /// M: store selected uri and Drmlevel for re_create this activity @{
        icicle.putParcelable(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, mSelectedUri);
        icicle.putInt(DRM_LEVEL, mDrmLevel);
        /// @}
    }
    
    @Override public void onPause() {
        super.onPause();
        stopMediaPlayer();
    }

    @Override public void onStop() {
        super.onStop();

        // We don't want the list to display the empty state, since when we
        // resume it will still be there and show up while the new query is
        // happening. After the async query finishes in response to onResume()
        // setLoading(false) will be called.
        mAdapter.setLoading(true);
        mAdapter.changeCursor(null);
    }

    /**
     * Changes the current sort order, building the appropriate query string
     * for the selected order.
     */
    boolean setSortMode(int sortMode) {
        if (sortMode != mSortMode) {
            switch (sortMode) {
                case TRACK_MENU:
                    mSortMode = sortMode;
                    /// M: add for chinese sorting
                    mSortOrder = MediaStore.Audio.Media.TITLE_PINYIN_KEY;
                    doQuery(false, null);
                    return true;
                case ALBUM_MENU:
                    mSortMode = sortMode;
                    /// M: add for chinese sorting
                    mSortOrder = MediaStore.Audio.Albums.ALBUM_PINYIN_KEY + " ASC, "
                            + MediaStore.Audio.Media.TRACK + " ASC, "
                            + MediaStore.Audio.Media.TITLE_PINYIN_KEY + " ASC";
                    doQuery(false, null);
                    return true;
                case ARTIST_MENU:
                    mSortMode = sortMode;
                    /// M: add for chinese sorting
                    mSortOrder = MediaStore.Audio.Artists.ARTIST_PINYIN_KEY + " ASC, "
                            + MediaStore.Audio.Albums.ALBUM_PINYIN_KEY + " ASC, "
                            + MediaStore.Audio.Media.TRACK + " ASC, "
                            + MediaStore.Audio.Media.TITLE_PINYIN_KEY + " ASC";
                    doQuery(false, null);
                    return true;
            }
            
        }
        return false;
    }

    /**
     * The first time this is called, we hide the large progress indicator
     * and show the list view, doing fade animations between them.
     */
    /*void makeListShown() {
        if (!mListShown) {
            mListShown = true;
            mProgressContainer.startAnimation(AnimationUtils.loadAnimation(
                    this, android.R.anim.fade_out));
            mProgressContainer.setVisibility(View.GONE);
            mListContainer.startAnimation(AnimationUtils.loadAnimation(
                    this, android.R.anim.fade_in));
            mListContainer.setVisibility(View.VISIBLE);
        }
    }*/

    /**
     * Common method for performing a query of the music database, called for
     * both top-level queries and filtering.
     * 
     * @param sync If true, this query should be done synchronously and the
     * resulting cursor returned.  If false, it will be done asynchronously and
     * null returned.
     * @param filterstring If non-null, this is a filter to apply to the query.
     */
    Cursor doQuery(boolean sync, String filterstring) {
        // Cancel any pending queries
        mQueryHandler.cancelOperation(MY_QUERY_TOKEN);
        
        StringBuilder where = new StringBuilder();
        where.append(MediaStore.Audio.Media.TITLE + " != ''");
        
        where.append(" AND "+MediaStore.Audio.Media.MIME_TYPE +" != 'audio/x-ms-wma'");
        
        /// M: determine the Dim level for query @{
        if (MusicFeatureOption.IS_SUPPORT_DRM) {
            String sIsDrm = MediaStore.Audio.Media.IS_DRM;
            String sDrmMethod = MediaStore.Audio.Media.DRM_METHOD;
            switch (mDrmLevel) {
                case OmaDrmStore.DrmExtra.LEVEL_FL:
                    where.append(" AND (" + sIsDrm + "!=1 OR (" + sIsDrm + "=1" +
                     " AND " + sDrmMethod + "="
                            + OmaDrmStore.DrmMethod.METHOD_FL + "))");
                    break;

                case OmaDrmStore.DrmExtra.LEVEL_SD:
                    where.append(" AND (" + sIsDrm + "!=1 OR (" + sIsDrm + "=1" +
                     " AND " + sDrmMethod + "="
                            + OmaDrmStore.DrmMethod.METHOD_SD + "))");
                    break;

                case OmaDrmStore.DrmExtra.LEVEL_ALL:
                    break;

                case -1:
                default:
                    // this intent does not contain DRM Extras
                    where.append(" AND " + sIsDrm + "!=1");
                    break;
            }
        }
        /// @}

        // We want to show all audio files, even recordings.  Enforcing the
        // following condition would hide recordings.
        //where.append(" AND " + MediaStore.Audio.Media.IS_MUSIC + "=1");

        Uri uri = mBaseUri;
        if (!TextUtils.isEmpty(filterstring)) {
            uri = uri.buildUpon().appendQueryParameter("filter", Uri.encode(filterstring)).build();
        }

        if (sync) {
            try {
                return getContentResolver().query(uri, CURSOR_COLS,
                        where.toString(), null, mSortOrder);
            } catch (UnsupportedOperationException ex) {
            }
        } else {
            mAdapter.setLoading(true);
            //Not Required
            //setProgressBarIndeterminateVisibility(true);
            mQueryHandler.startQuery(MY_QUERY_TOKEN, null, uri, CURSOR_COLS,
                    where.toString(), null, mSortOrder);
        }
        return null;
    }
    
    
    

    void setSelected(Cursor c) {
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        long newId = mCursor.getLong(mCursor.getColumnIndex(MediaStore.Audio.Media._ID));
        mSelectedUri = ContentUris.withAppendedId(uri, newId);

        mSelectedId = newId;
        if (newId != mPlayingId || mMediaPlayer == null) {
            stopMediaPlayer();
            //hummmingbird liuqin add for bugfix #2215 20170617 start
            if (getAudioManager().getStreamVolume(AudioManager.STREAM_MUSIC) > 0) {
                mMediaPlayer = new MediaPlayer();
                try {
                    mMediaPlayer.setDataSource(this, mSelectedUri);
                    mMediaPlayer.setOnCompletionListener(this);
                    mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mMediaPlayer.prepare();
                    mMediaPlayer.start();
                    mPlayingId = newId;
                    //Not Required
                    //listView.invalidateViews();

                    requestAudioFocus(getAudioManager());
                } catch (IOException e) {
                    /// M: finally just get invalidate list view @{
                }
            }
            listView.invalidateViews();
            //hummmingbird liuqin add for bugfix #2215 20170617 end
            /// @}
        } else if (mMediaPlayer != null) {
            stopMediaPlayer();
            listView.invalidateViews();
        }

        setRingtone(getContentResolver(), mSelectedUri);

        //hummmingbird liuqin add for bugfix #2182 20170615 start
        onClick(mSelectedUri);
        //hummmingbird liuqin add for bugfix #2182 20170615 end
    }
    
    public void onCompletion(MediaPlayer mp) {
        if (mMediaPlayer == mp) {
            mp.stop();
            mp.release();
            mMediaPlayer = null;
            mPlayingId = -1;
            listView.invalidateViews();

            //hummmingbird liuqin add for bugfix #2215 20170617 start
            abandonAudioFocus(getAudioManager());
            //hummmingbird liuqin add for bugfix #2215 20170617 end
        }
    }
    
    void stopMediaPlayer() {
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
            mPlayingId = -1;

            //hummmingbird liuqin add for bugfix #2215 20170617 start
            abandonAudioFocus(getAudioManager());
            //hummmingbird liuqin add for bugfix #2215 20170617 end
        }
    }
    
    private void setRingtone(ContentResolver resolver, Uri uri) {
		/// Set the flag in the database to mark this as a ringtone
		try {
			ContentValues values = new ContentValues(1);
			if ((RingtoneManager.TYPE_RINGTONE == mType) || (RingtoneManager.TYPE_VIDEO_CALL == mType)
					|| (RingtoneManager.TYPE_SIP_CALL == mType)) {
				values.put(MediaStore.Audio.Media.IS_RINGTONE, "1");
			} else if (RingtoneManager.TYPE_ALARM == mType) {
				values.put(MediaStore.Audio.Media.IS_ALARM, "1");
			} else if (RingtoneManager.TYPE_NOTIFICATION == mType) {
				values.put(MediaStore.Audio.Media.IS_NOTIFICATION, "1");
			} else {
				MtkLog.e(TAG, "Unsupport ringtone type =  " + mType);
				return;
			}
			resolver.update(uri, values, null, null);
			/// Restore the new uri and set it to be checked after resume
		} catch (UnsupportedOperationException ex) {
			/// most likely the card just got unmounted
			MtkLog.e(TAG, "couldn't set ringtone flag for uri " + uri);
		}
        //hummmingbird liuqin modify for bugfix #2182 20170615 start
		// onClick(uri);
        //hummmingbird liuqin modify for bugfix #2182 20170615 end
	}
    
	public void onClick(Uri uri) {
		if (mSelectedId >= 0) {
            setResult(RESULT_OK, new Intent().setData(mSelectedUri));
        }
	}

    /**
     * M: Monitor the SD card status change.
     */
    private final BroadcastReceiver mScanListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();
            String status = Environment.getExternalStorageState();

            if (Intent.ACTION_MEDIA_SCANNER_STARTED.equals(action) ||
                    Intent.ACTION_MEDIA_SCANNER_FINISHED.equals(action)) {
                MusicUtils.setSpinnerState(HbMusicPickerActivity.this);
            }
            doQuery(false, null);
        }
    };

    /**
     * Don't respond to quick search request in HbMusicPickerActivity
     */
    @Override
    public boolean onSearchRequested() {
        return false;
    };

    /**
     * M: Unregister sdcard listener
     */
    @Override
    protected void onDestroy() {
        if (mIsBroadcastReg == true) {
            unregisterReceiver(mScanListener);
        }
        if (mDrmClient != null) {
            mDrmClient.release();
            mDrmClient = null;
        }
        super.onDestroy();
    }

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        mCursor.moveToPosition(position);
        /// M: get the selected position.
        mSelectedPos = position;

        setSelected(mCursor);
	}

    //hummmingbird liuqin add for bugfix #2215 20170617 start
    private AudioManager mAudioManager;

    private AudioManager getAudioManager() {
        if (mAudioManager == null) {
            mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        }
        return mAudioManager;
    }

    public static void requestAudioFocus(AudioManager am) {
        am.requestAudioFocus(null, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
    }

    public static void abandonAudioFocus(AudioManager am) {
        am.abandonAudioFocus(null);
    }
    //hummmingbird liuqin add for bugfix #2215 20170617 end
}
