#include "jni.h"
#include "JNIHelp.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "utils/Log.h"



using namespace android;

#ifdef __cplusplus
extern "C" {
	
//ioctl
#define TEMP_SEN_IO  'T'
#define TEMP_SEN_WAKE_UP_CMD   _IO(TEMP_SEN_IO, 0x01)
#define TEMP_SEN_SLEEP_CMD 	   _IO(TEMP_SEN_IO, 0x02)
#endif
#define LOG_TAG "com_example_tempreture_TempSenControl"


JNIEXPORT jint JNICALL Java_com_example_tempreture_TempSenControl_TempRead
  (JNIEnv * env, jobject c,jint num) {	
    ALOGD("lgw Java_com_example_tempreture_TempSenControl_TempRead\n");  
	int fd = open("dev/temp",O_RDONLY, 0);
	char read_buf[4];
	if (fd < 0) {
		ALOGD("lgw OPEN Failed");
		return -1000;
	}
	else 
	{
		int ret = -1000;
		ALOGD("lgw begin read\n");
		if (read(fd, read_buf, sizeof(read_buf)) == -1) {
			//LOGE("temp read Failed");
			close(fd);
			return -1000;
		}
		close(fd);
		ALOGD("lgw read ok\n");
		if (num == 1)
		ret=(read_buf[1]<<8) | read_buf[0];
		else if (num ==2)
		ret=(read_buf[3]<<8) | read_buf[2];
		ALOGD("lgw temp read val=%x \n",ret);
		return ret;
	}

}

JNIEXPORT jint JNICALL Java_com_example_tempreture_TempSenControl_TempEnable
  (JNIEnv * env, jobject c,jboolean enable) {
   ALOGD("lgw Java_com_zte_engineer_TempSenControl_TempEnable\n");  
    int ret;
    int fd = open("dev/temp",O_RDONLY, 0);
   	if (fd < 0) {
		ALOGD("lgw OPEN Failed");
		return -1000;
	}
	if(enable==JNI_TRUE)
	{
	 	ret=ioctl(fd,TEMP_SEN_WAKE_UP_CMD);
	}
	else
	{
		
		ret=ioctl(fd,TEMP_SEN_SLEEP_CMD);
	}
	close(fd);
	if(ret!=0)
	  return -1;
    return 0;
  }
  
/*
 * JNI registration.
 */
static JNINativeMethod gMethods[] =
{
   {"TempRead",  "(I)I",(void *)Java_com_example_tempreture_TempSenControl_TempRead},
   {"TempEnable",  "(Z)I",(void *)Java_com_example_tempreture_TempSenControl_TempEnable},
};

int register_com_example_tempreture_TempSenControl(JNIEnv *e)
{
   return jniRegisterNativeMethods(e, "com/example/tempreture/TempSenControl",gMethods, NELEM(gMethods));
}

/*
 * JNI Initialization
 */
jint JNI_OnLoad(JavaVM *jvm, void *reserved)
{
    JNIEnv *e;

    ALOGD("lgw temp sen Service : loading JNI\n");
	
    // Check JNI version
    if(jvm->GetEnv((void **)&e, JNI_VERSION_1_6))
        return JNI_ERR;

    register_com_example_tempreture_TempSenControl(e);

    ALOGD ("lgw %s: exit", __FUNCTION__);

    return JNI_VERSION_1_6;
}
#ifdef __cplusplus
}
#endif
