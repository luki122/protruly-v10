#include "dpe_test_case_02_frame_0_dpe_setting_00.h"
#define DVE_ENABLE 0x1
#define WMFE_ENABLE 0x0
#define WMFE_ENABLE_0 0x0
#define WMFE_ENABLE_1 0x0
#define WMFE_ENABLE_2 0x0
int dpe_test_case_02_frame_0_golden_l_start_x = 12;
int dpe_test_case_02_frame_0_golden_l_start_y = 8;
int dpe_test_case_02_frame_0_golden_l_end_x = 4340;
int dpe_test_case_02_frame_0_golden_l_end_y = 8183;
int dpe_test_case_02_frame_0_golden_r_start_x = 8;
int dpe_test_case_02_frame_0_golden_r_start_y = 8;
int dpe_test_case_02_frame_0_golden_r_end_x = 4340;
int dpe_test_case_02_frame_0_golden_r_end_y = 8183;
MUINT32 dpe_test_case_02_frame_0_golden_DVE_HORZ_SV = 0x3FC;
MUINT32 dpe_test_case_02_frame_0_golden_DVE_VERT_SV =  0x00;
MBOOL g_b_dpe_test_case_02_frame_0DVECallback;
MBOOL g_b_dpe_test_case_02_frame_0WMFECallback;

MVOID DPE_dpe_test_case_02_frame_0DVECallback(DVEParams& rParams)
{
	MUINT32 DVE_HORZ_SV;
	MUINT32 DVE_VERT_SV;
	printf("--- [DVE callback func]\n");
	vector<DVEConfig>::iterator iter = rParams.mDVEConfigVec.begin();
	for (;iter!= rParams.mDVEConfigVec.end();iter++)
	{
		DVE_VERT_SV = (*iter).Dve_Vert_Sv;
		DVE_HORZ_SV = (*iter).Dve_Horz_Sv;
		printf("DVE_VERT_SV:%d, DVE_HORZ_SV:%d\n", DVE_VERT_SV, DVE_HORZ_SV);
		if ( (dpe_test_case_02_frame_0_golden_DVE_HORZ_SV == DVE_HORZ_SV) &&
			 (dpe_test_case_02_frame_0_golden_DVE_VERT_SV == DVE_VERT_SV))
		{
			//Pass
			printf("dpe DVE Statistic Result 0 bit true pass!!!\n");
		}
		else
		{
			//Error
			printf("dpe DVE Statistic Result 0 bit true fail, DVE_HORZ_SV:(%d), DVE_VERT_SV:(%d)!!!\n", DVE_HORZ_SV, DVE_VERT_SV);
		}
	}
	g_b_dpe_test_case_02_frame_0DVECallback = MTRUE;
}

MVOID DPE_dpe_test_case_02_frame_0WMFECallback(WMFEParams& rParams)
{
	printf("--- [WMEF callback func]\n");
	g_b_dpe_test_case_02_frame_0WMFECallback = MTRUE;
}


bool dpe_test_case_02_frame_0_DPE_Config()
{


	NSCam::NSIoPipe::NSDpe::IDpeStream* pStream;
	pStream= NSCam::NSIoPipe::NSDpe::IDpeStream::createInstance("dpe_test_case_02_frame_0");
	pStream->init();
	printf("--- [test_dpe_default...DpeStream init done\n");
	IMemDrv* mpImemDrv=NULL;
	mpImemDrv=IMemDrv::createInstance();
	mpImemDrv->init();

	int golden_l_start_x = dpe_test_case_02_frame_0_golden_l_start_x;
	int golden_l_start_y = dpe_test_case_02_frame_0_golden_l_start_y;
	int golden_l_end_x = dpe_test_case_02_frame_0_golden_l_end_x;
	int golden_l_end_y = dpe_test_case_02_frame_0_golden_l_end_y;
	int golden_r_start_x = dpe_test_case_02_frame_0_golden_r_start_x;
	int golden_r_start_y = dpe_test_case_02_frame_0_golden_r_start_y;
	int golden_r_end_x = dpe_test_case_02_frame_0_golden_r_end_x;
	int golden_r_end_y = dpe_test_case_02_frame_0_golden_r_end_y;
	MUINT32 golden_DVE_HORZ_SV = dpe_test_case_02_frame_0_golden_DVE_HORZ_SV;
	MUINT32 golden_DVE_VERT_SV = dpe_test_case_02_frame_0_golden_DVE_VERT_SV;
	//copy the pointer of golden answer of DPE
	unsigned long golden_dpe_dvo_l_frame;
	unsigned long golden_dpe_dvo_r_frame;
	unsigned long golden_dpe_confo_l_frame;
	unsigned long golden_dpe_confo_r_frame;
	unsigned long golden_dpe_respo_l_frame;
	unsigned long golden_dpe_respo_r_frame;
	unsigned long golden_dpe_wmf_dpo_frame_0;
	unsigned long golden_dpe_wmf_dpo_frame_1;
	unsigned long golden_dpe_wmf_dpo_frame_2;
	getdpe_test_case_02_frame_0GoldPointer(
	&golden_dpe_dvo_l_frame,
	&golden_dpe_dvo_r_frame,
	&golden_dpe_confo_l_frame,
	&golden_dpe_confo_r_frame,
	&golden_dpe_respo_l_frame,
	&golden_dpe_respo_r_frame,
	&golden_dpe_wmf_dpo_frame_0,
	&golden_dpe_wmf_dpo_frame_1,
	&golden_dpe_wmf_dpo_frame_2
	);
	//input frame pointer
	char* dpe_imgi_l_frame;
	char* dpe_imgi_r_frame;
	char* dpe_dvi_l_frame;
	char* dpe_dvi_r_frame;
	char* dpe_maski_l_frame;
	char* dpe_maski_r_frame;
	char* dpe_wmf_imgi_frame_0;
	char* dpe_wmf_dpi_frame_0;
	char* dpe_wmf_tbli_frame_0;
	char* dpe_wmf_imgi_frame_1;
	char* dpe_wmf_dpi_frame_1;
	char* dpe_wmf_tbli_frame_1;
	char* dpe_wmf_imgi_frame_2;
	char* dpe_wmf_dpi_frame_2;
	char* dpe_wmf_tbli_frame_2;
#if DVE_ENABLE
	dpe_imgi_l_frame = &dpe_test_case_02_frame_0_dpe_imgi_l_frame_00[0];
	dpe_imgi_r_frame = &dpe_test_case_02_frame_0_dpe_imgi_r_frame_00[0];
	dpe_dvi_l_frame = &dpe_test_case_02_frame_0_dpe_dvi_l_frame_00[0];
	dpe_dvi_r_frame = &dpe_test_case_02_frame_0_dpe_dvi_r_frame_00[0];
	dpe_maski_l_frame = &dpe_test_case_02_frame_0_dpe_maski_l_frame_00[0];
	dpe_maski_r_frame = &dpe_test_case_02_frame_0_dpe_maski_r_frame_00[0];
#endif
#if WMFE_ENABLE_0
	dpe_wmf_imgi_frame_0 = &dpe_test_case_02_frame_0_in_dpe_wmf_imgi_frame_0[0];
	dpe_wmf_dpi_frame_0 = &dpe_test_case_02_frame_0_in_dpe_wmf_dpi_frame_0[0];
	dpe_wmf_tbli_frame_0 = &dpe_test_case_02_frame_0_in_dpe_wmf_tbli_frame_0[0];
#endif
#if WMFE_ENABLE_1
	dpe_wmf_imgi_frame_1 = &dpe_test_case_02_frame_0_in_dpe_wmf_imgi_frame_1[0];
	dpe_wmf_dpi_frame_1 = &dpe_test_case_02_frame_0_in_dpe_wmf_dpi_frame_1[0];
	dpe_wmf_tbli_frame_1 = &dpe_test_case_02_frame_0_in_dpe_wmf_tbli_frame_1[0];
#endif
#if WMFE_ENABLE_2
	dpe_wmf_imgi_frame_2 = &dpe_test_case_02_frame_0_in_dpe_wmf_imgi_frame_2[0];
	dpe_wmf_dpi_frame_2 = &dpe_test_case_02_frame_0_in_dpe_wmf_dpi_frame_2[0];
	dpe_wmf_tbli_frame_2 = &dpe_test_case_02_frame_0_in_dpe_wmf_tbli_frame_2[0];
#endif

	//allocate the memory to be used the Target of DPE
	char* dpe_dvo_l_frame;
	char* dpe_dvo_r_frame;
	char* dpe_confo_l_frame;
	char* dpe_confo_r_frame;
	char* dpe_respo_l_frame;
	char* dpe_respo_r_frame;
	char* dpe_wmf_dpo_frame_0;
	char* dpe_wmf_dpo_frame_1;
	char* dpe_wmf_dpo_frame_2;

	printf("#########################################################\n");
	printf("###########dpe_test_case_02_frame_0Start to Test !!!!###########\n");
	printf("#########################################################\n");
#if DVE_ENABLE
	printf("--- dve input and output allocate init\n");
	IMEM_BUF_INFO buf_imgi_l_frame;
	buf_imgi_l_frame.size = dpe_test_case_02_frame_0_dpe_imgi_l_frame_00_size;
	printf("buf_imgi_l_frame.size:%d",buf_imgi_l_frame.size);
	mpImemDrv->allocVirtBuf(&buf_imgi_l_frame);
	mpImemDrv->mapPhyAddr(&buf_imgi_l_frame);
	memcpy( (MUINT8*)(buf_imgi_l_frame.virtAddr), (MUINT8*)(dpe_test_case_02_frame_0_dpe_imgi_l_frame_00), buf_imgi_l_frame.size);
	mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_imgi_l_frame);

	IMEM_BUF_INFO buf_imgi_r_frame;
	buf_imgi_r_frame.size = dpe_test_case_02_frame_0_dpe_imgi_r_frame_00_size;
	printf("buf_imgi_r_frame.size:%d",buf_imgi_r_frame.size);
	mpImemDrv->allocVirtBuf(&buf_imgi_r_frame);
	mpImemDrv->mapPhyAddr(&buf_imgi_r_frame);
	memcpy( (MUINT8*)(buf_imgi_r_frame.virtAddr), (MUINT8*)(dpe_test_case_02_frame_0_dpe_imgi_r_frame_00), buf_imgi_r_frame.size);
	mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_imgi_r_frame);

	IMEM_BUF_INFO buf_dvi_l_frame;
	buf_dvi_l_frame.size = dpe_test_case_02_frame_0_dpe_dvi_l_frame_00_size;
	printf("buf_dvi_l_frame.size:%d",buf_dvi_l_frame.size);
	mpImemDrv->allocVirtBuf(&buf_dvi_l_frame);
	mpImemDrv->mapPhyAddr(&buf_dvi_l_frame);
	memcpy( (MUINT8*)(buf_dvi_l_frame.virtAddr), (MUINT8*)(dpe_test_case_02_frame_0_dpe_dvi_l_frame_00), buf_dvi_l_frame.size);
	mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_dvi_l_frame);

	IMEM_BUF_INFO buf_dvi_r_frame;
	buf_dvi_r_frame.size = dpe_test_case_02_frame_0_dpe_dvi_r_frame_00_size;
	printf("buf_dvi_r_frame.size:%d",buf_dvi_r_frame.size);
	mpImemDrv->allocVirtBuf(&buf_dvi_r_frame);
	mpImemDrv->mapPhyAddr(&buf_dvi_r_frame);
	memcpy( (MUINT8*)(buf_dvi_r_frame.virtAddr), (MUINT8*)(dpe_test_case_02_frame_0_dpe_dvi_r_frame_00), buf_dvi_r_frame.size);
	mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_dvi_r_frame);

	IMEM_BUF_INFO buf_maski_l_frame;
	buf_maski_l_frame.size = dpe_test_case_02_frame_0_dpe_maski_l_frame_00_size;
	printf("buf_maski_l_frame.size:%d",buf_maski_l_frame.size);
	mpImemDrv->allocVirtBuf(&buf_maski_l_frame);
	mpImemDrv->mapPhyAddr(&buf_maski_l_frame);
	memcpy( (MUINT8*)(buf_maski_l_frame.virtAddr), (MUINT8*)(dpe_test_case_02_frame_0_dpe_maski_l_frame_00), buf_maski_l_frame.size);
	mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_maski_l_frame);

	IMEM_BUF_INFO buf_maski_r_frame;
	buf_maski_r_frame.size = dpe_test_case_02_frame_0_dpe_maski_r_frame_00_size;
	printf("buf_maski_r_frame.size:%d",buf_maski_r_frame.size);
	mpImemDrv->allocVirtBuf(&buf_maski_r_frame);
	mpImemDrv->mapPhyAddr(&buf_maski_r_frame);
	memcpy( (MUINT8*)(buf_maski_r_frame.virtAddr), (MUINT8*)(dpe_test_case_02_frame_0_dpe_maski_r_frame_00), buf_maski_r_frame.size);
	mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_maski_r_frame);

	IMEM_BUF_INFO buf_dvo_l_frame;
	buf_dvo_l_frame.size = dpe_test_case_02_frame_0_golden_dpe_dvo_l_size;
	printf("buf_dvo_l_frame.size:%d",buf_dvo_l_frame.size);
	mpImemDrv->allocVirtBuf(&buf_dvo_l_frame);
	mpImemDrv->mapPhyAddr(&buf_dvo_l_frame);
	memset( (MUINT8*)(buf_dvo_l_frame.virtAddr), 0xffffffff , buf_dvo_l_frame.size);
	mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_dvo_l_frame);

	IMEM_BUF_INFO buf_dvo_r_frame;
	buf_dvo_r_frame.size = dpe_test_case_02_frame_0_golden_dpe_dvo_r_size;
	printf("buf_dvo_r_frame.size:%d",buf_dvo_r_frame.size);
	mpImemDrv->allocVirtBuf(&buf_dvo_r_frame);
	mpImemDrv->mapPhyAddr(&buf_dvo_r_frame);
	memset( (MUINT8*)(buf_dvo_r_frame.virtAddr), 0xffffffff , buf_dvo_r_frame.size);
	mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_dvo_r_frame);

	IMEM_BUF_INFO buf_confo_l_frame;
	buf_confo_l_frame.size = dpe_test_case_02_frame_0_golden_dpe_confo_l_size;
	printf("buf_confo_l_frame.size:%d",buf_confo_l_frame.size);
	mpImemDrv->allocVirtBuf(&buf_confo_l_frame);
	mpImemDrv->mapPhyAddr(&buf_confo_l_frame);
	memset( (MUINT8*)(buf_confo_l_frame.virtAddr), 0xffffffff , buf_confo_l_frame.size);
	mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_confo_l_frame);

	IMEM_BUF_INFO buf_confo_r_frame;
	buf_confo_r_frame.size = dpe_test_case_02_frame_0_golden_dpe_confo_r_size;
	printf("buf_confo_r_frame.size:%d",buf_confo_r_frame.size);
	mpImemDrv->allocVirtBuf(&buf_confo_r_frame);
	mpImemDrv->mapPhyAddr(&buf_confo_r_frame);
	memset( (MUINT8*)(buf_confo_r_frame.virtAddr), 0xffffffff , buf_confo_r_frame.size);
	mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_confo_r_frame);

	IMEM_BUF_INFO buf_respo_l_frame;
	buf_respo_l_frame.size = dpe_test_case_02_frame_0_golden_dpe_respo_l_size;
	printf("buf_respo_l_frame.size:%d",buf_respo_l_frame.size);
	mpImemDrv->allocVirtBuf(&buf_respo_l_frame);
	mpImemDrv->mapPhyAddr(&buf_respo_l_frame);
	memset( (MUINT8*)(buf_respo_l_frame.virtAddr), 0xffffffff , buf_respo_l_frame.size);
	mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_respo_l_frame);

	IMEM_BUF_INFO buf_respo_r_frame;
	buf_respo_r_frame.size = dpe_test_case_02_frame_0_golden_dpe_respo_r_size;
	printf("buf_respo_r_frame.size:%d",buf_respo_r_frame.size);
	mpImemDrv->allocVirtBuf(&buf_respo_r_frame);
	mpImemDrv->mapPhyAddr(&buf_respo_r_frame);
	memset( (MUINT8*)(buf_respo_r_frame.virtAddr), 0xffffffff , buf_respo_r_frame.size);
	mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_respo_r_frame);

	printf("--- dve  input and output  allocate done\n");

#endif
#if WMFE_ENABLE_0
	printf("--- wmfe0  input and output  allocate init\n");
	IMEM_BUF_INFO buf_wmf_imgi_frame_0;
	buf_wmf_imgi_frame_0.size = dpe_test_case_02_frame_0_dpe_wmf_imgi_frame_00_00_0_size;
	printf("buf_wmf_imgi_frame_0.size:%d",buf_wmf_imgi_frame_0.size);
	mpImemDrv->allocVirtBuf(&buf_wmf_imgi_frame_0);
	mpImemDrv->mapPhyAddr(&buf_wmf_imgi_frame_0);
	memcpy( (MUINT8*)(buf_wmf_imgi_frame_0.virtAddr), (MUINT8*)(dpe_test_case_02_frame_0_dpe_wmf_imgi_frame_00_00_0), buf_wmf_imgi_frame_0.size);
	mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wmf_imgi_frame_0);

	IMEM_BUF_INFO buf_wmf_dpi_frame_0;
	buf_wmf_dpi_frame_0.size = dpe_test_case_02_frame_0_dpe_wmf_dpi_frame_00_00_0_size;
	printf("buf_wmf_dpi_frame_0.size:%d",buf_wmf_dpi_frame_0.size);
	mpImemDrv->allocVirtBuf(&buf_wmf_dpi_frame_0);
	mpImemDrv->mapPhyAddr(&buf_wmf_dpi_frame_0);
	memcpy( (MUINT8*)(buf_wmf_dpi_frame_0.virtAddr), (MUINT8*)(dpe_test_case_02_frame_0_dpe_wmf_dpi_frame_00_00_0), buf_wmf_dpi_frame_0.size);
	mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wmf_dpi_frame_0);

	IMEM_BUF_INFO buf_wmf_tbli_frame_0;
	buf_wmf_tbli_frame_0.size = dpe_test_case_02_frame_0_dpe_wmf_tbli_frame_00_00_0_size;
	printf("buf_wmf_tbli_frame_0.size:%d",buf_wmf_tbli_frame_0.size);
	mpImemDrv->allocVirtBuf(&buf_wmf_tbli_frame_0);
	mpImemDrv->mapPhyAddr(&buf_wmf_tbli_frame_0);
	memcpy( (MUINT8*)(buf_wmf_tbli_frame_0.virtAddr), (MUINT8*)(dpe_test_case_02_frame_0_dpe_wmf_tbli_frame_00_00_0), buf_wmf_tbli_frame_0.size);
	mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wmf_tbli_frame_0);

	IMEM_BUF_INFO buf_wmf_dpo_frame_0;
	buf_wmf_dpo_frame_0.size = dpe_test_case_02_frame_0_golden_dpe_wmf_dpo_0_size;
	printf("buf_wmf_dpo_frame_0.size:%d",buf_wmf_dpo_frame_0.size);
	mpImemDrv->allocVirtBuf(&buf_wmf_dpo_frame_0);
	mpImemDrv->mapPhyAddr(&buf_wmf_dpo_frame_0);
	memset( (MUINT8*)(buf_wmf_dpo_frame_0.virtAddr), 0xffffffff , buf_wmf_dpo_frame_0.size);
	mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wmf_dpo_frame_0);

	printf("--- wmfe0  input and output  allocate done\n");
#endif
#if WMFE_ENABLE_1
	printf("--- wmfe1  input and output  allocate init\n");
	IMEM_BUF_INFO buf_wmf_imgi_frame_1;
	buf_wmf_imgi_frame_1.size = dpe_test_case_02_frame_0_dpe_wmf_imgi_frame_00_00_1_size;
	printf("buf_wmf_imgi_frame_1.size:%d",buf_wmf_imgi_frame_1.size);
	mpImemDrv->allocVirtBuf(&buf_wmf_imgi_frame_1);
	mpImemDrv->mapPhyAddr(&buf_wmf_imgi_frame_1);
	memcpy( (MUINT8*)(buf_wmf_imgi_frame_1.virtAddr), (MUINT8*)(dpe_test_case_02_frame_0_dpe_wmf_imgi_frame_00_00_1), buf_wmf_imgi_frame_1.size);
	mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wmf_imgi_frame_1);

	IMEM_BUF_INFO buf_wmf_dpi_frame_1;
	buf_wmf_dpi_frame_1.size = dpe_test_case_02_frame_0_dpe_wmf_dpi_frame_00_00_1_size;
	printf("buf_wmf_dpi_frame_1.size:%d",buf_wmf_dpi_frame_1.size);
	mpImemDrv->allocVirtBuf(&buf_wmf_dpi_frame_1);
	mpImemDrv->mapPhyAddr(&buf_wmf_dpi_frame_1);
	memcpy( (MUINT8*)(buf_wmf_dpi_frame_1.virtAddr), (MUINT8*)(dpe_test_case_02_frame_0_dpe_wmf_dpi_frame_00_00_1), buf_wmf_dpi_frame_1.size);
	mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wmf_dpi_frame_1);

	IMEM_BUF_INFO buf_wmf_tbli_frame_1;
	buf_wmf_tbli_frame_1.size = dpe_test_case_02_frame_0_dpe_wmf_tbli_frame_00_00_1_size;
	printf("buf_wmf_tbli_frame_1.size:%d",buf_wmf_tbli_frame_1.size);
	mpImemDrv->allocVirtBuf(&buf_wmf_tbli_frame_1);
	mpImemDrv->mapPhyAddr(&buf_wmf_tbli_frame_1);
	memcpy( (MUINT8*)(buf_wmf_tbli_frame_1.virtAddr), (MUINT8*)(dpe_test_case_02_frame_0_dpe_wmf_tbli_frame_00_00_1), buf_wmf_tbli_frame_1.size);
	mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wmf_tbli_frame_1);

	IMEM_BUF_INFO buf_wmf_dpo_frame_1;
	buf_wmf_dpo_frame_1.size = dpe_test_case_02_frame_0_golden_dpe_wmf_dpo_1_size;
	printf("buf_wmf_dpo_frame_1.size:%d",buf_wmf_dpo_frame_1.size);
	mpImemDrv->allocVirtBuf(&buf_wmf_dpo_frame_1);
	mpImemDrv->mapPhyAddr(&buf_wmf_dpo_frame_1);
	memset( (MUINT8*)(buf_wmf_dpo_frame_1.virtAddr), 0xffffffff , buf_wmf_dpo_frame_1.size);
	mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wmf_dpo_frame_1);

	printf("--- wmfe1  input and output  allocate done\n");
#endif
#if WMFE_ENABLE_2
	printf("--- wmfe2  input and output  allocate init\n");
	IMEM_BUF_INFO buf_wmf_imgi_frame_2;
	buf_wmf_imgi_frame_2.size = dpe_test_case_02_frame_0_dpe_wmf_imgi_frame_00_00_2_size;
	printf("buf_wmf_imgi_frame_2.size:%d",buf_wmf_imgi_frame_2.size);
	mpImemDrv->allocVirtBuf(&buf_wmf_imgi_frame_2);
	mpImemDrv->mapPhyAddr(&buf_wmf_imgi_frame_2);
	memcpy( (MUINT8*)(buf_wmf_imgi_frame_2.virtAddr), (MUINT8*)(dpe_test_case_02_frame_0_dpe_wmf_imgi_frame_00_00_2), buf_wmf_imgi_frame_2.size);
	mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wmf_imgi_frame_2);

	IMEM_BUF_INFO buf_wmf_dpi_frame_2;
	buf_wmf_dpi_frame_2.size = dpe_test_case_02_frame_0_dpe_wmf_dpi_frame_00_00_2_size;
	printf("buf_wmf_dpi_frame_2.size:%d",buf_wmf_dpi_frame_2.size);
	mpImemDrv->allocVirtBuf(&buf_wmf_dpi_frame_2);
	mpImemDrv->mapPhyAddr(&buf_wmf_dpi_frame_2);
	memcpy( (MUINT8*)(buf_wmf_dpi_frame_2.virtAddr), (MUINT8*)(dpe_test_case_02_frame_0_dpe_wmf_dpi_frame_00_00_2), buf_wmf_dpi_frame_2.size);
	mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wmf_dpi_frame_2);

	IMEM_BUF_INFO buf_wmf_tbli_frame_2;
	buf_wmf_tbli_frame_2.size = dpe_test_case_02_frame_0_dpe_wmf_tbli_frame_00_00_2_size;
	printf("buf_wmf_tbli_frame_2.size:%d",buf_wmf_tbli_frame_2.size);
	mpImemDrv->allocVirtBuf(&buf_wmf_tbli_frame_2);
	mpImemDrv->mapPhyAddr(&buf_wmf_tbli_frame_2);
	memcpy( (MUINT8*)(buf_wmf_tbli_frame_2.virtAddr), (MUINT8*)(dpe_test_case_02_frame_0_dpe_wmf_tbli_frame_00_00_2), buf_wmf_tbli_frame_2.size);
	mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wmf_tbli_frame_2);

	IMEM_BUF_INFO buf_wmf_dpo_frame_2;
	buf_wmf_dpo_frame_2.size = dpe_test_case_02_frame_0_golden_dpe_wmf_dpo_2_size;
	printf("buf_wmf_dpo_frame_2.size:%d",buf_wmf_dpo_frame_2.size);
	mpImemDrv->allocVirtBuf(&buf_wmf_dpo_frame_2);
	mpImemDrv->mapPhyAddr(&buf_wmf_dpo_frame_2);
	memset( (MUINT8*)(buf_wmf_dpo_frame_2.virtAddr), 0xffffffff , buf_wmf_dpo_frame_2.size);
	mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_wmf_dpo_frame_2);

	printf("--- wmfe2  input and output  allocate done\n");
#endif
#if DVE_ENABLE
	printf("golden_dpe_dvo_l_size:%d, golden_dpe_dvo_r_size:%d\n", dpe_test_case_02_frame_0_golden_dpe_dvo_l_size, dpe_test_case_02_frame_0_golden_dpe_dvo_r_size);
	printf("golden_dpe_confo_l_size:%d, golden_dpe_confo_r_size:%d\n",dpe_test_case_02_frame_0_golden_dpe_confo_l_size , dpe_test_case_02_frame_0_golden_dpe_confo_r_size);
	printf("golden_dpe_respo_l_size:%d, golden_dpe_respo_r_size:%d\n",dpe_test_case_02_frame_0_golden_dpe_respo_l_size , dpe_test_case_02_frame_0_golden_dpe_respo_r_size);

#endif

	int ret=0;
#if DVE_ENABLE
	DVEParams rDveParams;
	DVEConfig dveconfig;
	rDveParams.mpfnCallback = DPE_dpe_test_case_02_frame_0DVECallback;

#if 0
	dveconfig.Dve_Skp_Pre_Dv = ;
	dveconfig.Dve_Mask_En = ;
	dveconfig.Dve_l_Bbox_En = ;
	dveconfig.Dve_r_Bbox_En = ;
	dveconfig.Dve_Horz_Ds_Mode = ;
	dveconfig.Dve_Vert_Ds_Mode = ;
	dveconfig.Dve_Imgi_l_Fmt = ;
	dveconfig.Dve_Imgi_r_Fmt = ;
	dveconfig.Dve_Org_l_Bbox.DVE_ORG_BBOX_RIGHT = ;
	dveconfig.Dve_Org_l_Bbox.DVE_ORG_BBOX_LEFT = ;
	dveconfig.Dve_Org_l_Bbox.DVE_ORG_BBOX_BOTTOM = ;
	dveconfig.Dve_Org_l_Bbox.DVE_ORG_BBOX_TOP = ;
	dveconfig.Dve_Org_r_Bbox.DVE_ORG_BBOX_RIGHT = ;
	dveconfig.Dve_Org_r_Bbox.DVE_ORG_BBOX_LEFT = ;
	dveconfig.Dve_Org_r_Bbox.DVE_ORG_BBOX_BOTTOM = ;
	dveconfig.Dve_Org_r_Bbox.DVE_ORG_BBOX_TOP = ;
	dveconfig.Dve_Org_Width = ;
	dveconfig.Dve_Org_Height = ;
	dveconfig.Dve_Org_Horz_Sr_1 = ;
	dveconfig.Dve_Org_Horz_Sr_0 = ;
	dveconfig.Dve_Org_Vert_Sr_0 = ;
	dveconfig.Dve_Org_Start_Vert_Sv = ;
	dveconfig.Dve_Org_Start_Horz_Sv = ;
	dveconfig.Dve_Cand_Num = ;


	dveconfig.Dve_Cand_0.DVE_CAND_SEL = ;
	dveconfig.Dve_Cand_0.DVE_CAND_TYPE = ;
	dveconfig.Dve_Cand_1.DVE_CAND_SEL = ;
	dveconfig.Dve_Cand_1.DVE_CAND_TYPE = ;
	dveconfig.Dve_Cand_2.DVE_CAND_SEL = ;
	dveconfig.Dve_Cand_2.DVE_CAND_TYPE = ;
	dveconfig.Dve_Cand_3.DVE_CAND_SEL = ;
	dveconfig.Dve_Cand_3.DVE_CAND_TYPE = ;

	dveconfig.Dve_Cand_4.DVE_CAND_SEL = ;
	dveconfig.Dve_Cand_4.DVE_CAND_TYPE = ;
	dveconfig.Dve_Cand_5.DVE_CAND_SEL = ;
	dveconfig.Dve_Cand_5.DVE_CAND_TYPE = ;
	dveconfig.Dve_Cand_6.DVE_CAND_SEL = ;
	dveconfig.Dve_Cand_6.DVE_CAND_TYPE = ;
	dveconfig.Dve_Cand_7.DVE_CAND_SEL = ;
	dveconfig.Dve_Cand_7.DVE_CAND_TYPE = ;
	dveconfig.Dve_Rand_Lut_0 = ;
	dveconfig.Dve_Rand_Lut_1 = ;
	dveconfig.Dve_Rand_Lut_2 = ;
	dveconfig.Dve_Rand_Lut_3 = ;
	dveconfig.DVE_VERT_GMV = ;
	dveconfig.DVE_HORZ_GMV = ;
	dveconfig.Dve_Horz_Dv_Ini = ;
	dveconfig.Dve_Coft_Shift = ;
	dveconfig.Dve_Corner_Th = ;
	dveconfig.Dve_Smth_Luma_Th_1 = ;
	dveconfig.Dve_Smth_Luma_Th_0 = ;
	dveconfig.Dve_Smth_Luma_Ada_Base = ;
	dveconfig.Dve_Smth_Luma_Horz_Pnlty_Sel = ;

	dveconfig.Dve_Smth_Dv_Mode = ;
	dveconfig.Dve_Smth_Dv_Horz_Pnlty_Sel = ;
	dveconfig.Dve_Smth_Dv_Vert_Pnlty_Sel = ;
	dveconfig.Dve_Smth_Dv_Ada_Base = ;
	dveconfig.Dve_Smth_Dv_Th_0 = ;
	dveconfig.Dve_Smth_Dv_Th_1 = ;
	dveconfig.Dve_Ord_Th = ;
	dveconfig.Dve_Ord_Coring = ;
	dveconfig.Dve_Ord_Pnlty_Sel = ;
	dveconfig.Dve_Type_Penality_Ctrl.DVE_REFINE_PNLTY_SEL = ;
	dveconfig.Dve_Type_Penality_Ctrl.DVE_GMV_PNLTY_SEL = ;
	dveconfig.Dve_Type_Penality_Ctrl.DVE_PREV_PNLTY_SEL = ;
	dveconfig.Dve_Type_Penality_Ctrl.DVE_NBR_PNLTY_SEL = ;
	dveconfig.Dve_Type_Penality_Ctrl.DVE_RAND_PNLTY_SEL = ;
	dveconfig.Dve_Type_Penality_Ctrl.DVE_TMPR_PNLTY_SEL = ;
	dveconfig.Dve_Type_Penality_Ctrl.DVE_SPTL_PNLTY_SEL = ;
	dveconfig.Dve_Type_Penality_Ctrl.DVE_RAND_COST = ;
	dveconfig.Dve_Type_Penality_Ctrl.DVE_GMV_COST = ;
	dveconfig.Dve_Type_Penality_Ctrl.DVE_PREV_COST = ;
	dveconfig.Dve_Type_Penality_Ctrl.DVE_NBR_COST = ;
	dveconfig.Dve_Type_Penality_Ctrl.DVE_REFINE_COST = ;
	dveconfig.Dve_Type_Penality_Ctrl.DVE_TMPR_COST = ;
	dveconfig.Dve_Type_Penality_Ctrl.DVE_SPTL_COST = ;
	dveconfig.Dve_Imgi_l.u4BufVA = buf_imgi_l_frame.virtAddr;
	dveconfig.Dve_Imgi_l.u4BufPA = buf_imgi_l_frame.phyAddr;
	dveconfig.Dve_Imgi_l.u4Stride = ;
	dveconfig.Dve_Imgi_r.u4BufVA = buf_imgi_r_frame.virtAddr;
	dveconfig.Dve_Imgi_r.u4BufPA = buf_imgi_r_frame.phyAddr;
	dveconfig.Dve_Imgi_r.u4Stride = ;

	dveconfig.Dve_Dvi_l.u4BufVA = buf_dvi_l_frame.virtAddr;
	dveconfig.Dve_Dvi_l.u4BufPA = buf_dvi_l_frame.phyAddr;
	dveconfig.Dve_Dvi_l.u4Stride = ;
	dveconfig.Dve_Dvi_r.u4BufVA = buf_dvi_r_frame.virtAddr;
	dveconfig.Dve_Dvi_r.u4BufPA = buf_dvi_r_frame.phyAddr;
	dveconfig.Dve_Dvi_r.u4Stride = ;

	dveconfig.Dve_Maski_l.u4BufVA = buf_maski_l_frame.virtAddr;
	dveconfig.Dve_Maski_l.u4BufPA = buf_maski_l_frame.phyAddr;
	dveconfig.Dve_Maski_l.u4Stride = ;
	dveconfig.Dve_Maski_r.u4BufVA = buf_maski_r_frame.virtAddr;
	dveconfig.Dve_Maski_r.u4BufPA = buf_maski_r_frame.phyAddr;
	dveconfig.Dve_Maski_r.u4Stride = ;


	dveconfig.Dve_Dvo_l.u4BufVA = buf_dvo_l_frame.virtAddr;
	dveconfig.Dve_Dvo_l.u4BufPA = buf_dvo_l_frame.phyAddr;
	dveconfig.Dve_Dvo_l.u4Stride = ;
	dveconfig.Dve_Dvo_r.u4BufVA = buf_dvo_r_frame.virtAddr;
	dveconfig.Dve_Dvo_r.u4BufPA = buf_dvo_r_frame.phyAddr;
	dveconfig.Dve_Dvo_r.u4Stride = ;

	dveconfig.Dve_Confo_l.u4BufVA = buf_confo_l_frame.virtAddr;
	dveconfig.Dve_Confo_l.u4BufPA = buf_confo_l_frame.phyAddr;
	dveconfig.Dve_Confo_l.u4Stride = ;
	dveconfig.Dve_Confo_r.u4BufVA = buf_confo_r_frame.virtAddr;
	dveconfig.Dve_Confo_r.u4BufPA = buf_confo_r_frame.phyAddr;
	dveconfig.Dve_Confo_r.u4Stride = ;

	dveconfig.Dve_Respo_l.u4BufVA = buf_respo_l_frame.virtAddr;
	dveconfig.Dve_Respo_l.u4BufPA = buf_respo_l_frame.phyAddr;
	dveconfig.Dve_Respo_l.u4Stride = ;
	dveconfig.Dve_Respo_r.u4BufVA = buf_respo_r_frame.virtAddr;
	dveconfig.Dve_Respo_r.u4BufPA = buf_respo_r_frame.phyAddr;
	dveconfig.Dve_Respo_r.u4Stride = ;



	rDveParams.mDVEConfigVec.push_back(dveconfig);
	g_b_dpe_test_case_02_frame_0DVECallback = MFALSE;
#endif
#endif

	WMFEParams rWmfeParams;
	WMFEConfig wmfeconfig;
	rWmfeParams.mpfnCallback = DPE_dpe_test_case_02_frame_0WMFECallback;
#if 0
#if WMFE_ENABLE_0
	wmfeconfig.Wmfe_Ctrl_0.Wmfe_Enable = ;
	wmfeconfig.Wmfe_Ctrl_0.WmfeFilterSize = ;
	wmfeconfig.Wmfe_Ctrl_0.Wmfe_Width = ;
	wmfeconfig.Wmfe_Ctrl_0.Wmfe_Height = ;
	wmfeconfig.Wmfe_Ctrl_0.WmfeImgiFmt = ;
	wmfeconfig.Wmfe_Ctrl_0.WmfeDpiFmt= ;
	wmfeconfig.Wmfe_Ctrl_0.Wmfe_Imgi.u4BufVA = buf_wmf_imgi_frame_0.virtAddr;
	wmfeconfig.Wmfe_Ctrl_0.Wmfe_Imgi.u4BufPA = buf_wmf_imgi_frame_0.phyAddr;
	wmfeconfig.Wmfe_Ctrl_0.Wmfe_Imgi.u4Stride = ;
	wmfeconfig.Wmfe_Ctrl_0.Wmfe_Dpi.u4BufVA = buf_wmf_dpi_frame_0.virtAddr;
	wmfeconfig.Wmfe_Ctrl_0.Wmfe_Dpi.u4BufPA = buf_wmf_dpi_frame_0.phyAddr;
	wmfeconfig.Wmfe_Ctrl_0.Wmfe_Dpi.u4Stride = ;
	wmfeconfig.Wmfe_Ctrl_0.Wmfe_Tbli.u4BufVA = buf_wmf_tbli_frame_0.virtAddr;
	wmfeconfig.Wmfe_Ctrl_0.Wmfe_Tbli.u4BufPA = buf_wmf_tbli_frame_0.phyAddr;
	wmfeconfig.Wmfe_Ctrl_0.Wmfe_Tbli.u4Stride = ;
	wmfeconfig.Wmfe_Ctrl_0.Wmfe_Dpo.u4BufVA = buf_wmf_dpo_frame_0.virtAddr;
	wmfeconfig.Wmfe_Ctrl_0.Wmfe_Dpo.u4BufPA = buf_wmf_dpo_frame_0.phyAddr;
	wmfeconfig.Wmfe_Ctrl_0.Wmfe_Dpo.u4Stride = ;
#endif
#if WMFE_ENABLE_1
	wmfeconfig.Wmfe_Ctrl_1.Wmfe_Enable = ;
	wmfeconfig.Wmfe_Ctrl_1.WmfeFilterSize = ;
	wmfeconfig.Wmfe_Ctrl_1.Wmfe_Width = ;
	wmfeconfig.Wmfe_Ctrl_1.Wmfe_Height = ;
	wmfeconfig.Wmfe_Ctrl_1.WmfeImgiFmt = ;
	wmfeconfig.Wmfe_Ctrl_1.WmfeDpiFmt= ;
	wmfeconfig.Wmfe_Ctrl_1.Wmfe_Imgi.u4BufVA = buf_wmf_imgi_frame_1.virtAddr;
	wmfeconfig.Wmfe_Ctrl_1.Wmfe_Imgi.u4BufPA = buf_wmf_imgi_frame_1.phyAddr;
	wmfeconfig.Wmfe_Ctrl_1.Wmfe_Imgi.u4Stride = ;
	wmfeconfig.Wmfe_Ctrl_1.Wmfe_Dpi.u4BufVA = buf_wmf_dpi_frame_1.virtAddr;
	wmfeconfig.Wmfe_Ctrl_1.Wmfe_Dpi.u4BufPA = buf_wmf_dpi_frame_1.phyAddr;
	wmfeconfig.Wmfe_Ctrl_1.Wmfe_Dpi.u4Stride = ;
	wmfeconfig.Wmfe_Ctrl_1.Wmfe_Tbli.u4BufVA = buf_wmf_tbli_frame_1.virtAddr;
	wmfeconfig.Wmfe_Ctrl_1.Wmfe_Tbli.u4BufPA = buf_wmf_tbli_frame_1.phyAddr;
	wmfeconfig.Wmfe_Ctrl_1.Wmfe_Tbli.u4Stride = ;
	wmfeconfig.Wmfe_Ctrl_1.Wmfe_Dpo.u4BufVA = buf_wmf_dpo_frame_1.virtAddr;
	wmfeconfig.Wmfe_Ctrl_1.Wmfe_Dpo.u4BufPA = buf_wmf_dpo_frame_1.phyAddr;
	wmfeconfig.Wmfe_Ctrl_1.Wmfe_Dpo.u4Stride = ;
#endif
#if WMFE_ENABLE_2
	wmfeconfig.Wmfe_Ctrl_2.Wmfe_Enable = ;
	wmfeconfig.Wmfe_Ctrl_2.WmfeFilterSize = ;
	wmfeconfig.Wmfe_Ctrl_2.Wmfe_Width = ;
	wmfeconfig.Wmfe_Ctrl_2.Wmfe_Height = ;
	wmfeconfig.Wmfe_Ctrl_2.WmfeImgiFmt = ;
	wmfeconfig.Wmfe_Ctrl_2.WmfeDpiFmt= ;
	wmfeconfig.Wmfe_Ctrl_2.Wmfe_Imgi.u4BufVA = buf_wmf_imgi_frame_2.virtAddr;
	wmfeconfig.Wmfe_Ctrl_2.Wmfe_Imgi.u4BufPA = buf_wmf_imgi_frame_2.phyAddr;
	wmfeconfig.Wmfe_Ctrl_2.Wmfe_Imgi.u4Stride = ;
	wmfeconfig.Wmfe_Ctrl_2.Wmfe_Dpi.u4BufVA = buf_wmf_dpi_frame_2.virtAddr;
	wmfeconfig.Wmfe_Ctrl_2.Wmfe_Dpi.u4BufPA = buf_wmf_dpi_frame_2.phyAddr;
	wmfeconfig.Wmfe_Ctrl_2.Wmfe_Dpi.u4Stride = ;
	wmfeconfig.Wmfe_Ctrl_2.Wmfe_Tbli.u4BufVA = buf_wmf_tbli_frame_2.virtAddr;
	wmfeconfig.Wmfe_Ctrl_2.Wmfe_Tbli.u4BufPA = buf_wmf_tbli_frame_2.phyAddr;
	wmfeconfig.Wmfe_Ctrl_2.Wmfe_Tbli.u4Stride = ;
	wmfeconfig.Wmfe_Ctrl_2.Wmfe_Dpo.u4BufVA = buf_wmf_dpo_frame_2.virtAddr;
	wmfeconfig.Wmfe_Ctrl_2.Wmfe_Dpo.u4BufPA = buf_wmf_dpo_frame_2.phyAddr;
	wmfeconfig.Wmfe_Ctrl_2.Wmfe_Dpo.u4Stride = ;
#endif
#endif
	rWmfeParams.mWMFEConfigVec.push_back(wmfeconfig);
	g_b_dpe_test_case_02_frame_0WMFECallback = MFALSE;
	//enque
	ret=pStream->DVEenque(rDveParams);
	if(!ret)
	{
		printf("---ERRRRRRRRR [dpe_default..dve enque fail\n]");
	}
	else
	{
		printf("---[dpe_default..dve enque done\n]");
	}
	//enque
	ret=pStream->WMFEenque(rWmfeParams);
	if(!ret)
	{
		printf("---ERRRRRRRRR [dpe_default..wmfe enque fail\n]");
	}
	else
	{
		printf("---[dpe_default..wmfe enque done\n]");
	}
#if DVE_ENABLE
	do{
		usleep(100000);
		if (MTRUE == g_b_dpe_test_case_02_frame_0DVECallback)
		{
			break;
		}
	}while(1);
#endif
#if WMFE_ENABLE
	do{
		usleep(100000);
		if (MTRUE == g_b_dpe_test_case_02_frame_0WMFECallback)
		{
			break;
		}
	}while(1);
#endif



    MUINT32 DVE_ORG_WDITH = dveconfig.Dve_Org_Width;
    MUINT32 DVE_ORG_HEIGHT = dveconfig.Dve_Org_Height;
    MUINT32 DVE_HORZ_DS_MODE = dveconfig.Dve_Horz_Ds_Mode;
    MUINT32 DVE_VERT_DS_MODE = dveconfig.Dve_Vert_Ds_Mode;
    int int_data_dma_0, int_data_dma_1, int_data_dma_2, int_data_dma_3;
    MUINT32 blk_width;
    MUINT32 blk_height;
    MUINT32 golden_l_start_blk_x;
    int golden_l_end_blk_x;
    MUINT32 golden_l_start_blk_y;
    int golden_l_end_blk_y;

    MUINT32 golden_r_start_blk_x;
    int golden_r_end_blk_x;
    MUINT32 golden_r_start_blk_y;
    int golden_r_end_blk_y;

    int err_cnt_dma;
    MUINT32 REG_DPE_WMFE_SIZE_0 = (wmfeconfig.Wmfe_Ctrl_0.Wmfe_Height << 16) | wmfeconfig.Wmfe_Ctrl_0.Wmfe_Width;
    MUINT32 REG_DPE_WMFE_SIZE_1 = (wmfeconfig.Wmfe_Ctrl_1.Wmfe_Height << 16) | wmfeconfig.Wmfe_Ctrl_1.Wmfe_Width;
    MUINT32 REG_DPE_WMFE_SIZE_2 = (wmfeconfig.Wmfe_Ctrl_2.Wmfe_Height << 16) | wmfeconfig.Wmfe_Ctrl_2.Wmfe_Width;
    MUINT32 wmfe_curr_width;
    MUINT32 wmfe_curr_height;

    if ( 0 == DVE_HORZ_DS_MODE)
    {
        blk_width = (DVE_ORG_WDITH+7) >> 3;
        golden_l_start_blk_x = (golden_l_start_x >> 3);
        if (golden_l_start_x & 0x7)
        {
            golden_l_start_blk_x += 1;
        }
        golden_l_end_blk_x  = (golden_l_end_x >> 3);
        golden_r_start_blk_x = (golden_r_start_x >> 3);
        if (golden_r_start_x & 0x7)
        {
            golden_r_start_blk_x += 1;
        }
        golden_r_end_blk_x  = (golden_r_end_x >> 3);
    }
    else
    {
        blk_width = (DVE_ORG_WDITH+3) >> 2;
        golden_l_start_blk_x = (golden_l_start_x >> 2);
        if (golden_l_start_x & 0x3)
        {
            golden_l_start_blk_x += 1;
        }
        golden_l_end_blk_x  = (golden_l_end_x >> 2);
        golden_r_start_blk_x = (golden_r_start_x >> 2);
        if (golden_r_start_x & 0x3)
        {
            golden_r_start_blk_x += 1;
        }
        golden_r_end_blk_x  = (golden_r_end_x >> 2);
    }
    
    if ( 0 == DVE_VERT_DS_MODE)
    {
        blk_height = (DVE_ORG_HEIGHT+7) >> 3;
        golden_l_start_blk_y = (golden_l_start_y >> 3);
        if (golden_l_start_y & 0x7)
        {
            golden_l_start_blk_y += 1;
        }
        golden_l_end_blk_y  = (golden_l_end_y >> 3);
        golden_r_start_blk_y = (golden_r_start_y >> 3);
        if (golden_r_start_y & 0x7)
        {
            golden_r_start_blk_y += 1;
        }
        golden_r_end_blk_y  = (golden_r_end_y >> 3);

    }
    else
    {
        blk_height = (DVE_ORG_HEIGHT+3) >> 2;
        golden_l_start_blk_y = (golden_l_start_y >> 2);
        if (golden_l_start_y & 0x3)
        {
            golden_l_start_blk_y += 1;
        }
        golden_l_end_blk_y  = (golden_l_end_y >> 2);
        golden_r_start_blk_y = (golden_r_start_y >> 2);
        if (golden_r_start_y & 0x3)
        {
            golden_r_start_blk_y += 1;
        }
        golden_r_end_blk_y  = (golden_r_end_y >> 2);

    }

    if (golden_l_end_blk_x == blk_width)
        golden_l_end_blk_x = blk_width - 1;
    if (golden_l_end_blk_y == blk_height)
        golden_l_end_blk_y = blk_height - 1; 
    if (golden_r_end_blk_x == blk_width)
        golden_r_end_blk_x = blk_width - 1;
    if (golden_r_end_blk_y == blk_height)
        golden_r_end_blk_y = blk_height - 1; 
        
    //Compare dpe_confo_l_frame_
#if DVE_ENABLE
    int_data_dma_0 = golden_l_start_blk_x;
    int_data_dma_1 = golden_l_start_blk_y;
    if (golden_l_end_x < 0)
    {
        int_data_dma_2 = -1;
    }
    else
    {
        int_data_dma_2 = golden_l_end_blk_x;
    }
    if (golden_l_end_y < 0)
    {
        int_data_dma_3 = -1;
    }
    else
    {
        int_data_dma_3 = golden_l_end_blk_y;
    }       
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_confo_l_frame); 
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_confo_r_frame);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_respo_l_frame); 
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_respo_r_frame);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_dvo_l_frame); 
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_dvo_r_frame);
        
    err_cnt_dma = comp_roi_mem_with_file((char*)golden_dpe_confo_l_frame, 
                          1, 
                          dveconfig.Dve_Confo_l.u4BufVA,
                          blk_width,
                          blk_height,
                          dveconfig.Dve_Confo_l.u4Stride,
                          int_data_dma_0,
                          int_data_dma_1,
                          int_data_dma_2,
                          int_data_dma_3,
                          dveconfig.Dve_Maski_l.u4BufVA,
                          1,
                          dveconfig.Dve_Maski_l.u4Stride
                          );    
    if (err_cnt_dma)
    {
        //Error
        printf("dpe left confo bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
    }
    else
    {
        //Pass
        printf("dpe left confo bit true pass!!!\n");
    }

    //Compare the dpe_respo_l_frame_
    err_cnt_dma = comp_roi_mem_with_file((char*)golden_dpe_respo_l_frame, 
                          1, 
                          dveconfig.Dve_Respo_l.u4BufVA,
                          blk_width,
                          blk_height,
                          dveconfig.Dve_Respo_l.u4Stride,
                          int_data_dma_0,
                          int_data_dma_1,
                          int_data_dma_2,
                          int_data_dma_3,
                          dveconfig.Dve_Maski_l.u4BufVA,
                          1,
                          dveconfig.Dve_Maski_l.u4Stride
                          );    
    if (err_cnt_dma)
    {
        //Error
        printf("dpe left respo bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
    }
    else
    {
        //Pass
        printf("dpe left respo bit true pass!!!\n");
    }



    //Compare dpe_confo_r_frame_
    int_data_dma_0 = golden_r_start_blk_x;
    int_data_dma_1 = golden_r_start_blk_y;
    if (golden_r_end_x < 0)
    {
        int_data_dma_2 = -1;
    }
    else
    {
        int_data_dma_2 = golden_r_end_blk_x;
    }
    if (golden_r_end_y < 0)
    {
        int_data_dma_3 = -1;
    }
    else
    {
        int_data_dma_3 = golden_r_end_blk_y;
    }       
    comp_roi_mem_with_file((char*)golden_dpe_confo_r_frame, 
                          1, 
                          dveconfig.Dve_Confo_r.u4BufVA,
                          blk_width,
                          blk_height,
                          dveconfig.Dve_Confo_r.u4Stride,
                          int_data_dma_0,
                          int_data_dma_1,
                          int_data_dma_2,
                          int_data_dma_3,
                          dveconfig.Dve_Maski_r.u4BufVA,
                          1,
                          dveconfig.Dve_Maski_r.u4Stride
                          ); 
    if (err_cnt_dma)
    {
        //Error
        printf("dpe right confo bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
    }
    else
    {
        //Pass
        printf("dpe right confo bit true pass!!!\n");
    }


    //Compare the dpe_respo_r_frame_
    err_cnt_dma = comp_roi_mem_with_file((char*)golden_dpe_respo_r_frame, 
                          1, 
                          dveconfig.Dve_Respo_r.u4BufVA,
                          blk_width,
                          blk_height,
                          dveconfig.Dve_Respo_r.u4Stride,
                          int_data_dma_0,
                          int_data_dma_1,
                          int_data_dma_2,
                          int_data_dma_3,
                          dveconfig.Dve_Maski_r.u4BufVA,
                          1,
                          dveconfig.Dve_Maski_r.u4Stride
                          );    
    if (err_cnt_dma)
    {
        //Error
        printf("dpe right respo bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
    }
    else
    {
        //Pass
        printf("dpe right respo bit true pass!!!\n");
    }


    //Compare the dpe_dvo_l_frame_
    err_cnt_dma = comp_roi_mem_with_file((char*)golden_dpe_dvo_l_frame, 
                          0, 
                          dveconfig.Dve_Dvo_l.u4BufVA,
                          (blk_width << 1),
                          blk_height,
                          dveconfig.Dve_Dvo_l.u4Stride,
                          0,
                          0,
                          ((blk_width << 1)-1),
                          blk_height-1,
                          dveconfig.Dve_Maski_l.u4BufVA,
                          2,
                          dveconfig.Dve_Maski_l.u4Stride
                          );  
    if (err_cnt_dma)
    {
        //Error
        printf("dpe left dvo bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
    }
    else
    {
        //Pass
        printf("dpe left dvo bit true pass!!!\n");
    }


    //Compare the dpe_dvo_r_frame_
    err_cnt_dma = comp_roi_mem_with_file((char*)golden_dpe_dvo_r_frame, 
                          0, 
                          dveconfig.Dve_Dvo_r.u4BufVA,
                          (blk_width << 1),
                          blk_height,
                          dveconfig.Dve_Dvo_r.u4Stride,
                          0,
                          0,
                          ((blk_width << 1)-1),
                          blk_height-1,
                          dveconfig.Dve_Maski_r.u4BufVA,
                          2,
                          dveconfig.Dve_Maski_r.u4Stride
                          ); 
    if (err_cnt_dma)
    {
        //Error
        printf("dpe right dvo bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
    }
    else
    {
        //Pass
        printf("dpe right dvo bit true pass!!!\n");
    }
#endif

    //Start WMFE DRAM comparison !!
    //Compare dpe_wmf_dpo_frame_ 0
#if WMFE_ENABLE_0
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_wmf_dpo_frame_0);

    int_data_dma_0 = 0;
    int_data_dma_1 = 0;
    wmfe_curr_width = (REG_DPE_WMFE_SIZE_0 & 0xfff);
    wmfe_curr_height = ((REG_DPE_WMFE_SIZE_0 >> 16) & 0xfff);
    int_data_dma_2 = wmfe_curr_width-1;
    int_data_dma_3 = wmfe_curr_height-1;

    err_cnt_dma = comp_roi_mem_with_file((char*)golden_dpe_wmf_dpo_frame_0, 
                          0, 
                          wmfeconfig.Wmfe_Ctrl_0.Wmfe_Dpo.u4BufVA,
                          wmfe_curr_width,
                          wmfe_curr_height,
                          wmfeconfig.Wmfe_Ctrl_0.Wmfe_Dpo.u4Stride,
                          int_data_dma_0,
                          int_data_dma_1,
                          int_data_dma_2,
                          int_data_dma_3,
                          0,
                          0,
                          0
                          ); 
    if (err_cnt_dma)
    {
        //Error
        printf("dpe WMFE DPO Frame0 bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
    }
    else
    {
        //Pass
        printf("dpe WMFE DPO Frame0 bit true pass!!!\n");
    }
#endif

    //Compare dpe_wmf_dpo_frame_ 1
#if WMFE_ENABLE_1
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_wmf_dpo_frame_1);

    int_data_dma_0 = 0;
    int_data_dma_1 = 0;
    wmfe_curr_width = (REG_DPE_WMFE_SIZE_1 & 0xfff);
    wmfe_curr_height = ((REG_DPE_WMFE_SIZE_1 >> 16) & 0xfff);
    int_data_dma_2 = wmfe_curr_width-1;
    int_data_dma_3 = wmfe_curr_height-1;

    err_cnt_dma = comp_roi_mem_with_file((char*)golden_dpe_wmf_dpo_frame_1, 
                          0, 
                          wmfeconfig.Wmfe_Ctrl_1.Wmfe_Dpo.u4BufVA,
                          wmfe_curr_width,
                          wmfe_curr_height,
                          wmfeconfig.Wmfe_Ctrl_1.Wmfe_Dpo.u4Stride,
                          int_data_dma_0,
                          int_data_dma_1,
                          int_data_dma_2,
                          int_data_dma_3,
                          0,
                          0,
                          0
                          ); 
    if (err_cnt_dma)
    {
        //Error
        printf("dpe WMFE DPO Frame1 bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
    }
    else
    {
        //Pass
        printf("dpe WMFE DPO Frame1 bit true pass!!!\n");
    }
#endif

    //Compare dpe_wmf_dpo_frame_ 2
#if WMFE_ENABLE_2
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_wmf_dpo_frame_2);

    int_data_dma_0 = 0;
    int_data_dma_1 = 0;
    wmfe_curr_width = (REG_DPE_WMFE_SIZE_2 & 0xfff);
    wmfe_curr_height = ((REG_DPE_WMFE_SIZE_2 >> 16) & 0xfff);
    int_data_dma_2 = wmfe_curr_width-1;
    int_data_dma_3 = wmfe_curr_height-1;

    err_cnt_dma = comp_roi_mem_with_file((char*)golden_dpe_wmf_dpo_frame_2, 
                          0, 
                          wmfeconfig.Wmfe_Ctrl_2.Wmfe_Dpo.u4BufVA,
                          wmfe_curr_width,
                          wmfe_curr_height,
                          wmfeconfig.Wmfe_Ctrl_2.Wmfe_Dpo.u4Stride,
                          int_data_dma_0,
                          int_data_dma_1,
                          int_data_dma_2,
                          int_data_dma_3,
                          0,
                          0,
                          0
                          );    
    if (err_cnt_dma)
    {
        //Error
        printf("dpe WMFE DPO Frame2 bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
    }
    else
    {
        //Pass
        printf("dpe WMFE DPO Frame2 bit true pass!!!\n");
    }
#endif




#if DVE_ENABLE
	mpImemDrv->freeVirtBuf(&buf_imgi_l_frame);
	mpImemDrv->freeVirtBuf(&buf_imgi_r_frame);
	mpImemDrv->freeVirtBuf(&buf_dvi_l_frame);
	mpImemDrv->freeVirtBuf(&buf_dvi_r_frame);
	mpImemDrv->freeVirtBuf(&buf_maski_l_frame);
	mpImemDrv->freeVirtBuf(&buf_maski_r_frame);
	mpImemDrv->freeVirtBuf(&buf_dvo_l_frame);
	mpImemDrv->freeVirtBuf(&buf_dvo_r_frame);
	mpImemDrv->freeVirtBuf(&buf_confo_l_frame);
	mpImemDrv->freeVirtBuf(&buf_confo_r_frame);
	mpImemDrv->freeVirtBuf(&buf_respo_l_frame);
	mpImemDrv->freeVirtBuf(&buf_respo_r_frame);
#endif
#if WMFE_ENABLE_0
	mpImemDrv->freeVirtBuf(&buf_wmf_imgi_frame_0);
	mpImemDrv->freeVirtBuf(&buf_wmf_dpi_frame_0);
	mpImemDrv->freeVirtBuf(&buf_wmf_tbli_frame_0);
	mpImemDrv->freeVirtBuf(&buf_wmf_dpo_frame_0);
#endif
#if WMFE_ENABLE_1
	mpImemDrv->freeVirtBuf(&buf_wmf_imgi_frame_1);
	mpImemDrv->freeVirtBuf(&buf_wmf_dpi_frame_1);
	mpImemDrv->freeVirtBuf(&buf_wmf_tbli_frame_1);
	mpImemDrv->freeVirtBuf(&buf_wmf_dpo_frame_1);
#endif
#if WMFE_ENABLE_2
	mpImemDrv->freeVirtBuf(&buf_wmf_imgi_frame_2);
	mpImemDrv->freeVirtBuf(&buf_wmf_dpi_frame_2);
	mpImemDrv->freeVirtBuf(&buf_wmf_tbli_frame_2);
	mpImemDrv->freeVirtBuf(&buf_wmf_dpo_frame_2);
#endif

	return true;
}
