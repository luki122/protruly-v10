package com.android.server.hb;

import java.util.List;

import com.android.server.am.ActivityManagerService;

import android.app.hb.ITMSManager;
import android.os.Build;
import android.os.IBinder;
import android.os.RemoteException;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.app.hb.CodeNameInfo;
import android.app.hb.ITrafficCorrectListener;
import android.os.SystemProperties;
/**
 * hummmingbird luolaigang add for TMSManager 20170330
 * @author luolaigang
 *
 */
public class TMSService extends ITMSManager.Stub {
	private static final String TAG = "TMSManagerService";
	private Context mContext;
	public static final String TMS_SERVICE_ACTION = "android.intent.action.TMSService";
    private ServiceConnectionLocation conn = new ServiceConnectionLocation();
    
	public TMSService(Context context) {
		mContext = context;		
	}
	
    public static Intent getExplicitIntent(Context context, Intent implicitIntent) {
        // Retrieve all services that can match the given intent
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> resolveInfo = pm.queryIntentServices(implicitIntent, 0);
        // Make sure only one match was found
        if (resolveInfo == null || resolveInfo.size() != 1) {
            return null;
        }
        // Get component info and create ComponentName
        ResolveInfo serviceInfo = resolveInfo.get(0);
        String packageName = serviceInfo.serviceInfo.packageName;
        String className = serviceInfo.serviceInfo.name;
        ComponentName component = new ComponentName(packageName, className);
        // Create a new intent. Use the old one for extras and such reuse
        Intent explicitIntent = new Intent(implicitIntent);
        // Set the component to be explicit
        explicitIntent.setComponent(component);
        return explicitIntent;
    }
    static final boolean IS_USERDEBUG_BUILD = "userdebug".equals(Build.TYPE);
	public void systemReady() {
		Intent mIntent = new Intent();
        mIntent.setAction(TMS_SERVICE_ACTION);
        Intent eintent = new Intent(getExplicitIntent(mContext,mIntent));
        mContext.bindService(eintent, conn, Context.BIND_AUTO_CREATE);

        //luolaigang add for mtklog
        new Thread(){
        	public void run() {
        		while(!ActivityManagerService.isSystemReady()){
        	        try {
						sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
        		}        		
        		
        		//打开日志
        		if(SystemProperties.get("ro.sw.dui", "false").equals("yes")){
        			Intent mtkIntent = new Intent("com.mediatek.mtklogger.ADB_CMD");
        	        mtkIntent.putExtra("cmd_name", "start");
        	        mtkIntent.putExtra("cmd_target", 7);
        	        mContext.sendBroadcast(mtkIntent);
        		}
    	        /*mtkIntent = new Intent("com.mediatek.mtklogger.ADB_CMD");
    	        mtkIntent.putExtra("cmd_name", "set_auto_start_1");
    	        mtkIntent.putExtra("mtkIntent", 7);
    	        mContext.sendBroadcast(mtkIntent);*/
        		
        		if(IS_USERDEBUG_BUILD){
        			SystemProperties.set("persist.sys.bugreportswitch", "off");
        		}
        	};
        }.start();
	}

	public String getLocation(String number) {
		return conn.getLocation(number);
	}

    /**
     * add by zhaolaichao
     * @return
     */
    public List<CodeNameInfo> getAllProvinces() {
        try {
            return conn.getAllProvinces();
        } catch (Exception e) {
            Log.e(TAG, "[getValue] RemoteException");
        }
        return null;
    }
    /**
     *add by zhaolaichao
     * @return
     */
    public List<CodeNameInfo> getCities(String provinceCode) {
        try {
            return conn.getCities(provinceCode);
        } catch (Exception e) {
            Log.e(TAG, "[getValue] RemoteException");
        }
        return null;
    }
    /**
     *add by zhaolaichao
     * @return
     */
    public List<CodeNameInfo> getCarries() {
        try {
            return conn.getCarries();
        } catch (Exception e) {
            Log.e(TAG, "[getValue] RemoteException");
        }
        return null;
    }
    /**
     *add by zhaolaichao
     * @return
     */
    public List<CodeNameInfo> getBrands(String carryId) {
        try {
            return conn.getBrands(carryId);
        } catch (Exception e) {
            Log.e(TAG, "[getValue] RemoteException");
        }
        return null;
    }
    /**
     *add by zhaolaichao
     * @return
     */
    public int setConfig(int simIndex, String provinceId, String cityId, String carryId, String brandId, int closingDay) {
        try {
            return conn.setConfig(simIndex, provinceId, cityId, carryId, brandId, closingDay);
        } catch (Exception e) {
            Log.e(TAG, "[getValue] RemoteException");
        }
        return -1;
    }
    /**
     *add by zhaolaichao
     * @return
     */
    public int startCorrection(int simIndex) {
        try {
            return conn.startCorrection(simIndex);
        } catch (Exception e) {
            Log.e(TAG, "[getValue] RemoteException");
        }
        return -1;
    }
    /**
     *add by zhaolaichao
     * @return
     */
    public int analysisSMS(int simIndex, String queryCode, String queryPort, String smsBody) {
        try {
            return conn.analysisSMS(simIndex, queryCode, queryPort, smsBody);
        } catch (Exception e) {
            Log.e(TAG, "[getValue] RemoteException");
        }
        return -1;
    }
    /**
     *add by zhaolaichao
     * @return
     */
    public int[] getTrafficInfo(int simIndex) {
        try {
            return conn.getTrafficInfo(simIndex);
        } catch (Exception e) {
            Log.e(TAG, "[getValue] RemoteException");
        }
        return null;
    }

    /**
     *add by zhaolaichao
     * @return
     */
    public void trafficCorrectListener(ITrafficCorrectListener listener) {
        try {
            conn.trafficCorrectListener(listener);
        } catch (Exception e) {
            Log.e(TAG, "[getValue] RemoteException");
        }
    }

	class ServiceConnectionLocation implements ServiceConnection{
		private ITMSManager mTMSManager;
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
        	mTMSManager = ITMSManager.Stub.asInterface(service);
        }
        
        public String getLocation(String number){
        	String location = null;
        	try {
        		location = mTMSManager.getLocation(number);
            } catch (RemoteException e) {
                e.printStackTrace();
                systemReady();
            }
        	return location;
        }

        /**
         * add by zhaolaichao
         * @return
         */
        public List<CodeNameInfo> getAllProvinces() {
            try {
                return mTMSManager.getAllProvinces();
            } catch (RemoteException e) {
                e.printStackTrace();
                Log.e(TAG, "ServiceConnectionLocation() -> getAllProvinces RemoteException");
            }
            return null;
        }
        /**
         *add by zhaolaichao
         * @return
         */
        public List<CodeNameInfo> getCities(String provinceCode) {
            try {
                return mTMSManager.getCities(provinceCode);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            return null;
        }
        /**
         *add by zhaolaichao
         * @return
         */
        public List<CodeNameInfo> getCarries() {
            try {
                return mTMSManager.getCarries();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        /**
         *add by zhaolaichao
         * @return
         */
        public List<CodeNameInfo> getBrands(String carryId) {
            try {
                return mTMSManager.getBrands(carryId);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        /**
         *add by zhaolaichao
         * @return
         */
        public int setConfig(int simIndex, String provinceId, String cityId, String carryId, String brandId, int closingDay) {
            try {
                return mTMSManager.setConfig(simIndex, provinceId, cityId, carryId, brandId, closingDay);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return -1;
        }
        /**
         *add by zhaolaichao
         * @return
         */
        public int startCorrection(int simIndex) {
            try {
                return mTMSManager.startCorrection(simIndex);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            return -1;
        }
        /**
         *add by zhaolaichao
         * @return
         */
        public int analysisSMS(int simIndex, String queryCode, String queryPort, String smsBody) {
            try {
                return mTMSManager.analysisSMS(simIndex, queryCode, queryPort, smsBody);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            return -1;
        }
        /**
         *add by zhaolaichao
         * @return
         */
        public int[] getTrafficInfo(int simIndex) {
            try {
                return mTMSManager.getTrafficInfo(simIndex);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            return null;
        }

        /**
         *add by zhaolaichao
         * @return
         */
        public void trafficCorrectListener(ITrafficCorrectListener listener) {
            try {
                mTMSManager.trafficCorrectListener(listener);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    
	}
}