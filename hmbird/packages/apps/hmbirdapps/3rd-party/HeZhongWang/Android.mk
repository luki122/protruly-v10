LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := HeZhong
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED

LOCAL_MODULE_PATH := $(TARGET_OUT)/vendor/operator/app

LOCAL_MULTILIB := 64

LOCAL_PREBUILT_JNI_LIBS :=  \
        lib/arm64-v8a/libBaiduMapSDK_base_v4_0_0.so \
        lib/arm64-v8a/libBaiduMapSDK_map_v4_0_0.so \
        lib/arm64-v8a/libBaiduMapSDK_search_v4_0_0.so \
        lib/arm64-v8a/libBaiduMapSDK_util_v4_0_0.so \
        lib/arm64-v8a/libenc.so \
        lib/arm64-v8a/libffmpeg.so \
        lib/arm64-v8a/libimagepipeline.so \
        lib/arm64-v8a/libjpush217.so \
        lib/arm64-v8a/liblocSDK6a.so \
        lib/arm64-v8a/libOMX.24.so \
        lib/arm64-v8a/libstlport_shared.so \
        lib/arm64-v8a/libvao.0.so \
        lib/arm64-v8a/libvplayer.so \
        lib/arm64-v8a/libvscanner.so \
        lib/arm64-v8a/libvvo.0.so \
        lib/arm64-v8a/libvvo.9.so \
        lib/arm64-v8a/libyuv.so \
        lib/arm64-v8a/libzbar.so
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

include $(BUILD_PREBUILT)

