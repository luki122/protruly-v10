/*
* Copyright (C) 2014 MediaTek Inc.
* Modification based on code covered by the mentioned copyright
* and/or permission notice(s).
*/

/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.providers.media.music;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Locale;

import com.android.providers.media.R;

import android.app.Activity;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.audiofx.AudioEffect;
import android.net.Uri;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.os.storage.StorageManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telephony.TelephonyManager;
//Not Required
//import android.text.TextUtils;
import android.text.format.Time;

//Not Required
//import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import android.telephony.TelephonyManager;

public class MusicUtils {
	
	public static final String SDCARD_STATUS_UPDATE = "com.android.music.sdcardstatusupdate";

    /*  Try to use String.format() as little as possible, because it creates a
     *  new Formatter every time you call it, which is very inefficient.
     *  Reusing an existing Formatter more than tripled the speed of
     *  makeTimeString().
     *  This Formatter/StringBuilder are also used by makeAlbumSongsLabel()
     */
    private static StringBuilder sFormatBuilder = new StringBuilder();
    private static Formatter sFormatter = new Formatter(sFormatBuilder, Locale.getDefault());
    private static final Object[] sTimeArgs = new Object[5];

    public static String makeTimeString(Context context, long secs) {
        String durationformat = context.getString(
                secs < 3600 ? R.string.durationformatshort : R.string.durationformatlong);

        /* Provide multiple arguments so the format can be changed easily
         * by modifying the xml.
         */
        sFormatBuilder.setLength(0);

        final Object[] timeArgs = sTimeArgs;
        timeArgs[0] = secs / 3600;
        timeArgs[1] = secs / 60;
        timeArgs[2] = (secs / 60) % 60;
        timeArgs[3] = secs;
        timeArgs[4] = secs % 60;
        /// M: use local format
        return sFormatter.format(Locale.getDefault(), durationformat, timeArgs).toString();
    }
    
    public static void hideDatabaseError(Activity a) {
        /// M: Disappear sdcard error. {@
        View v = a.findViewById(R.id.sd_error);
        if (v != null) {
            v.setVisibility(View.GONE);
        }
        /// @}
        v = a.findViewById(R.id.sd_message);
        if (v != null) {
            v.setVisibility(View.GONE);
        }
        v = a.findViewById(R.id.sd_icon);
        if (v != null) {
            v.setVisibility(View.GONE);
        }
        v = a.findViewById(android.R.id.list);
        if (v != null) {
            v.setVisibility(View.VISIBLE);
        }
    }
    
    public static void setSpinnerState(Activity a) {
        if (isMediaScannerScanning(a)) {
            /// M: start the progress spinner
            a.setProgressBarIndeterminateVisibility(true);
        } else {
            /// M: stop the progress spinner
            a.setProgressBarIndeterminateVisibility(false);
        }
    }
    
    public static boolean isMediaScannerScanning(Context context) {
        boolean result = false;
        Cursor cursor = query(context, MediaStore.getMediaScannerUri(),
                new String [] { MediaStore.MEDIA_SCANNER_VOLUME }, null, null, null);
        if (cursor != null) {
            result = cursor.getCount() > 0;
            cursor.close();
        }

        return result;
    }
    
    public static Cursor query(Context context, Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder, int limit) {
        try {
            ContentResolver resolver = context.getContentResolver();
            if (resolver == null) {
                return null;
            }
            if (limit > 0) {
                uri = uri.buildUpon().appendQueryParameter("limit", "" + limit).build();
            }
            return resolver.query(uri, projection, selection, selectionArgs, sortOrder);
         } catch (UnsupportedOperationException ex) {
            return null;
        }

    }
    public static Cursor query(Context context, Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        return query(context, uri, projection, selection, selectionArgs, sortOrder, 0);
    }
    
    static void emptyShow(ListView list, Activity a) {
        if (list.getCount() == 0) {
            View scanView = a.findViewById(R.id.scan);
            scanView.setVisibility(View.VISIBLE);
            TextView text = (TextView) a.findViewById(R.id.message);
            if (MusicUtils.isMediaScannerScanning(a)) {
                a.setProgressBarIndeterminateVisibility(false);
                text.setText(R.string.scanning);
                scanView.findViewById(R.id.spinner).setVisibility(View.VISIBLE);
                scanView.findViewById(R.id.message).setVisibility(View.VISIBLE);
            } else if (hasMountedSDcard(a.getApplicationContext())) {
                text.setText(R.string.no_music_title);
                scanView.findViewById(R.id.spinner).setVisibility(View.GONE);
                scanView.findViewById(R.id.message).setVisibility(View.VISIBLE);
            } else {
                scanView.findViewById(R.id.message).setVisibility(View.GONE);
            }
            list.setEmptyView(scanView);
        } else {
            if (isMediaScannerScanning(a)) {
                a.setProgressBarIndeterminateVisibility(true);
            } else {
                a.setProgressBarIndeterminateVisibility(false);
            }
        }
    }

    static boolean hasMountedSDcard(Context context) {
        StorageManager storageManager = (StorageManager) context.getSystemService(
                Context.STORAGE_SERVICE);
        boolean hasMountedSDcard = false;
        if (storageManager != null) {
            String[] volumePath = storageManager.getVolumePaths();
            if (volumePath != null) {
                String status = null;
                int length = volumePath.length;
                for (int i = 0; i < length; i++) {
                    status = storageManager.getVolumeState(volumePath[i]);
                    if (Environment.MEDIA_MOUNTED.equals(status)) {
                        hasMountedSDcard = true;
                    }
                }
            }
        }
        return hasMountedSDcard;
    }
    
    public interface Defs {
        public final static int OPEN_URL = 0;
        public final static int ADD_TO_PLAYLIST = 1;
        public final static int USE_AS_RINGTONE = 2;
        public final static int PLAYLIST_SELECTED = 3;
        public final static int NEW_PLAYLIST = 4;
        public final static int PLAY_SELECTION = 5;
        public final static int GOTO_START = 6;
        public final static int GOTO_PLAYBACK = 7;
        public final static int PARTY_SHUFFLE = 8;
        public final static int SHUFFLE_ALL = 9;
        public final static int DELETE_ITEM = 10;
        public final static int SCAN_DONE = 11;
        public final static int QUEUE = 12;
        public final static int EFFECTS_PANEL = 13;
        /// M: add for send fm transmitter
        public final static int FM_TRANSMITTER = 14;
        /// M: add for drm
        public final static int DRM_INFO = 15;
        public final static int CHILD_MENU_BASE = 16; // this should be the last item
        /**M: Add Hotknot menu.@{**/
        public final static int HOTKNOT = CHILD_MENU_BASE + 10;
        /**@}**/
    }
}
