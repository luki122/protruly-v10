LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

sinclude $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/mtkcam.mk
LOCAL_ARM_MODE := arm
LOCAL_PRELINK_MODULE := false

LOCAL_SRC_FILES:= Meta_CCAP_Para.cpp
$(info MTK_PATH_SOURCE = $(MTK_PATH_SOURCE))
LOCAL_C_INCLUDES += \
    $(TOP)/$(MTKCAM_C_INCLUDES)/.. \
    $(TOP)/$(MTKCAM_C_INCLUDES) \
    $(MTK_PATH_SOURCE)/hardware/meta/common/inc \
    $(MTK_PATH_SOURCE)/hardware/mtkcam/acdk/common/include/ \
    $(MTK_PATH_SOURCE)/hardware/mtkcam/acdk/common/include/acdk/ \
    $(MTK_PATH_SOURCE)/hardware/mtkcam/acdk/$(shell echo $(MTK_PLATFORM) | tr A-Z a-z)/acdk/inc/cct \
    $(MTK_PATH_SOURCE)/hardware/mtkcam/acdk/$(shell echo $(MTK_PLATFORM) | tr A-Z a-z)/acdk/inc/acdk \
    $(MTK_PATH_SOURCE)/hardware/mtkcam/acdk/$(shell echo $(MTK_PLATFORM) | tr A-Z a-z)/acdk/inc \
    $(MTK_PATH_SOURCE)/hardware/mtkcam/acdk/$(shell echo $(MTK_PLATFORM) | tr A-Z a-z)/inc/cct \
    $(MTK_PATH_SOURCE)/hardware/mtkcam/acdk/$(shell echo $(MTK_PLATFORM) | tr A-Z a-z)/include \
    $(MTK_PATH_SOURCE)/external/mhal/src/custom/inc \
    $(MTK_PATH_SOURCE)/external/mhal/inc \
    $(MTK_PATH_COMMON)/kernel/imgsensor/inc \
    $(MTK_PATH_CUSTOM)/hal/inc \
    $(MTK_PATH_CUSTOM_PLATFORM)/hal/inc \
    $(MTK_PATH_CUSTOM_PLATFORM)/hal/inc/aaa \

LOCAL_C_INCLUDES += $(MTK_PATH_SOURCE)/hardware/mtkcam/drv/include/$(shell echo $(MTK_PLATFORM) | tr A-Z a-z)
#-----------------------------------------------------------
LOCAL_WHOLE_STATIC_LIBRARIES += libacdk_entry_cctif
LOCAL_WHOLE_STATIC_LIBRARIES += libacdk_entry_mdk

#-----------------------------------------------------------
LOCAL_SHARED_LIBRARIES := libcutils libc libstdc++
LOCAL_STATIC_LIBRARIES += libft

LOCAL_MODULE := libccap

#
# Start of common part ------------------------------------


#-----------------------------------------------------------
LOCAL_CFLAGS += $(MTKCAM_CFLAGS)

#-----------------------------------------------------------
LOCAL_C_INCLUDES += $(MTKCAM_C_INCLUDES)

#-----------------------------------------------------------

# End of common part ---------------------------------------
#

include $(BUILD_STATIC_LIBRARY)

