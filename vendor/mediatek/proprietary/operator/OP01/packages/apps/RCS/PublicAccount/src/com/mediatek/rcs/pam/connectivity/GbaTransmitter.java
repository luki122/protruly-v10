package com.mediatek.rcs.pam.connectivity;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.android.okhttp.MediaType;
import com.android.okhttp.OkHttpClient;
import com.android.okhttp.Request;
import com.android.okhttp.RequestBody;
import com.android.okhttp.ResponseBody;
import com.mediatek.gba.GbaHttpUrlCredential;
import com.mediatek.rcs.pam.CommonHttpHeader;
import com.mediatek.rcs.pam.PlatformManager;
import com.mediatek.rcs.pam.client.PAMClient.Response;
import com.mediatek.rcs.pam.client.PAMClient.Transmitter;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.Authenticator;

/**
 * PAM client with GBA support.
 */
public class GbaTransmitter implements Transmitter {
    public static final String TAG = "PAM/GbaTransmitter";

    private String mServerUrl;
    private Context mContext;
    private String mNafUrl;

    public GbaTransmitter(Context context, String serverUrl, String nafUrl) {
        mServerUrl = serverUrl;
        mContext = context;
        mNafUrl = nafUrl;

        GbaHttpUrlCredential gbaCredential = new GbaHttpUrlCredential(mContext, mNafUrl);
        Authenticator.setDefault(gbaCredential.getAuthenticator());
    }

    @Override
    public Response sendRequest(String msgname, String content, boolean postOrGet)
            throws IOException {
        Log.d(TAG, "sendRequest: " + msgname);
        Log.d(TAG, content);
        Response result = new Response();
        try {
            URL serverUrl = new URL(mServerUrl);
            OkHttpClient client = new OkHttpClient();
            Request request = null;
            String identity = PlatformManager.getInstance().getIdentity(mContext);
            if (TextUtils.isEmpty(identity)) {
                result.result = 401;
                return result;
            }

            MediaType contentType = MediaType.parse("text/xml");
            RequestBody resuestBody = RequestBody.create(contentType, content);
            request = new Request.Builder()
                    .url(mServerUrl)
                    .method(HttpTransmitter.POST, resuestBody)
                    .header(CommonHttpHeader.ACCEPT_ENCODING,
                            CommonHttpHeader.ACCEPT_ENCODING_VALUE)
                    .header(CommonHttpHeader.X_3GPP_INTENDED_IDENTITY, identity)
                    .header(CommonHttpHeader.USER_AGENT, "XDM-client/OMA1.0").build();
            com.android.okhttp.Response httpResponse = client.newCall(request).execute();
            ResponseBody responseBody = httpResponse.body();
            result.result = httpResponse.code();
            result.content = IOUtils.toString(responseBody.byteStream(), "UTF-8");
            Log.d(TAG, "Status Line: " + responseBody);
            Log.d(TAG, "Status Code: " + result.result);
            Log.d(TAG, "Response Content: " + result.content);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            result.result = -1;
        }
        return result;
    }
}
