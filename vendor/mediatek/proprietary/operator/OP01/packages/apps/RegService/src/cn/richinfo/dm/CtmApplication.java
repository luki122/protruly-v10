package cn.richinfo.dm;

import android.app.Application;
import android.content.ContentResolver;
import android.database.ContentObserver;
import cn.richinfo.dm.DMSDK;
import com.dmyk.android.telephony.DmykTelephonyManager;
import com.dmyk.android.telephony.DmykAbsTelephonyManager;
import android.net.Uri;
import android.os.Handler;
import android.content.Intent;
import com.dmyk.android.telephony.DmykTelephonyManager.MLog;
import android.provider.Settings;


public class CtmApplication extends Application {
    private DmykTelephonyManager mDTM = null;
    private ApnObserver[] mObservers = new ApnObserver[2];
    private static CtmApplication sInstance;

    private static final String ENHANCED_4G_MODE_ENABLED_SIM1 =
                                    Settings.Global.ENHANCED_4G_MODE_ENABLED;
    private static final String ENHANCED_4G_MODE_ENABLED_SIM2 = "volte_vt_enabled_sim2";

    public CtmApplication() {
        super();
    }

    public void onCreate() {
        super.onCreate();
        MLog.d("CtmApplication.onCreate()");
        DMSDK.init(this);
        DMSDK.setDebugMode(false);
        // NOTE: this should be invoked after DMSDK.init(this), otherwise we cannot use the
        // the context specified by DMSDK. If DMSDK provides a non-null context, then use
        // application context instead.
        mDTM = (DmykTelephonyManager) DmykAbsTelephonyManager.getDefault(this);
        // DMSDK.setDmykAbsTelephonyManager(mDTM);

        // Observers for APN provider
        for (int slot = 0; slot < 2; ++slot) {
            Uri uri = mDTM.getAPNContentUri(slot);
            if (uri == null) {
                MLog.d("No URI for slot " + slot + ", probably there is no sim in the slot");
                mObservers[slot] = null;
            } else {
                mObservers[slot] = new ApnObserver(uri);
                getContentResolver().registerContentObserver(uri, true, mObservers[slot]);
            }
        }

        // Sync VoLTE status before registering observers
        syncVolteStatus();
        registerVolteObservers();

        sInstance = this;
    }

    private class ApnObserver extends ContentObserver {
        public final Uri uri;
        public ApnObserver(Uri uri) {
            super(new Handler());
            this.uri = uri;
        }

        @Override
        public void onChange(boolean selfChange) {
            onChange(selfChange, null);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            Intent intent = new Intent(DmykAbsTelephonyManager.ACTION_APN_STATE_CHANGE);
            intent.setData(uri);
            sendBroadcast(intent);
        }

    }

    private class VolteStatusObserver extends ContentObserver {
        private int mSlotId;
        private String mKey;
        public VolteStatusObserver(int slotId, String key) {
            super(new Handler());
            mSlotId = slotId;
            mKey = key;
        }

        @Override
        public void onChange(boolean selfChange) {
            onChange(selfChange, null);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            MLog.d("VolteStatusObserver.onChange(" + selfChange + ", " + uri + ")");
            int internalStatus = mDTM.getVoLTEState(mSlotId);
            MLog.d("VolteStatusObserver.onChange(): internalStatus=" + internalStatus);
            int status = Settings.System.getInt(
                getContentResolver(),
                mKey,
                DmykAbsTelephonyManager.VOLTE_STATE_UNKNOWN);
            MLog.d("VolteStatusObserver.onChange(): status=" + status);
            if (internalStatus != DmykAbsTelephonyManager.VOLTE_STATE_UNKNOWN &&
                status != DmykAbsTelephonyManager.VOLTE_STATE_UNKNOWN) {
                if (internalStatus != status) {
                    mDTM.setVoLTEState(mSlotId, status);
                }

                Intent intent = new Intent(DmykAbsTelephonyManager.ACTION_VOLTE_STATE_CHANGE);
                sendBroadcast(intent);
            }
        }
    }

    private class InternalVolteStatusObserver extends ContentObserver {
        private int mSlotId;
        private String mKey;
        public InternalVolteStatusObserver(int slotId, String key) {
            super(new Handler());
            mSlotId = slotId;
            mKey = key;
        }

        @Override
        public void onChange(boolean selfChange) {
            onChange(selfChange, null);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            MLog.d("InternalVolteStatusObserver.onChange(" + selfChange + ", " + uri + ")");
            int internalStatus = mDTM.getVoLTEState(mSlotId);
            MLog.d("InternalVolteStatusObserver.onChange(): internalStatus=" + internalStatus);
            int status = Settings.System.getInt(
                getContentResolver(),
                mKey,
                DmykAbsTelephonyManager.VOLTE_STATE_UNKNOWN);
            MLog.d("InternalVolteStatusObserver.onChange(): status=" + status);
            if (internalStatus != DmykAbsTelephonyManager.VOLTE_STATE_UNKNOWN &&
                status != DmykAbsTelephonyManager.VOLTE_STATE_UNKNOWN) {
                if (internalStatus != status) {
                    MLog.d("InternalVolteStatusObserver.onChange(): set status, key=" + mKey + ", status=" + internalStatus);
                    Settings.System.putInt(
                        getContentResolver(),
                        mKey,
                        internalStatus);
                }
            }
        }
    }

    public static void updateAPNObservers() {
        if (sInstance == null) {
            return;
        }
        sInstance.updateAPNObserversInternal();
    }

    public void updateAPNObserversInternal() {
        MLog.d("Update APNObserver list");
        for (int slot = 0; slot < 2; ++slot) {
            Uri uri = mDTM.getAPNContentUri(slot);
            if (uri == null) {
                MLog.d("No URI for slot " + slot + ", probably there is no sim in the slot");
                if (mObservers[slot] != null) {
                    getContentResolver().unregisterContentObserver(mObservers[slot]);
                    mObservers[slot] = null;
                }
            } else {
                if (mObservers[slot] != null) {
                    if (mObservers[slot].uri.compareTo(uri) != 0) {
                        getContentResolver().unregisterContentObserver(mObservers[slot]);
                        mObservers[slot] = new ApnObserver(uri);
                        getContentResolver().registerContentObserver(uri, true, mObservers[slot]);
                    }
                } else {
                    mObservers[slot] = new ApnObserver(uri);
                    getContentResolver().registerContentObserver(uri, true, mObservers[slot]);
                }
            }
        }

    }

    private void syncVolteStatus() {
        MLog.d("syncVolteStatus");
        ContentResolver cr = getContentResolver();
        Settings.Global.putInt(
            cr,
            DmykAbsTelephonyManager.VOLTE_DMYK_STATE_0,
            mDTM.getVoLTEState(0));
        Settings.Global.putInt(
            cr,
            DmykAbsTelephonyManager.VOLTE_DMYK_STATE_1,
            mDTM.getVoLTEState(1));
    }

    private void registerVolteObservers() {
        MLog.d("registerVolteObservers");
        ContentResolver cr = getContentResolver();
        cr.registerContentObserver(
            Settings.System.getUriFor(Settings.System.VOLTE_DMYK_STATE_0),
            true,
            new VolteStatusObserver(0, Settings.System.VOLTE_DMYK_STATE_0));
        cr.registerContentObserver(
            Settings.System.getUriFor(Settings.System.VOLTE_DMYK_STATE_1),
            true,
            new VolteStatusObserver(1, Settings.System.VOLTE_DMYK_STATE_1));
        cr.registerContentObserver(
            Settings.Global.getUriFor(ENHANCED_4G_MODE_ENABLED_SIM1),
            true,
            new InternalVolteStatusObserver(0, Settings.System.VOLTE_DMYK_STATE_0));
        cr.registerContentObserver(
            Settings.Global.getUriFor(ENHANCED_4G_MODE_ENABLED_SIM2),
            true,
            new InternalVolteStatusObserver(1, Settings.System.VOLTE_DMYK_STATE_1));
    }
}
