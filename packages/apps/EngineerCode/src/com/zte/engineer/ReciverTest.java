
package com.zte.engineer;

import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.view.KeyEvent;
import android.provider.Settings;

public class ReciverTest extends ZteActivity {
    /*
     * Define some aliases to make these debugging flags easier to refer to.
     */
    private final static String LOGTAG = "ZTEReceiverTest";
    private final static int MESSAGE_START_PLAY = 1;
    private final static int MESSAGE_STOP_PLAY = 2;
    private AudioManager mAudioManager = null;
    private MediaPlayer mp = null;

    @Override
    protected void onCreate(Bundle arg0) {
        // TODO Auto-generated method stub
        super.onCreate(arg0);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.singlebuttonview);
        ((TextView) findViewById(R.id.singlebutton_textview)).setText(R.string.audio_receiver);
        ((Button) findViewById(R.id.btnPass)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnFail)).setOnClickListener(this);
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		mAudioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL, mAudioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL), AudioManager.FLAG_PLAY_SOUND); 
	//	mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), AudioManager.FLAG_PLAY_SOUND); 

    }	
	
    private void InitMediaPlayer() {
        mp = new MediaPlayer();
        mp.setVolume(1f, 1f);
        try {
            mp.setDataSource(this,
                    RingtoneManager.getActualDefaultRingtoneUri(this, RingtoneManager.TYPE_ALARM));
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    Handler mPlayerHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_START_PLAY:
                    InitMediaPlayer();
                    mAudioManager.setMode(2);
                    try {
                        Thread.sleep(500);
                    } catch (java.lang.InterruptedException e) {
                    }

                    mp.setAudioStreamType(AudioManager.STREAM_VOICE_CALL);
                    mp.setLooping(true);
                    try {
                        mp.prepare();
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mp.start();
                    break;
                case MESSAGE_STOP_PLAY:
                    mp.stop();
                    mp.release();
                    mAudioManager.setMode(0);
                    break;
                default:
                    break;
            }
        };
    };

    @Override
    protected void onPause() {
        super.onPause();
		Settings.System.putInt(getContentResolver(), "AEON_KEY_TEST", 0);
        mPlayerHandler.sendEmptyMessage(MESSAGE_STOP_PLAY);
        mAudioManager.setMode(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
		Settings.System.putInt(getContentResolver(), "AEON_KEY_TEST", 1);
        mPlayerHandler.sendEmptyMessage(MESSAGE_START_PLAY);
    }

}
