package android.content.res;

import android.content.res.HbThemeZip.ThemeFileInfo;

/**
 * 系统资源framework-res.apk
 * @author alexluo
 *
 */
public class HbSystemTheme extends HbApplicationTheme {

	private static HbSystemTheme mInstance;
	
	  // Locker
	
	private HbSystemTheme(Resources res, String packageName) {
		super(res, packageName);
		// TODO Auto-generated constructor stub
	}
	
	public static HbSystemTheme getInstance(Resources res, String packageName){
				mInstance = new HbSystemTheme(res, packageName);
			mInstance.update();
			return mInstance;
	}
	
	
	@Override
	public Integer getThemeValue(int id) {
		// TODO Auto-generated method stub
		return mThemeZipFile.getThemeInteger(id);
	}
	
	@Override
	public Float getFloatThemeValue(int id) {
		// TODO Auto-generated method stub
		return mThemeZipFile.getThemeFloat(id);
	}
	

	@Override
	public ThemeFileInfo getThemeFileInfo(String themePah,int id) {

		return mThemeZipFile.getThemeFileInfo(HbApplicationTheme.SYSTEM_RES_ANDROID, themePah);
	}
	
	@Override
	public String getVectorPath(int id) {
		// TODO Auto-generated method stub
		return mThemeZipFile.getVectorPath(id);
	}
	

}
