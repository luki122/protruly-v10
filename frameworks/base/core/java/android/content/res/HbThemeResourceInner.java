package android.content.res;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.HashMap;

import android.app.ActivityThread;
import android.content.res.HbThemeZip.ThemeFileInfo;
import android.content.res.Resources.NotFoundException;
import android.content.res.Resources.Theme;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.LongSparseArray;
import android.util.SparseArray;
import android.util.TypedValue;

public class HbThemeResourceInner extends Resources{

	
	private static final String TAG = "HbTheme";

	private static final String NINE_PATCH_SUFFIX = ".9.png";
	
	private static final String XML_DRAWABLE_SUFFIX = ".xml";

	private HashMap<Integer, WeakReference<ThemeFileInfo>> mCachedThemeFile = new HashMap<Integer, WeakReference<ThemeFileInfo>>();


	private final LongSparseArray<WeakReference<ColorStateList>> mColorStateListCache = new LongSparseArray<WeakReference<ColorStateList>>();
	private final ArrayMap<String, LongSparseArray<WeakReference<ConstantState>>> mDrawableCache =
            new ArrayMap<String, LongSparseArray<WeakReference<ConstantState>>>();
    private final ArrayMap<String, LongSparseArray<WeakReference<ConstantState>>> mColorDrawableCache =
            new ArrayMap<String, LongSparseArray<WeakReference<ConstantState>>>();

	// These are protected by mAccessLock.
	private final Object mAccessLock = new Object();

	private SparseArray mThemeValues = new SparseArray();


	private HbApplicationTheme mTheme;

	private boolean clearFromConfigChanged = false;

	 String mPackageName;
	 
	
	public HbThemeResourceInner() {
		super();
		// TODO Auto-generated constructor stub
		mPackageName = ActivityThread.currentPackageName();
		init();
	}

	public HbThemeResourceInner(AssetManager assets, DisplayMetrics metrics,
			Configuration config, CompatibilityInfo compatInfo) {
		super(assets, metrics, config, compatInfo);
		// TODO Auto-generated constructor stub
		mPackageName = ActivityThread.currentPackageName();
		init();
	}

	public HbThemeResourceInner(AssetManager assets, DisplayMetrics metrics,
			Configuration config) {
		super(assets, metrics, config);
		// TODO Auto-generated constructor stub
		mPackageName = ActivityThread.currentPackageName();
		init();
	}
	
	/**
	 * 初始化主题资源
	 */
	private void init() {
		
		mThemeValues.clear();
		mTheme = new HbApplicationTheme(this, mPackageName);
		mTheme.update();
		mTheme.addSystemResources(HbApplicationTheme.SYSTEM_RES_ANDROID);
		mTheme.addSystemResources(HbApplicationTheme.SYSTEM_RES_HUMMINGBIRD);
	}
	
	private void clear(int configChanges) {
		if (mTheme != null) {
			mTheme.freeCaches();
		}
		if (mColorStateListCache != null ) {
			mColorStateListCache.clear();
		}
		
//		clearDrawableCachesLocked(mColorDrawableCache,configChanges);
//		clearDrawableCachesLocked(mDrawableCache,configChanges);
		
	}
	
	private void clearDrawableCachesLocked(
            ArrayMap<String, LongSparseArray<WeakReference<ConstantState>>> caches,int configChanges) {
        final int N = caches.size();
        for (int i = 0; i < N; i++) {
            clearDrawableCacheLocked(caches.valueAt(i),configChanges);
        }
    }

    private void clearDrawableCacheLocked(
            LongSparseArray<WeakReference<ConstantState>> cache,int configChanges) {
        final int N = cache.size();
        for (int i = 0; i < N; i++) {
            final WeakReference<ConstantState> ref = cache.valueAt(i);
            if (ref != null) {
                final ConstantState cs = ref.get();
                if (cs != null) {
                    if (Configuration.needNewResources(
                            configChanges, cs.getChangingConfigurations())) {
                        cache.setValueAt(i, null);
                    } 
                }
            }
        }
    }
    
    
	
	
	@Override
	protected void updateThemeConfiguration(int configChanges){
		// TODO Auto-generated method stub
		/*
		 * 如果主题改变了就清理主题缓存
		 */
		try{
			Log.w(TAG, "ThemeConfiguration->"+mPackageName);
			clear(configChanges);
		}catch(Exception e){
			Log.w(TAG, "ThemeConfiguration doesn't init->"+e);
		}
	}
	

	public HbApplicationTheme getTheme() {

		return mTheme;
	}

	@Override
	public void initTheme(String pkgName) {
		// TODO Auto-generated method stub
		mPackageName = pkgName;
		init();
	}
	
	public InputStream hasAssetsFile(String assetPath){
		return mTheme.getAssetsFile(assetPath);
	}
	
	@Override
	public float getDimension(int id) throws NotFoundException {
		// TODO Auto-generated method stub
		synchronized (mAccessLock) {
				Float dimen = mTheme.getFloatThemeValue(id);
				if(dimen != null){
					return dimen;
				}
		}
		
		return super.getDimension(id);
	}
	
	@Override
	public int getDimensionPixelOffset(int id) throws NotFoundException {
		
		// TODO Auto-generated method stub
		synchronized (mAccessLock) {
				Float dimen = mTheme.getFloatThemeValue(id);
				if(dimen != null){
					return (int)(dimen+0.5f);
				}
		}
		
		return super.getDimensionPixelOffset(id);
	}
	
	@Override
	public int getDimensionPixelSize(int id) throws NotFoundException {
		// TODO Auto-generated method stub
		synchronized (mAccessLock) {
				Float dimen = mTheme.getFloatThemeValue(id);
				if(dimen != null){
					return (int)(dimen+0.5f);
				}
		}
		
		return super.getDimensionPixelSize(id);
	}
	
	public Integer getThemeDimensionPixelSize(int id){
		synchronized (mAccessLock) {
			Float dimen = mTheme.getFloatThemeValue(id);
			if(dimen != null){
				return (int)(dimen+0.5f);
			}
	}
		return null;
	}
	
	@Override
	public String getVectorPath(int id) {
		// TODO Auto-generated method stub
		if(id == 0){
			return null;
		}
		return mTheme.getVectorPath(id);
	}
	
	@Override
	Drawable loadDrawable(TypedValue value, int id, Theme theme)
			throws NotFoundException {
		// TODO Auto-generated method stub
		
		/*
		 * load drawable from theme package
		 */
		final Drawable themeDrawable = loadDrawableFromTheme(value, id, theme);
		
		if (themeDrawable != null) {
			return themeDrawable;
		}

		return super.loadDrawable(value, id, theme);
	}
	
	/**
	 * 读取主题包中的图片
	 * 
	 * @param value
	 * @param id
	 * @param theme
	 * @return
	 */
	protected Drawable loadDrawableFromTheme(TypedValue value, int id,Theme theme) {
		Drawable drawable = null;
		ThemeFileInfo themeFile = null;
		String srcName = value.string != null?value.string.toString():null;
		if(srcName == null){
			return drawable;
		}
		   final boolean isColorDrawable;
		   final ArrayMap<String, LongSparseArray<WeakReference<ConstantState>>> caches;
	        final long key;
	        TypedValue themeValue = new TypedValue();
			themeValue.setTo(value);
	        if (themeValue.type >= TypedValue.TYPE_FIRST_COLOR_INT
	                && themeValue.type <= TypedValue.TYPE_LAST_COLOR_INT) {
	            isColorDrawable = true;
	            caches = mColorDrawableCache;
	            key = themeValue.data;
	        } else {
	            isColorDrawable = false;
	            caches = mDrawableCache;
	            key = (((long) themeValue.assetCookie) << 32) | themeValue.data;
	        }
	        final Drawable cachedDrawable =getCachedDrawable(caches, key,srcName);
            if (cachedDrawable != null) {
                return cachedDrawable;
            }
		  if(isColorDrawable){
			  Integer color = mTheme.getThemeValue(id);
			  if(color != null){
				  drawable = new ColorDrawable(color);
			  }
		  }else{
				final String path = srcName;
				themeFile  = mTheme.getThemeFileInfo(path,id);
				if(themeFile != null){
					mCachedThemeFile.put(id, new WeakReference<ThemeFileInfo>(themeFile));
					if(path.endsWith(NINE_PATCH_SUFFIX)){
						try {
							drawable = HbDrawableUtils.decodeDrawableFromStream(themeFile.inputStream,this);
							
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}finally{
							try{
							themeFile.inputStream.close();
							}catch(Exception ex){
								
							}
						}
					}else if(path.endsWith(XML_DRAWABLE_SUFFIX)){
						
					}else{
						if(value.density != themeFile.density){
							value.density = themeFile.density;
						}
						
						drawable = Drawable.createFromResourceStream(this, value, themeFile.inputStream, themeFile.srcName, null);
						try {
							themeFile.inputStream.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}finally{
							try{
								themeFile.inputStream.close();
							}catch(Exception ex){
								
							}
						}
					}
					
					
				}
		  }
		if(drawable != null){
			drawable.setChangingConfigurations(value.changingConfigurations);
			cacheDrawable(isColorDrawable, caches, key, drawable,srcName);
		}
		return drawable;
	}
	
	
	 private void cacheDrawable(boolean isColorDrawable,
	            ArrayMap<String, LongSparseArray<WeakReference<ConstantState>>> caches,
			long key, Drawable dr,String stringKey) {
		final ConstantState cs = dr.getConstantState();
		if (cs == null) {
			return;
		}
		synchronized (mAccessLock) {
			final String themeKey = stringKey;
			  if(themeKey != null){
				LongSparseArray<WeakReference<ConstantState>> themedCache = caches
						.get(themeKey);
				if (themedCache == null) {
					themedCache = new LongSparseArray<WeakReference<ConstantState>>(
							1);
					caches.put(themeKey, themedCache);
				}
				themedCache.put(key, new WeakReference<ConstantState>(cs));
			}
		}
	}
	
	
	private Drawable getCachedDrawable(
            ArrayMap<String, LongSparseArray<WeakReference<ConstantState>>> caches,
            long key, String resKey) {
        synchronized (mAccessLock) {
            final String themeKey = resKey;
            if(themeKey != null){
	            final LongSparseArray<WeakReference<ConstantState>> themedCache = caches.get(themeKey);
	            if (themedCache != null) {
	                final Drawable themedDrawable = getCachedDrawableLocked(themedCache, key);
	                if (themedDrawable != null) {
	                    return themedDrawable;
	                }
	            }
            }
            // No cached drawable, we'll need to create a new one.
            return null;
        }
    }

    private ConstantState getConstantStateLocked(
            LongSparseArray<WeakReference<ConstantState>> drawableCache, long key) {
        final WeakReference<ConstantState> wr = drawableCache.get(key);
        if (wr != null) {   // we have the key
            final ConstantState entry = wr.get();
            if (entry != null) {
                //Log.i(TAG, "Returning cached drawable @ #" +
                //        Integer.toHexString(((Integer)key).intValue())
                //        + " in " + this + ": " + entry);
                return entry;
            } else {  // our entry has been purged
                drawableCache.delete(key);
            }
        }
        return null;
    }

    private Drawable getCachedDrawableLocked(
            LongSparseArray<WeakReference<ConstantState>> drawableCache, long key) {
        final ConstantState entry = getConstantStateLocked(drawableCache, key);
        if (entry != null) {
            return entry.newDrawable(this);
        }
        return null;
    }
	
	
	
	/**
	 * 重写这个方法来实现Bitmap的加载
	 */
	@Override
	public InputStream openRawResource(int id, TypedValue value)
			throws NotFoundException {
		// TODO Auto-generated method stub
		 getValue(id, value, true);
		String srcName = value.string != null?value.string.toString():null;
		if(!TextUtils.isEmpty(srcName)){
			ThemeFileInfo themeFile = mTheme.getThemeFileInfo(srcName, id);
			if(themeFile != null){
				return themeFile.inputStream;
			}
		}
		return super.openRawResource(id, value);
	}
	
	

	@Override
	ColorStateList loadColorStateList(TypedValue value, int id,Theme theme)
			throws NotFoundException {
		// TODO Auto-generated method stub
		TypedValue themeValue = new TypedValue();
		themeValue.setTo(value);
		final long key = (((long) themeValue.assetCookie) << 32)
				| themeValue.data;
		ColorStateList csl = getCachedColorStateList(key);
		
		if (csl != null) {
			return csl;
		}
	        // Handle inline color definitions.
	        if (value.type >= TypedValue.TYPE_FIRST_COLOR_INT
	                && value.type <= TypedValue.TYPE_LAST_COLOR_INT) {
	            Integer color = mTheme.getThemeValue(id);
	        	if (color != null) {
					csl = ColorStateList.valueOf(color);
					if (csl != null) {
						mColorStateListCache.put(key,
								new WeakReference<ColorStateList>(csl));
						return csl;
					}
				}
	        }
		return super.loadColorStateList(value, id,theme);
	}
	
	private ColorStateList getCachedColorStateList(long key) {
		synchronized (mAccessLock) {
			WeakReference<ColorStateList> wr = mColorStateListCache.get(key);
			if (wr != null) { // we have the key
				ColorStateList entry = wr.get();
				if (entry != null) {
					// Log.i(TAG, "Returning cached color state list @ #" +
					// Integer.toHexString(((Integer)key).intValue())
					// + " in " + this + ": " + entry);
					return entry;
				} else { // our entry has been purged
					mColorStateListCache.delete(key);
				}
			}
		}
		return null;
	}
	
	
	
	
	////////

	@Override
	 public boolean getBoolean(int id) throws NotFoundException {
		synchronized (mAccessLock) {
				Integer bool = mTheme.getThemeValue(id);
				if(bool != null){
					return bool != 0;
				}
		}
		
		return super.getBoolean(id);
	}
	
	@Override
	public int getInteger(int id) throws NotFoundException {
		// TODO Auto-generated method stub
		synchronized (mAccessLock) {
			
				Integer integer = mTheme.getThemeValue(id);
				if(integer != null){
					return integer;
				}
			
		}
		
		return super.getInteger(id);
	}
	
	@Override
	public int getColor(int id) throws NotFoundException {
		// TODO Auto-generated method stub
		synchronized (mAccessLock) {
				Integer color = mTheme.getThemeValue(id);
				if(color != null){
					return color;
			}
		}
		
		return super.getColor(id);
	}
	
	
	

}
