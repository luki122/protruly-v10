package com.eastaeon.agingtest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import android.app.Activity;
import android.content.*;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;
import android.provider.Settings;

public class CameraTest extends Activity{
	private final static String TAG = "CameraActivity";
	private Context mContext;
	private SurfaceView surfaceView;
	private SurfaceHolder surfaceHolder;
	private Handler handler=new Handler();
	private Handler takehandler=new Handler();
	private Camera camera;
	private File picture;
	private Button btnSave;
	private boolean mTakePictureEnable = true;
	private int mRunningTotalTime = 1800000;
	private int mRunningStepTime = 3000;
	private int mTakePictureNumber = 0;
	private long mExitTime;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN , WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_camera_test);
		setupViews();
		mContext = getApplication();
		mTakePictureNumber=0;
		mTakePictureEnable = true;
	}

	@Override
	protected void onResume() {
		super.onResume();
		Settings.System.putInt(getContentResolver(), "AEON_KEY_TEST", 1);
		handler.postDelayed(runnable, mRunningTotalTime);
		takehandler.postDelayed(takeRunnable, mRunningStepTime);
	}

	@Override
	protected void onPause() {
        super.onPause();
		Settings.System.putInt(getContentResolver(), "AEON_KEY_TEST", 0);
		takehandler.removeCallbacks(takeRunnable);
		handler.removeCallbacks(runnable);	
	}

	@Override
	public void onAttachedToWindow() {
		super.onAttachedToWindow();
	}
	
	private void setupViews(){
		surfaceView = (SurfaceView) findViewById(R.id.camera_preview); // Camera interface to instantiate components
		surfaceHolder = surfaceView.getHolder(); // Camera interface to instantiate components
		surfaceHolder.addCallback(surfaceCallback); // Add a callback for the SurfaceHolder
		surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	private void takePic() {
		//camera.stopPreview();// stop the preview
		if(camera != null){
			camera.takePicture(null, null, pictureCallback); // picture
		}
	}

	// Photo call back
	Camera.PictureCallback pictureCallback = new Camera.PictureCallback() {
		//@Override
		public void onPictureTaken(byte[] data, Camera camera) {
			mTakePictureNumber++;
			String msg =  String.format("Take %d Pictures!", mTakePictureNumber);
			Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
			if(camera != null){
				camera.startPreview();
			}
		}
	};

	AutoFocusCallback afcb= new AutoFocusCallback() {
		@Override
		public void onAutoFocus(boolean success, Camera camera) {
			if (success) {
				if(camera != null){
					camera.takePicture(null, null, pictureCallback);
				}
			}
		}
	};

	Runnable runnable=new Runnable() {
		@Override
		public void run() {
			mTakePictureEnable = false;
            setResult(Activity.RESULT_OK);
			//takehandler.removeCallbacks(takeRunnable);
			surfaceCallback.close();
			finish();
			//android.os.Process.killProcess(android.os.Process.myPid());
		}
	};
	
	Runnable takeRunnable=new Runnable() {
		@Override
		public void run() {
			if(mTakePictureEnable){
				if(camera != null){
					camera.autoFocus(afcb);
				}
				takehandler.postDelayed(this, mRunningStepTime);
			}
		}
	};

        // SurfaceHodler Callback handle to open the camera, off camera and photo size changes
	MyCallback surfaceCallback = new MyCallback();
	private class MyCallback implements SurfaceHolder.Callback {
		public void surfaceCreated(SurfaceHolder holder) {
			Camera.CameraInfo info = new Camera.CameraInfo();
			for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
				Camera.getCameraInfo(i, info);
				if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
					try {
						// Gets to here OK
						camera = Camera.open(i);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			
			try {
				camera.setPreviewDisplay(holder);
			} catch (IOException e) {
				camera.release();// release camera
				camera = null;
			}
		}

		public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
			Camera.Parameters parameters = camera.getParameters(); // Camera parameters to obtain
			parameters.setPictureFormat(PixelFormat.JPEG);// Setting Picture Format
			parameters.set("jpeg-quality", 85);
			parameters.setPreviewSize(4864,2736);
			parameters.setZoom(0);
			parameters.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
			//          parameters.set("rotation", 180); // Arbitrary rotation
			camera.setDisplayOrientation(90);
			//          parameters.setPreviewSize(400, 300); // Set Photo Size
			camera.setParameters(parameters); // Setting camera parameters
			camera.startPreview(); // Start Preview
		}

		public void surfaceDestroyed(SurfaceHolder holder) {
			close();
		}

		synchronized public void close() {
			if (camera != null) {
				camera.stopPreview();// stop preview
				camera.release(); // Release camera resources
				camera = null;
			}			
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		android.util.Log.d("zengtao", "keyCode = "+event.getKeyCode());
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if ((System.currentTimeMillis() - mExitTime) > 2000) {
				Object mHelperUtils;
				mExitTime = System.currentTimeMillis();
			} else {
				surfaceCallback.close();
				finish();
			}
			return true;
		}
		
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		android.util.Log.d("zengtao", "keycode:" + event.getKeyCode());
		if (event.getKeyCode() == KeyEvent.KEYCODE_APP_SWITCH) {
			return true;
		} else {
			return super.dispatchKeyEvent(event);
		}
	}	
}
