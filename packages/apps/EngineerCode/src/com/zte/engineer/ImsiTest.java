
package com.zte.engineer;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.internal.telephony.PhoneConstants;
import com.mediatek.telephony.TelephonyManagerEx;
import android.os.SystemProperties;

import android.telephony.SubscriptionManager;
import android.text.TextUtils;

public class ImsiTest extends ZteActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.singlebuttonview);

        TelephonyManager telephonyManager = (TelephonyManager) this
                .getSystemService(Context.TELEPHONY_SERVICE);

        TextView mTextView = (TextView) findViewById(R.id.singlebutton_textview);
        mTextView.setText(R.string.imsi);
        String mIMSI1 = null;
        String mIMSI2 = null;
        if ("true".equals(SystemProperties.get("ro.mediatek.gemini_support"))) {

            // may before kk
            //mIMSI11 = telephonyManager.getSubscriberIdGemini(0);
            //mIMSI12 = telephonyManager.getSubscriberIdGemini(1);

            if (Build.VERSION.SDK_INT >= 23) {
                mIMSI1 = telephonyManager
                        .getSubscriberId(getSubIdBySlot(PhoneConstants.SIM_ID_1));
                mIMSI2 = telephonyManager
                        .getSubscriberId(getSubIdBySlot(PhoneConstants.SIM_ID_2));
            } else if (Build.VERSION.SDK_INT >= 19) { //Build.VERSION_CODES.KITKAT
                mIMSI1 = TelephonyManagerEx.getDefault().getSubscriberId(PhoneConstants.SIM_ID_1);
                mIMSI2 = TelephonyManagerEx.getDefault().getSubscriberId(PhoneConstants.SIM_ID_2);
            }

            TextView mTextViewIMEI = (TextView) findViewById(R.id.singlebutton_textview_2);
            mTextViewIMEI.setText(String.format(getResources().getString(R.string.display_IMSI_1),
                    mIMSI1));
            mTextViewIMEI = (TextView) findViewById(R.id.singlebutton_textview_3);
            mTextViewIMEI.setText(String.format(getResources().getString(R.string.display_IMSI_2),
                    mIMSI2));
        } else { // the case is unchecked in M version !
            mIMSI1 = telephonyManager.getSubscriberId();

            TextView mTextViewIMEI = (TextView) findViewById(R.id.singlebutton_textview_2);

            mTextViewIMEI.setText(String.format(getResources().getString(R.string.display_IMSI),
                    mIMSI1));
        }

        Button btnPass = (Button) findViewById(R.id.btnPass);
        btnPass.setOnClickListener(this);
        Button btnFail = (Button) findViewById(R.id.btnFail);
        btnFail.setOnClickListener(this);

        boolean hasImsi = false;
        if (!TextUtils.isEmpty(mIMSI1) || !TextUtils.isEmpty(mIMSI2)) {
            hasImsi = true;
        }
        btnPass.setEnabled(hasImsi);
    }

    // The method is added by XiaFei 2016.
    private int getSubIdBySlot(int slot) {
        int[] subId = SubscriptionManager.getSubId(slot);
        return (subId != null) ? subId[0] : SubscriptionManager.getDefaultSubId();
    }

}
