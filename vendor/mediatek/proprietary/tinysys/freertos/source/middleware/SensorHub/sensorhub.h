#ifndef __SENSORHUB_H__
#define __SENSORHUB_H__


extern int xSensorManagerInit(void);
extern int xSensorFrameworkInit(void);
//extern int xSensorFrameworkTestInit(void);
//extern int FakeACC_Init(void);
//extern int FakeMAG_Init(void);
//extern int FakeGYRO_Init(void);
//extern int FakePDRInit(void);
//extern int FakePedometerInit(void);
extern int xFrameworkFlushDirectPushFIFO(void);
#endif
