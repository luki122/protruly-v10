ANDROID_PARTNER_M_HOME := packages/apps/hmbirdapps
define all_files_under_subdir
$(foreach files, $(notdir $(wildcard $1)),$(dir $(1))$(files):$(2)/$(files)) 
endef

PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_M_HOME)/HMBThemeManager/wallpaper/lockscreen/*.jpg,system/hummingbird/theme/wallpaper/lockscreen) \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_M_HOME)/HMBThemeManager/wallpaper/desktop/*.jpg,system/hummingbird/theme/wallpaper/desktop)

PRODUCT_PACKAGES += HMBSettings
PRODUCT_PACKAGES += HMBSystemUI
PRODUCT_PACKAGES += HMBLauncher
PRODUCT_PACKAGES += HMBContactsProvider
PRODUCT_PACKAGES += HMBInCallUI
PRODUCT_PACKAGES += HMBMms
PRODUCT_PACKAGES += HMBTelecomm
PRODUCT_PACKAGES += HMBTelephonyProvider
PRODUCT_PACKAGES += HMBTeleService
PRODUCT_PACKAGES += HMBThemeManager
PRODUCT_PACKAGES += HMBReject
PRODUCT_PACKAGES += HMBDialer
PRODUCT_PACKAGES += HMBContacts
PRODUCT_PACKAGES += IFlyIme
PRODUCT_PACKAGES += HMBPermissionControl
PRODUCT_PACKAGES += HMBPowerManager
PRODUCT_PACKAGES += HMBManager
PRODUCT_PACKAGES += HMBProvision
PRODUCT_PACKAGES += HMBDeskClock
PRODUCT_PACKAGES += HMBNetManager
PRODUCT_PACKAGES += HMBQuickSearchBox
PRODUCT_PACKAGES += HMBCalendar
PRODUCT_PACKAGES += HMBCalendarProvider
PRODUCT_PACKAGES += HMBPackageInstaller
PRODUCT_PACKAGES += TMSBrowser
PRODUCT_PACKAGES += HMBFileManager
PRODUCT_PACKAGES += HMBSpiderMan
PRODUCT_PACKAGES += HMBGallery
PRODUCT_PACKAGES += HMBVrCamera
PRODUCT_PACKAGES += VrBrowser
PRODUCT_PACKAGES += HMBApplicationsProvider
PRODUCT_PACKAGES += MJWeatherBox
PRODUCT_PACKAGES += TencentVideo
PRODUCT_PACKAGES += AMAP
#PRODUCT_PACKAGES += TencentMarket
PRODUCT_PACKAGES += Tuniu
PRODUCT_PACKAGES += EliteLife
PRODUCT_PACKAGES += HeZhong
PRODUCT_PACKAGES += LeYao
PRODUCT_PACKAGES += DouWong
#PRODUCT_PACKAGES += XunDaoBao
PRODUCT_PACKAGES += HMBDownloadProvider
PRODUCT_PACKAGES += HMBLockScreen
PRODUCT_PACKAGES += qqmusic
PRODUCT_PACKAGES += HMBCalculator
PRODUCT_PACKAGES += Account
PRODUCT_PACKAGES += HMBUploadPhoneInfo
PRODUCT_PACKAGES += moffice
#PRODUCT_PACKAGES += VR_FaceTime
#PRODUCT_PACKAGES += VrLive
PRODUCT_PACKAGES += appStore
PRODUCT_PACKAGES += HMBVRWallPaper
PRODUCT_PACKAGES += TencentNLP
PRODUCT_PACKAGES += SoundRecorder
PRODUCT_PACKAGES += Sina_Weibo

PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_M_HOME)/HMBPermissionControl/HMBPermissionControl.mpinfo,system/plugin/HMBPermissionControl)

PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_M_HOME)/HMBThemeManager/bin/preinstall.sh,system/bin)


PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_M_HOME)/HMBThemeManager/media/bootanimation.zip,system/media)

PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_M_HOME)/HMBVrCamera/lib/libjni_jpegdecoder_vr.so,system/lib) \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_M_HOME)/HMBVrCamera/lib/libjni_logo_decoder.so,system/lib) \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_M_HOME)/HMBVrCamera/lib64/libjni_jpegdecoder_vr.so,system/lib64) \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_M_HOME)/HMBVrCamera/lib64/libjni_logo_decoder.so,system/lib64) \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_M_HOME)/HMBVrCamera/beautylibs/arm64-v8a/libjni_st_facebeauty.so,system/lib64) \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_M_HOME)/HMBVrCamera/beautylibs/arm64-v8a/libst_facebeauty.so,system/lib64) \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_M_HOME)/HMBVrCamera/beautylibs/arm64-v8a/libst_mobile.so,system/lib64) \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_M_HOME)/HMBVrCamera/beautylibs/arm64-v8a/libstmobile_jni.so,system/lib64) \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_M_HOME)/HMBVrCamera/beautylibs/armeabi-v7a/libst_facebeauty.so,system/lib) \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_M_HOME)/HMBVrCamera/beautylibs/armeabi-v7a/libjni_st_facebeauty.so,system/lib) 


PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_M_HOME)/3rd-party/TMSBrowser/config/browser_config.xml,system/hummingbird/config)

PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_M_HOME)/HMBThemeManager/system/hummingbird/theme/theme_info/Fashion/description.xml,system/hummingbird/theme/theme_info/Fashion) \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_M_HOME)/HMBThemeManager/system/hummingbird/theme/theme_info/Fashion/previews/*.jpg,system/hummingbird/theme/theme_info/Fashion/previews) \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_M_HOME)/HMBThemeManager/system/hummingbird/theme/theme_pkg/Fashion,system/hummingbird/theme/theme_pkg)

PRODUCT_COPY_FILES += \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_M_HOME)/HMBVrCamera/preinstall/*.jpg,system/backup/vrcamera/) \
        $(call all_files_under_subdir,$(ANDROID_PARTNER_M_HOME)/HMBVrCamera/preinstall/*.png,system/backup/vrcamera/)
