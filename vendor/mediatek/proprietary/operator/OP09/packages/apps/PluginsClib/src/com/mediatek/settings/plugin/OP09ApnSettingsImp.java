package com.mediatek.settings.plugin;

import java.util.ArrayList;
import java.util.Arrays;


import android.content.Context;
import android.preference.Preference;
import android.telephony.TelephonyManager;
import android.telephony.SubscriptionManager;
import android.util.Log;

import com.mediatek.common.PluginImpl;
import com.mediatek.op09.plugin.R;
import com.mediatek.settings.ext.DefaultApnSettingsExt;
import com.mediatek.telephony.TelephonyManagerEx;

/**
 * APN CT feature.
 */
@PluginImpl(interfaceName = "com.mediatek.settings.ext.IApnSettingsExt")
public class OP09ApnSettingsImp extends DefaultApnSettingsExt {

    private static final String TAG = "OP09ClibApnSettingsImp";
    private Context mContext;
    private static final String CT_NUMERIC_1 = "46011";
    private static final String CT_NUMERIC_2 = "46003";

    /**
     * Constructor method.
     * @param context is Settings's context.
     */
    public OP09ApnSettingsImp(Context context) {
        mContext = context;
    }

    /**
     * Customize the unselected APN.
     * @param mnoApnList the unselectable mno apn list.
     * @param mvnoApnList the unselectable mvno apn list.
     * @param subId the subscription ID.
     */
    @Override
    public void customizeUnselectableApn(ArrayList<Preference> mnoApnList,
        ArrayList<Preference> mvnoApnList, int subId) {
        TelephonyManagerEx telephonyManagerEx = TelephonyManagerEx.getDefault();
        String values[] = telephonyManagerEx.getSupportCardType(
            SubscriptionManager.getSlotId(subId));
        Log.d(TAG, "customizeUnselectableApn cardType = "
                + Arrays.toString(values));

        TelephonyManager tm =
            (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        /// isCtCard will be true.
        boolean isCtCard = false;
        if (values != null) {
            for (int i = 0; i < values.length; i++) {
                if ("RUIM".equals(values[i])) {
                    isCtCard = true;
                    break;
                } else if ("CSIM".equals(values[i])) {
                    isCtCard = true;
                    break;
                }
            }
        }
        ///simMccMnc means is CT card, 46011 means is CT4G card,46003 means is CT3G card.
        ///networkType means ps data, when is NETWORK_TYPE_LTE or NETWORK_TYPE_EHRPD,
        ///dataMccMnc will be true.
        String simMccMnc = tm.getSimOperator(subId);
        int networkType = tm.getNetworkType(subId);
        boolean dataMccMnc = false;
        if (simMccMnc != null
                && (simMccMnc.equals(CT_NUMERIC_1) || simMccMnc.equals(CT_NUMERIC_2))) {
            if (TelephonyManager.NETWORK_TYPE_LTE == networkType
                    || TelephonyManager.NETWORK_TYPE_EHRPD == networkType ) {
                dataMccMnc = true;
            }
        }

        if (isCtCard && dataMccMnc) {
            if (mnoApnList != null && mnoApnList.size() > 0) {
                mnoApnList.remove(mnoApnList.size() - 1);
            }
        }

        Log.d(TAG, "simMccMnc = " + simMccMnc
                + " dataMccMnc = " + dataMccMnc + " networkType = " + networkType);
    }
}
