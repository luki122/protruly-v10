#include <utils/Log.h>
#include <fcntl.h>
#include <math.h>

#include "pd_buf_mgr.h"

class PD_S5K2L7MIPIRAW : protected PDBufMgr
{
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Ctor/Dtor.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
private:    ////    Disallowed.
    MUINT32  m_PDBufSz;
    MUINT16 *m_PDBuf;
    MUINT32  m_PDXSz; //pixels in two byte.
    MUINT32  m_PDYSz; //lines

protected :
    /**
    * @brief checking current sensor is supported or not.
    */
    MBOOL IsSupport( SPDProfile_t &iPdProfile);
    /**
    * @brief seprate LR PD
    */
    void seprate( int stride, unsigned char *ptr, int pd_x_num, int pd_y_num, unsigned short *ptrLROut);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Operations.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:
    PD_S5K2L7MIPIRAW();
    ~PD_S5K2L7MIPIRAW();

    static PDBufMgr* getInstance();

    /**
    * @brief get PD calibration data size.
    */
    MINT32 GetPDCalSz();
    /**
    * @brief convert PD data buffer format.
    */
    MUINT16* ConvertPDBufFormat( MUINT32 i4Size, MUINT32 i4Stride, MUINT8 *ptrBufAddr, MUINT32 i4FrmCnt);
    /**
    * @brief output PDO information
    */
    MBOOL GetPDOHWInfo( MINT32 i4CurSensorMode, SPDOHWINFO_T &oPDOhwInfo);
    /**
    * @brief output DualPD VC information
    */
#if MTK_CAM_HAVE_DUALPD_SUPPORT
    MBOOL GetDualPDVCInfo( MINT32 i4CurSensorMode, SDUALPDVCINFO_T &oDualPDVChwInfo, MINT32 i4AETargetMode);
#endif
};

