/* 
* Copyright (C) 2016 Fourier Semiconductor Inc. 
*
*/

#ifndef FS_I2C_H
#define FS_I2C_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

enum FS_I2C_Error {
    FS_I2C_InvalidDev = -2,
    FS_I2C_InvalidData = -1,
    FS_I2C_OK = 0,
    FS_I2C_NoAck,
    FS_I2C_TimeOut
};

typedef enum FS_I2C_Error FS_I2C_Error_t;

#define FS_I2C_MAX_SIZE 512

/*  I2C write operation.
*   @devAddr = I2C device address
*   @nWriteBytes = size of data in bytes
*   @data = data array in bytes to write
*/
FS_I2C_Error_t FS_I2C_Write(uint8_t devAddr, int nWriteBytes, const uint8_t *data);

/*  I2C read nReadBytes bytes.
*   The readBuffer must be big enough to hold nReadBytes.
*   @devAddr = I2C device address
*   @nWriteBytes = size of data in bytes
*   @writeData = data array in bytes to write
*   @nReadBytes = size of readBuffer and number of bytes to read
*   @readBuffer = byte buffer to receive the read data
*/
FS_I2C_Error_t FS_I2C_WriteRead(uint8_t devAddr, 
    int nWriteBytes, 
    const uint8_t *writeData,
    int nReadBytes,
    const uint8_t *readBuffer);

/* I2c interface */
int FS_I2C_Interface(char *targetName, 
    int (*init)(char *devName),
    int (*write)(int fd, int size, uint8_t *buffer, unsigned int *pError),
    int (*write_read)(int fd, int nWriteBytes, const uint8_t* writeData,
        int nReadBytes, uint8_t *readData, unsigned int *pError),
    int (*get_version)(char *buffer, int fd));

#ifdef __cplusplus
}
#endif
#endif
