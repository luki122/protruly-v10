
package com.zte.engineer;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.os.storage.StorageManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import java.util.List;
import java.util.Collections;
import android.os.storage.VolumeInfo;
import java.io.File;
import android.content.Intent;
import android.hardware.usb.UsbManager;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.os.storage.StorageEventListener;
import android.os.storage.VolumeRecord;
import android.os.storage.DiskInfo;
import android.widget.Toast;
import android.view.KeyEvent;
import android.provider.Settings;

public class SDcardTest extends ZteActivity {

    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            showOtg();
            /*boolean available = (intent.getBooleanExtra(UsbManager.USB_CONFIGURED, false) &&
                               intent.getBooleanExtra(UsbManager.USB_FUNCTION_MASS_STORAGE, false));

            android.util.Log.i("chengrq", "onReceive=" + intent.getAction() + ",available=" + available);

           if(available){
               showOtg();
           }else{
               dismissOtg();
           }
           */
        }
    };

    private TextView mTextView3;
    private TextView mSDStatus3;
    private TextView mSDTotal3;
    private TextView mBSDUsed3;
    private TextView mBSDAvailable3;
    private StorageManager mStorageManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // hide title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.normal);

        boolean isPlusIn = false;
        long mSDTotalCount;
        long mSDAvailableCount;
        long mSDUsedCount;

        TextView mTextView = (TextView) findViewById(R.id.normal_textview);
        TextView mSDStatus = (TextView) findViewById(R.id.normal_textview2);
        TextView mSDTotal = (TextView) findViewById(R.id.normal_textview3);
        TextView mBSDUsed = (TextView) findViewById(R.id.normal_textview4);
        TextView mBSDAvailable = (TextView) findViewById(R.id.normal_textview5);

        boolean isPlusIn2 = false;
        long mSDTotalCount2;
        long mSDAvailableCount2;
        long mSDUsedCount2;

        TextView mTextView2 = (TextView) findViewById(R.id.normal_textview7);
        TextView mSDStatus2 = (TextView) findViewById(R.id.normal_textview9);
        TextView mSDTotal2 = (TextView) findViewById(R.id.normal_textview10);
        TextView mBSDUsed2 = (TextView) findViewById(R.id.normal_textview11);
        TextView mBSDAvailable2 = (TextView) findViewById(R.id.normal_textview12);
        // add usb otg storage info begin
        boolean isPlusIn3 = false;
        long mSDTotalCount3;
        long mSDAvailableCount3;
        long mSDUsedCount3;

        mTextView3 = (TextView) findViewById(R.id.normal_textview14);
        mSDStatus3 = (TextView) findViewById(R.id.normal_textview15);
        mSDTotal3 = (TextView) findViewById(R.id.normal_textview16);
        mBSDUsed3 = (TextView) findViewById(R.id.normal_textview17);
        mBSDAvailable3 = (TextView) findViewById(R.id.normal_textview18);
        // add usb otg storage info end

        try {
            mStorageManager = (StorageManager) getSystemService(Context.STORAGE_SERVICE);
            String[] storagePathList = mStorageManager.getVolumePaths();
            if (storagePathList.length >= 1) {
                String state = mStorageManager.getVolumeState(storagePathList[0]);
                isPlusIn = Environment.MEDIA_MOUNTED.equals(state);
                mTextView.setText(R.string.sd_info);
                StatFs stat = new StatFs(storagePathList[0]);
                if (true == isPlusIn) {
                    mSDTotalCount = Util.getTotalBytes(stat) / 1024 / 1024;
                    mSDAvailableCount = Util.getAvailableBytes(stat) / 1024 / 1024; // MB
                    mSDUsedCount = mSDTotalCount - mSDAvailableCount;
                    mSDStatus.setText(getString(R.string.state) + getString(R.string.sd_mounted));
                } else {
                    mSDTotalCount = 0;
                    mSDAvailableCount = 0;
                    mSDUsedCount = 0;
                    mSDStatus.setText(getString(R.string.state) + getString(R.string.sd_removed));
                }

                mSDTotal.setText(String.format(getString(R.string.sd_total),
                        Long.toString(mSDTotalCount)));
                mBSDUsed.setText(String.format(getString(R.string.sd_used), Long.toString(mSDUsedCount)));
                mBSDAvailable.setText(String.format(getString(R.string.sd_available),
                        Long.toString(mSDAvailableCount)));
            }
            if (storagePathList.length >= 2) {
                String state = mStorageManager.getVolumeState(storagePathList[1]);
                isPlusIn2 = Environment.MEDIA_MOUNTED.equals(state);
                mTextView2.setText(R.string.sd2_info);
                StatFs stat = new StatFs(storagePathList[1]);
                if (true == isPlusIn2) {
                    mSDTotalCount2 = Util.getTotalBytes(stat) / 1024 / 1024;
                    mSDAvailableCount2 = Util.getAvailableBytes(stat) / 1024 / 1024;
                    mSDUsedCount2 = mSDTotalCount2 - mSDAvailableCount2;
                    mSDStatus2.setText(getString(R.string.state) + getString(R.string.sd_mounted));
                } else {
                    mSDTotalCount2 = 0;
                    mSDAvailableCount2 = 0;
                    mSDUsedCount2 = 0;
                    mSDStatus2.setText(getString(R.string.state) + getString(R.string.sd_removed));
                }

                mSDTotal2.setText(String.format(getString(R.string.sd_total),
                        Long.toString(mSDTotalCount2)));
                mBSDUsed2.setText(String.format(getString(R.string.sd_used),
                        Long.toString(mSDUsedCount2)));
                mBSDAvailable2.setText(String.format(getString(R.string.sd_available),
                        Long.toString(mSDAvailableCount2)));
            }
            if (storagePathList.length >= 3) {
                String state = mStorageManager.getVolumeState(storagePathList[2]);
                isPlusIn3 = Environment.MEDIA_MOUNTED.equals(state);
                mTextView3.setText(R.string.otg_info);
                StatFs stat = new StatFs(storagePathList[2]);
                if (true == isPlusIn3) {
                    mSDTotalCount3 = Util.getTotalBytes(stat) / 1024 / 1024;
                    mSDAvailableCount3 = Util.getAvailableBytes(stat) / 1024 / 1024;
                    mSDUsedCount3 = mSDTotalCount3 - mSDAvailableCount3;
                    mSDStatus3.setText(getString(R.string.state) + getString(R.string.sd_mounted));
                } else {
                    mSDTotalCount3 = 0;
                    mSDAvailableCount3 = 0;
                    mSDUsedCount3 = 0;
                    mSDStatus3.setText(getString(R.string.state) + getString(R.string.sd_removed));
                }

                mSDTotal3.setText(String.format(getString(R.string.sd_total),
                        Long.toString(mSDTotalCount3)));
                mBSDUsed3.setText(String.format(getString(R.string.sd_used),
                        Long.toString(mSDUsedCount3)));
                mBSDAvailable3.setText(String.format(getString(R.string.sd_available),
                        Long.toString(mSDAvailableCount3)));
            }
            // add usb otg info end
        } catch (Exception e) {
            Toast.makeText(SDcardTest.this, R.string.sd_invalid_path, Toast.LENGTH_SHORT).show();
            finishSelf(RESULT_FAIL);
            return;
        }

        showOtg();
        //this.registerReceiver(mUsbReceiver, new IntentFilter(UsbManager.ACTION_USB_STATE));
        mStorageManager.registerListener(mListener);
        //end

        /*
         * if(false == isPlusIn) { String storageDirectory =
         * Environment.getExternalStorageDirectory().toString(); StatFs stat =
         * new StatFs(storageDirectory); mSDTotalCount = (long)
         * stat.getBlockCount() * stat.getBlockSize() / 1024 / 1024;
         * mSDAvailableCount = (long) stat.getAvailableBlocks() *
         * stat.getBlockSize() / 1024 / 1024; mSDUsedCount = mSDTotalCount -
         * mSDAvailableCount; mSDStatus.setText(getString(R.string.state) +
         * getString(R.string.sd_mounted)); } else { mSDTotalCount = 0;
         * mSDAvailableCount = 0; mSDUsedCount =0;
         * mSDStatus.setText(getString(R.string.state) +
         * getString(R.string.sd_removed)); }
         * mSDTotal.setText(String.format(getString(R.string.sd_total),
         * Long.toString(mSDTotalCount)));
         * mBSDUsed.setText(String.format(getString(R.string.sd_used),
         * Long.toString(mSDUsedCount)));
         * mBSDAvailable.setText(String.format(getString(R.string.sd_available),
         * Long.toString(mSDAvailableCount)));
         */
        ((Button) findViewById(R.id.btnPass)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnFail)).setOnClickListener(this);

    }
	
    @Override
    protected void onResume() {
        // registerReceiver(screenoff, new IntentFilter(Screenoff));
        Settings.System.putInt(getContentResolver(), "AEON_KEY_TEST", 1);
        super.onResume();

        // PhoneWindowManager.setKeyTestState(true);
    }

    @Override
    protected void onPause() {
        // unregisterReceiver(screenoff);
        Settings.System.putInt(getContentResolver(), "AEON_KEY_TEST", 0);
        super.onPause();

        // PhoneWindowManager.setKeyTestState(false);
    }	
	
    private void onVolumeStateChangedInternal(VolumeInfo vol) {
        android.util.Log.e("chengrq","state:"+vol.getState());
        switch (vol.getType()) {
            case VolumeInfo.TYPE_PUBLIC:
                //onPublicVolumeStateChangedInternal(vol);
                showOtg();
                break;
        }
    }

    private final StorageEventListener mListener = new StorageEventListener() {
        @Override
        public void onVolumeStateChanged(VolumeInfo vol, int oldState, int newState) {
            onVolumeStateChangedInternal(vol);
        }

        @Override
        public void onVolumeRecordChanged(VolumeRecord rec) {
            // Avoid kicking notifications when getting early metadata before
            // mounted. If already mounted, we're being kicked because of a
            // nickname or init'ed change.
            final VolumeInfo vol = mStorageManager.findVolumeByUuid(rec.getFsUuid());
            if (vol != null && vol.isMountedReadable()) {
                onVolumeStateChangedInternal(vol);
            }
        }

        @Override
        public void onVolumeForgotten(String fsUuid) {
            // Stop annoying the user
            //mNotificationManager.cancelAsUser(fsUuid, PRIVATE_ID, UserHandle.ALL);
        }

        @Override
        public void onDiskScanned(DiskInfo disk, int volumeCount) {
            //onDiskScannedInternal(disk, volumeCount);
        }

        // M: add for ALPS02472594 @ {
        @Override
        public void onDiskDestroyed(DiskInfo disk) {
            //mNotificationManager.cancelAsUser(disk.getId(), DISK_ID, UserHandle.ALL);
        }
        // @ }
    };

    private void dismissOtg() {
        mTextView3.setText("");
        mSDStatus3.setText("");
        mSDTotal3.setText("");
        mBSDUsed3.setText("");
        mBSDAvailable3.setText("");
        
        mTextView3.invalidate();
        mSDStatus3.invalidate();
        mSDTotal3.invalidate();
        mBSDUsed3.invalidate();
        mBSDAvailable3.invalidate();
    }

    private void showOtg(){
        if (Build.VERSION.SDK_INT >= 23){
            //chengrq start
            long publicAvailableBytes = 0;
            long publicTotalBytes = 0;
            long publicUsedBytes = 0;
            
            dismissOtg();
            
            final List<VolumeInfo> volumes = mStorageManager.getVolumes();
            Collections.sort(volumes, VolumeInfo.getDescriptionComparator());
            for (VolumeInfo vol : volumes) {
                if (vol.getType() == VolumeInfo.TYPE_PUBLIC && vol.isUSBOTG()) {
                    final File path = vol.getPath();
                    
                    if(path == null)
                        return;
                    
                    final long freeBytes = (long)path.getFreeSpace() / 1024 / 1024;
                    final long totalBytes = (long) path.getTotalSpace() / 1024 / 1024;
                    final long usedBytes = (long)(totalBytes - freeBytes);
                    
                    android.util.Log.e("chengrq","path:"+path.toString());
                    
                    android.util.Log.e("chengrq","freeBytes:"+freeBytes);
                    android.util.Log.e("chengrq","totalBytes:"+totalBytes);
                    android.util.Log.e("chengrq","usedBytes:"+usedBytes);
                    
                    mTextView3.setText(R.string.otg_info);
                    mSDStatus3.setText(getString(R.string.state) + getString(R.string.sd_mounted));
                    mSDTotal3.setText(String.format(getString(R.string.sd_total),
                        Long.toString(totalBytes)));
                    mBSDUsed3.setText(String.format(getString(R.string.sd_used),
                        Long.toString(usedBytes)));
                    mBSDAvailable3.setText(String.format(getString(R.string.sd_available),
                        Long.toString(freeBytes)));
                        
                    mTextView3.invalidate();
                    mSDStatus3.invalidate();
                    mSDTotal3.invalidate();
                    mBSDUsed3.invalidate();
                    mBSDAvailable3.invalidate();    
                }
            }
            //invalidate();
        }
    }

}
