/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#define LOG_TAG "aaa_hal_sttCtrl"

#include <Trace.h>
#include <sys/stat.h>
#include <cutils/properties.h>
#include <string.h>

#include <isp_tuning.h>
#include <aaa_hal_sttCtrl.h>
#include <aaa_types.h>
#include <aaa_error_code.h>
#include <aaa_log.h>
#include <IHalSensor.h>

#include <CamIO/PortMap.h>
#include <CamIO/IHalCamIO.h>
#include <CamIO/INormalPipe.h>
#include <debug/DebugUtil.h>


#if defined(HAVE_AEE_FEATURE)
#include <aee.h>
#define AEE_ASSERT_3A_HAL(String) \
          do { \
              aee_system_exception( \
                  "Hal3ASttCtrl", \
                  NULL, \
                  DB_OPT_DEFAULT, \
                  String); \
          } while(0)
#else
#define AEE_ASSERT_3A_HAL(String)
#endif

using namespace NS3Av3;
using namespace NSCam;

Hal3ASttCtrl*
Hal3ASttCtrl::
createInstance(MINT32 i4SensorDevId, MINT32 i4SensorOpenIndex)
{
    MY_LOG("Hal3ASttFlow::createInstance i4SensorDevId(%d), i4SensorOpenIndex(%d)\n"
           , i4SensorDevId
           , i4SensorOpenIndex);

    switch (i4SensorDevId)
    {
        case ESensorDev_Main:
            return Hal3ASttCtrlDev<ESensorDev_Main>::getInstance();
        break;
        case ESensorDev_Sub:
            return Hal3ASttCtrlDev<ESensorDev_Sub>::getInstance();
        break;
        case ESensorDev_MainSecond:
            return Hal3ASttCtrlDev<ESensorDev_MainSecond>::getInstance();
        break;
#ifdef MTK_SUB2_IMGSENSOR
        case ESensorDev_SubSecond:
            return Hal3ASttCtrlDev<ESensorDev_SubSecond>::getInstance();
        break;
#endif
        default:
            MY_ERR("Unsupport sensor device ID: %d\n", i4SensorDevId);
            AEE_ASSERT_3A_HAL("Unsupport sensor device.");
            return MNULL;
    }
}

Hal3ASttCtrl*
Hal3ASttCtrl::
getInstance(MINT32 i4SensorDevId)
{
    switch (i4SensorDevId)
    {
        case ESensorDev_Main:
            return Hal3ASttCtrlDev<ESensorDev_Main>::getInstance();
        case ESensorDev_Sub:
            return Hal3ASttCtrlDev<ESensorDev_Sub>::getInstance();
        case ESensorDev_MainSecond:
            return Hal3ASttCtrlDev<ESensorDev_MainSecond>::getInstance();
#ifdef MTK_SUB2_IMGSENSOR
        case ESensorDev_SubSecond:
            return Hal3ASttCtrlDev<ESensorDev_SubSecond>::getInstance();
#endif
        default:
            MY_ERR("Unsupport sensor device ID: %d\n", i4SensorDevId);
            AEE_ASSERT_3A_HAL("Unsupport sensor device.");
            return MNULL;
    }

}

MVOID
Hal3ASttCtrl::
destroyInstance()
{
    MY_LOG("[%s] \n", __FUNCTION__);
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Hal3ASttCtrl::Hal3ASttCtrl()
    : m_Users(0)
    , m_Lock()
    , m_pPDAFStatus(FEATURE_PDAF_UNSUPPORT)
    , m_pMvHDRStatus(FEATURE_MVHDR_UNSUPPORT)
    , rAAOBufThread(NULL)
    , rAFOBufThread(NULL)
    , rPDOBufThread(NULL)
    , m_pSttPipe(NULL)
{
    for(int i = 0; i < EPIPE_CAMSV_FEATURE_NUM; i++)
        m_pCamsvSttPipe[i] = NULL;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Hal3ASttCtrl::~Hal3ASttCtrl()
{}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL
Hal3ASttCtrl::
initStt(MINT32 i4SensorDevId, MINT32 i4SensorOpenIndex, MINT32 const i4SubsampleCount)
{
    MY_LOG("[%s] i4SensorDevId(%d), i4SensorOpenIndex(%d)\n"
           , __FUNCTION__
           , i4SensorDevId
           , i4SensorOpenIndex);

    Mutex::Autolock lock(m_Lock);

    // check user count
    if (m_Users > 0)
    {
        MY_LOG_IF(1,"%d has created \n", m_Users);
        android_atomic_inc(&m_Users);
        return MTRUE;
    }
    char value[PROPERTY_VALUE_MAX] = {'\0'};
    property_get("debug.stt_flow.enable", value, "7");
    m_i4SttPortEnable = atoi(value);
    m_i4SensorDev = i4SensorDevId;
    m_i4SensorIdx = i4SensorOpenIndex;

    querySensorStaticInfo();

    m_pBufMgrList[BUF_AAO] = IBufMgr::createInstance(BUF_AAO,m_i4SensorDev,m_i4SensorIdx);
    m_pBufMgrList[BUF_AFO] = IBufMgr::createInstance(BUF_AFO,m_i4SensorDev,m_i4SensorIdx);
    if(isFlkEnable())
        m_pBufMgrList[BUF_FLKO] = IBufMgr::createInstance(BUF_FLKO,m_i4SensorDev,m_i4SensorIdx);
    else
        m_pBufMgrList[BUF_FLKO] = NULL;
    m_pBufMgrList[BUF_PDO] = IBufMgr::createInstance(BUF_PDO,m_i4SensorDev,m_i4SensorIdx);
    m_pBufMgrList[BUF_MVHDR] = IBufMgr::createInstance(BUF_MVHDR,m_i4SensorDev,m_i4SensorIdx);

    /********************************
     * StatisticPipe init and config
     ********************************/
    {
        CAM_TRACE_BEGIN("3A STT init");
        MY_LOG("[%s] Statistic Pipe createInstance\n", __FUNCTION__);
        m_pSttPipe = IStatisticPipe::createInstance(m_i4SensorIdx, LOG_TAG);
        MY_LOG("[%s] Statistic Pipe init\n", __FUNCTION__);
        if (MFALSE == m_pSttPipe->init()) {
            MY_LOG("IStatisticPipe init fail");
            CAM_TRACE_END();
            return MFALSE;
        }
        CAM_TRACE_END();

        // Check FLK enable or not by normal pipe information
        MBOOL enable_FLK = isFlkEnable();
        MY_LOG("[%s] enable_FLK = %d, SensorIdx = %d\n", __FUNCTION__, (int)enable_FLK, m_i4SensorIdx);

        MY_LOG("[%s] Statistic Pipe config\n", __FUNCTION__);
        std::vector<statPortInfo> vp;
        QInitStatParam statParm(vp);
        if (m_i4SttPortEnable & ENABLE_STT_FLOW_AAO)
            statParm.mStatPortInfo.push_back(statPortInfo(PORT_AAO));
        if (m_i4SttPortEnable & ENABLE_STT_FLOW_AFO)
            statParm.mStatPortInfo.push_back(statPortInfo(PORT_AFO));
        if ((m_i4SttPortEnable & ENABLE_STT_FLOW_FLKO) && enable_FLK)
            statParm.mStatPortInfo.push_back(statPortInfo(PORT_FLKO));
        if(m_pPDAFStatus == FEATURE_PDAF_SUPPORT_PDO)
            statParm.mStatPortInfo.push_back(statPortInfo(PORT_PDO, m_u4TGSizeW, m_u4TGSizeH));

        CAM_TRACE_BEGIN("3A STT configPipe");
        m_pSttPipe->configPipe(statParm, i4SubsampleCount);
        CAM_TRACE_END();
    }

    /**********************************
     * virtual channal init and config
     **********************************/
    if(m_pPDAFStatus == FEATURE_PDAF_SUPPORT_VIRTUAL_CHANNEL ||
       m_pMvHDRStatus == FEATURE_MVHDR_SUPPORT_VIRTUAL_CHANNEL )
    {
        if(m_pPDAFStatus == FEATURE_PDAF_SUPPORT_VIRTUAL_CHANNEL)
            m_pCamsvSttPipe[EPIPE_CAMSV_FEATURE_PDAF] = ICamsvStatisticPipe::createInstance(m_i4SensorIdx, LOG_TAG, EPIPE_CAMSV_FEATURE_PDAF);
        if(m_pMvHDRStatus == FEATURE_MVHDR_SUPPORT_VIRTUAL_CHANNEL)
            m_pCamsvSttPipe[EPIPE_CAMSV_FEATURE_MVHDR] = ICamsvStatisticPipe::createInstance(m_i4SensorIdx, LOG_TAG, EPIPE_CAMSV_FEATURE_MVHDR);

        for(int i = 0; i < EPIPE_CAMSV_FEATURE_NUM; i++)
        {
            if(m_pCamsvSttPipe[i] == NULL)
            {
                MY_LOG("ICamsvStatisticPipe[%d] createInstance fail", i);
                continue;
            }
            MY_LOG("[%s] Virtual Channal Pipe(%d) init\n", __FUNCTION__, i);
            if (MFALSE == m_pCamsvSttPipe[i]->init()) {
                MY_LOG("ICamsvStatisticPipe[%d] init fail", i);
                m_pCamsvSttPipe[i]->destroyInstance(LOG_TAG);
                m_pCamsvSttPipe[i] = NULL;
                CAM_TRACE_END();
                continue;
            }
            CAM_TRACE_END();

            MBOOL bRet = m_pCamsvSttPipe[i]->sendCommand(ECAMSVSPipeCmd_SET_CAMSV_SENARIO_ID, (MINTPTR)&m_i4SensorMode, NULL,NULL);

            CAM_TRACE_BEGIN("3A CamsvSTT configPipe");
            std::vector<statPortInfo> vp;
            QInitStatParam statParm(vp);
            if(MFALSE == m_pCamsvSttPipe[i]->configPipe(statParm))
            {
                MY_LOG("ICamsvStatisticPipe[%d] configPipe fail", i);
                m_pCamsvSttPipe[i]->uninit();
                m_pCamsvSttPipe[i]->destroyInstance(LOG_TAG);
                m_pCamsvSttPipe[i] = NULL;
            }
            CAM_TRACE_END();
        }
    }
    android_atomic_inc(&m_Users);
    return MTRUE;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL
Hal3ASttCtrl::
uninitStt()
{
    MY_LOG("[%s] \n", __FUNCTION__);

    Mutex::Autolock lock(m_Lock);

    // If no more users, return directly and do nothing.
    if (m_Users <= 0)
    {
        return MTRUE;
    }

    // More than one user, so decrease one User.
    android_atomic_dec(&m_Users);

    if(m_Users == 0)
    {
        // Statistic Pipe uninit
        if(m_pSttPipe != NULL)
        {
            CAM_TRACE_BEGIN("3A STT uninit");
            m_pSttPipe->uninit();
            m_pSttPipe->destroyInstance(LOG_TAG);
            m_pSttPipe = NULL;
            CAM_TRACE_END();
        }

        for(int i = 0; i < EPIPE_CAMSV_FEATURE_NUM; i++)
        {
            if(m_pCamsvSttPipe[i] != NULL)
            {
                // Virtaul channal uninit
                CAM_TRACE_BEGIN("3A CamsvSTT uninit");
                m_pCamsvSttPipe[i]->uninit();
                m_pCamsvSttPipe[i]->destroyInstance(LOG_TAG);
                m_pCamsvSttPipe[i] = NULL;
                CAM_TRACE_END();
            }
        }

        for(int i = 0; i < BUF_NUM; i++)
        {
            if(m_pBufMgrList[i] != NULL)
            {
                m_pBufMgrList[i]->destroyInstance();
                m_pBufMgrList[i] = NULL;
            }
        }
    }
    return MTRUE;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL
Hal3ASttCtrl::
startStt()
{
    MY_LOGW("[%s] m_i4SttPortEnable = %d \n", __FUNCTION__, m_i4SttPortEnable);
    if(m_pSttPipe != NULL)
        m_pSttPipe->start();

    for(int i = 0; i < EPIPE_CAMSV_FEATURE_NUM; i++)
    {
        if(m_pCamsvSttPipe[i] != NULL)
        {
            MY_LOGW("[%s] ICamsvStatisticPipe[%d] start\n", __FUNCTION__, i);
            m_pCamsvSttPipe[i]->start();
        }
    }

    MBOOL enable_FLK = isFlkEnable();
    MY_LOGW("[%s] enable_FLK=%d, SensorDev=%d\n", __FUNCTION__, (int)enable_FLK, m_i4SensorDev);
    char pThreadName[256] = {'\0'};
    if(m_i4SttPortEnable & ENABLE_STT_FLOW_AAO)
    {
        sprintf(pThreadName, "AAOBufThread_%d", m_i4SensorDev);
        std::vector<IBufMgr*> list;
        // add AAO
        list.push_back(m_pBufMgrList[BUF_AAO]);
        // add FLKO
        if((m_i4SttPortEnable & ENABLE_STT_FLOW_FLKO) && enable_FLK)
            list.push_back(m_pBufMgrList[BUF_FLKO]);

        // add mvHDR
        if(m_pMvHDRStatus != FEATURE_MVHDR_UNSUPPORT && m_pCamsvSttPipe[EPIPE_CAMSV_FEATURE_MVHDR] != NULL)
            list.push_back(m_pBufMgrList[BUF_MVHDR]);
        rAAOBufThread = ThreadStatisticBuf::createInstance(m_i4SensorDev, pThreadName, list);
    }
    if(m_i4SttPortEnable & ENABLE_STT_FLOW_AFO)
    {
        sprintf(pThreadName, "AFOBufThread_%d", m_i4SensorDev);
        std::vector<IBufMgr*> list;
        // add AF
        list.push_back(m_pBufMgrList[BUF_AFO]);
        rAFOBufThread = ThreadStatisticBuf::createInstance(m_i4SensorDev, pThreadName, list);
    }
    if((m_pPDAFStatus == FEATURE_PDAF_SUPPORT_VIRTUAL_CHANNEL && m_pCamsvSttPipe[EPIPE_CAMSV_FEATURE_PDAF] != NULL) ||
       (m_pPDAFStatus == FEATURE_PDAF_SUPPORT_PDO && m_pSttPipe != NULL))
    {
        sprintf(pThreadName, "PDOBufThread_%d", m_i4SensorDev);
        std::vector<IBufMgr*> list;
        // add PDAF
        list.push_back(m_pBufMgrList[BUF_PDO]);
        rPDOBufThread = ThreadStatisticBuf::createInstance(m_i4SensorDev, pThreadName, list);
    }
    return MTRUE;
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL
Hal3ASttCtrl::
preStopStt()
{
    MY_LOG("[%s] \n", __FUNCTION__);
    if(rAFOBufThread != NULL)
        rAFOBufThread->waitFinished();
    if(rAAOBufThread != NULL)
        rAAOBufThread->waitFinished();
    if(rPDOBufThread != NULL)
        rPDOBufThread->waitFinished();

    if(m_pSttPipe != NULL)
        m_pSttPipe->stop();
    for(int i = 0; i < EPIPE_CAMSV_FEATURE_NUM; i++)
    {
        if(m_pCamsvSttPipe[i] != NULL)
        m_pCamsvSttPipe[i]->stop();
    }
    return MTRUE;
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL
Hal3ASttCtrl::
stopStt()
{
    MY_LOGW("[%s] \n", __FUNCTION__);
    DebugUtil::getInstance(m_i4SensorDev)->update(LOG_TAG, "stopStt", 0);
    if(rAFOBufThread != NULL){
        DebugUtil::getInstance(m_i4SensorDev)->update(LOG_TAG, "stopStt", 1);
        rAFOBufThread->destroyInstance();
        rAFOBufThread = NULL;
    }
    DebugUtil::getInstance(m_i4SensorDev)->update(LOG_TAG, "stopStt", 2);
    if(rAAOBufThread != NULL){
        DebugUtil::getInstance(m_i4SensorDev)->update(LOG_TAG, "stopStt", 3);
        rAAOBufThread->destroyInstance();
        rAAOBufThread = NULL;
    }
    if(rPDOBufThread != NULL){
        rPDOBufThread->destroyInstance();
        rPDOBufThread = NULL;
    }
    DebugUtil::getInstance(m_i4SensorDev)->update(LOG_TAG, "stopStt", -1);
    return MTRUE;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL
Hal3ASttCtrl::
abortDeque()
{
    MY_LOGW("[%s] \n", __FUNCTION__);
    for(int i = 0; i < BUF_NUM; i++)
    {
        if(m_pBufMgrList[i] != NULL)
        {
            if(i == BUF_MVHDR)
            {
                if(m_pMvHDRStatus != FEATURE_MVHDR_UNSUPPORT && m_pCamsvSttPipe[EPIPE_CAMSV_FEATURE_MVHDR] != NULL)
                    m_pBufMgrList[i]->abortDequeue();
            } else if(i == BUF_PDO)
            {
                if(m_pPDAFStatus != FEATURE_PDAF_UNSUPPORT)
                    m_pBufMgrList[i]->abortDequeue();
            } else
                m_pBufMgrList[i]->abortDequeue();
        }
    }
    return MTRUE;
}


MVOID
Hal3ASttCtrl::
setSensorMode(MINT32 i4SensorMode)
{
    MY_LOG("[%s] i4SensorMode(%d)", __FUNCTION__, i4SensorMode);
    m_i4SensorMode = i4SensorMode;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL
Hal3ASttCtrl::
isFlkEnable()
{
    char value[PROPERTY_VALUE_MAX] = {'\0'};
    MUINT disable;
    property_get("debug.aaa_flk.disable", value, "0");
    disable = atoi(value);

    if(disable)
        return 0;

    MBOOL uni_info = MFALSE;
    INormalPipe* pCamIO;
    if (pCamIO == NULL)
    {
        pCamIO = INormalPipe::createInstance(m_i4SensorIdx, LOG_TAG);
        if (pCamIO == NULL)
        {
            MY_ERR("Fail to create NormalPipe");
            return MFALSE;
        }
    }

    pCamIO->sendCommand(ENPipeCmd_GET_UNI_INFO,(MINTPTR)&uni_info, 0, 0);

    if (pCamIO != NULL)
    {
        pCamIO->destroyInstance(LOG_TAG);
        pCamIO = NULL;
    }

    return uni_info;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL
Hal3ASttCtrl::
isMvHDREnable()
{
    if((m_pMvHDRStatus != FEATURE_MVHDR_UNSUPPORT) &&
       (m_pCamsvSttPipe[EPIPE_CAMSV_FEATURE_MVHDR] != NULL))
        return MTRUE;
    else
        return MFALSE;
}

MVOID
Hal3ASttCtrl::
querySensorStaticInfo()
{
    IHalSensorList* const pIHalSensorList = IHalSensorList::get();

    //Before phone boot up (before opening camera), we can query IHalsensor for the sensor static info (EX: MONO or Bayer)
    SensorStaticInfo sensorStaticInfo;
    IHalSensorList*const pHalSensorList = IHalSensorList::get();
    MY_LOG("Hal3ASttCtrl::querySensorStaticInfo m_i4SensorIdx = %d\n", m_i4SensorIdx);

    IHalSensor* pIHalSensor = pIHalSensorList->createSensor( "sttCtrl", m_i4SensorDev);

    if (!pHalSensorList)
    {
        MY_ERR("IHalSensorList::get() == NULL");
        return;
    }
    pHalSensorList->querySensorStaticInfo(m_i4SensorDev,&sensorStaticInfo);

    MINT32 scen = m_i4SensorMode;
    MUINT32 PDSupport = 0;
    pIHalSensor->sendCommand( m_i4SensorDev, SENSOR_CMD_GET_SENSOR_PDAF_CAPACITY,(MINTPTR)&scen, (MINTPTR)&PDSupport, 0);

#if MTK_CAM_HAVE_DUALPD_SUPPORT
    /*0: NO PDAF, 1: PDAF Raw Data mode, 2:PDAF VC mode(Full), 3:PDAF VC mode(Binning), 4: PDAF DualPD Raw Data mode, 5: PDAF DualPD VC mode*/
    if((sensorStaticInfo.PDAF_Support == 1 || sensorStaticInfo.PDAF_Support == 4) && PDSupport == 1 /*ESensorMode_Capture*/)
#else
    /*0: NO PDAF, 1: PDAF Raw Data mode, 2:PDAF VC mode(HDR), 3:PDAF VC mode(Binning)*/
    if(sensorStaticInfo.PDAF_Support == 1 && PDSupport == 1 /*ESensorMode_Capture*/)
#endif
    {
        MUINT32 u4TGSzW=0, u4TGSzH=0, u4BINSzW=0, u4BINSzH=0;
        NSCam::NSIoPipe::NSCamIOPipe::INormalPipe *pPipe;
        pPipe = NSCam::NSIoPipe::NSCamIOPipe::INormalPipe::createInstance( m_i4SensorIdx, LOG_TAG);

        if( pPipe==NULL)
        {
            MY_ERR( "Fail to create NormalPipe");
        }
        else
        {
            // TG size
            pPipe->sendCommand( NSCam::NSIoPipe::NSCamIOPipe::ENPipeCmd_GET_TG_OUT_SIZE, (MINTPTR)(&u4TGSzW), (MINTPTR)(&u4TGSzH),0);
            // TG after BIN Blk size : for HPF coordinate setting.
            pPipe->sendCommand( NSCam::NSIoPipe::NSCamIOPipe::ENPipeCmd_GET_BIN_INFO, (MINTPTR)(&u4BINSzW), (MINTPTR)(&u4BINSzH), 0);
            // Checking sensor mode.
            MY_LOG( "%s: Dev 0x%04x, TGSZ: W %d, H %d, BINSZ: W %d, H %d",
                    __FUNCTION__,
                    m_i4SensorDev,
                    u4TGSzW,
                    u4TGSzH,
                    u4BINSzW,
                    u4BINSzH);
            //ret = pPipe->sendCommand( cmd, arg1, arg2, arg3);
            pPipe->destroyInstance( LOG_TAG);
        }
#if MTK_CAM_HAVE_DUALPD_SUPPORT
        if((sensorStaticInfo.PDAF_Support == 1 && /* PDAF Raw Data mode is not supported once frontal binning is not enalbed. */
            (u4TGSzW==u4BINSzW && u4TGSzH==u4BINSzH && u4TGSzW!=0 && u4TGSzH!=0 && u4TGSzW==(MUINT32)sensorStaticInfo.captureWidth))
            ||
            (sensorStaticInfo.PDAF_Support == 4))
#else
        /* PDO is not supported once frontal binning is not enalbed. */
        if( u4TGSzW==u4BINSzW && u4TGSzH==u4BINSzH && u4TGSzW!=0 && u4TGSzH!=0 && u4TGSzW==(MUINT32)sensorStaticInfo.captureWidth)
#endif
        {
            m_u4TGSizeW = u4TGSzW;
            m_u4TGSizeH = u4TGSzH;
            m_pPDAFStatus = FEATURE_PDAF_SUPPORT_PDO;
        }
        else
            m_pPDAFStatus = FEATURE_PDAF_UNSUPPORT;
    }
    else if(sensorStaticInfo.PDAF_Support == 2 || sensorStaticInfo.PDAF_Support == 3
#if MTK_CAM_HAVE_DUALPD_SUPPORT
            || sensorStaticInfo.PDAF_Support == 5
#endif
           )
        m_pPDAFStatus = FEATURE_PDAF_SUPPORT_VIRTUAL_CHANNEL;
    else
        m_pPDAFStatus = FEATURE_PDAF_UNSUPPORT;

    // TODO : query HDR mode from sensor info
    if(MTRUE)
        m_pMvHDRStatus = FEATURE_MVHDR_SUPPORT_VIRTUAL_CHANNEL;

    MY_LOG("[%s] SensorDev(%d), SensorOpenIdx(%d), PDAF_Support(%d), mvHDR_Status(%d), SensorMode(%d), PDSupport(%d), PDAFStatus(%d)\n",
        __FUNCTION__, m_i4SensorDev, m_i4SensorIdx, sensorStaticInfo.PDAF_Support, m_pMvHDRStatus, m_i4SensorMode, PDSupport, m_pPDAFStatus);
}


