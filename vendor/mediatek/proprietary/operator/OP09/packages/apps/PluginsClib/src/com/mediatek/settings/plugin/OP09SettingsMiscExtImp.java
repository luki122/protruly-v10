package com.mediatek.settings.plugin;

import android.content.Context;
import android.util.Log;

import com.mediatek.common.PluginImpl;
import com.mediatek.settings.ext.DefaultSettingsMiscExt;

@PluginImpl(interfaceName = "com.mediatek.settings.ext.ISettingsMiscExt")
public class OP09SettingsMiscExtImp extends DefaultSettingsMiscExt{

    private static final String TAG = "OP09ClibSettingsMiscExtImp";
    private Context mContext;
    private static final String VIRTUARWIFI = "02:00:00:00:00:00";

    public OP09SettingsMiscExtImp(Context context) {
        super(context);
        mContext = context;
        Log.d(TAG, "SettingsMiscExt this=" + this);
    }

    @Override
    public String customizeMacAddressString(String macAddressString,
            String unavailable) {
        if (macAddressString != null) {
            Log.d(TAG, "customizeMacAddressString: macAddressString = "
                    + macAddressString);
            if (VIRTUARWIFI.equals(macAddressString)) {
                return unavailable;
            }
        }
        return super.customizeMacAddressString(macAddressString, unavailable);
    }

}
