package android.content.res;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.content.res.HbThemeZip.ThemeFileInfo;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;

/**
 * app自己的资源
 * @author alexluo
 *
 */
public class HbApplicationTheme {
	/**
	 * 当前主题包存放的路径
	 */
	private static final String THEME_PATH = "/data/hummingbird/theme/current/";
	
	/**
	 * 系统主题包名
	 */
	public static final String SYSTEM_RES_ANDROID = "android";
	
	/**
	 * Hb系统主题包名
	 */
	public static final String SYSTEM_RES_HUMMINGBIRD = "com.hb";
	
	private static final String LAUNCHER_ICON_PACKAGE_NAME = "com.hb.publicicon.res";
	
	private static final String THEME_ICON_PACKAGENAME = "icons";
	
    private static final String NINE_PATCH_SUFFIX = ".9.png";
	
	private static final String XML_DRAWABLE_SUFFIX = ".xml";

	public static final int INDEX_COLOR = 0;

	public static final int INDEX_DIMEN = 1;

	public static final int INDEX_INTEGER = 2;

	public static final int INDEX_BOOL = 3;
	
	private Resources mRes;
	
	private String mPackageName;
	
	private ThemeResourceData mData;
	
	protected HbThemeZip mThemeZipFile;
	
	private List<String> mSystemResPkgs = new ArrayList<String>();
	
	
	
	private Map<String,HbApplicationTheme> mSystemTheme = new HashMap<String,HbApplicationTheme>();
	
	public HbApplicationTheme(Resources res,String packageName){
		this.mRes = res;
		this.mPackageName = packageName;
		mData = new ThemeResourceData();
		mData.themePath = THEME_PATH;
		mThemeZipFile = new HbThemeZip(res,mPackageName, mData);
	}
	
	public void update(){
		mThemeZipFile.updateThemeFiles(mRes);
		mSystemResPkgs.clear();
	}
	
	public void addSystemResources(String packageName){
		if(mSystemResPkgs.contains(packageName)){
			return;
		}
		mSystemResPkgs.add(packageName);
	}
	
	/**
	 *初始化系统资源 
	 */
	public void makeSystemTheme(){
		int length = mSystemResPkgs.size();
		if(length > 0){
			try {
				for(int i = 0; i < length;i++){
					String pkgName = mSystemResPkgs.get(i);
					HbApplicationTheme systemTheme = makeSystemTheme(pkgName);
					if(systemTheme != null){
						mSystemTheme.put(pkgName, systemTheme);
					}
				}
				} catch (Exception e) {
				// do nothing
			}
			
		}
	
	}
	/**
	 * 初始化系统资源包，每个app都包含有两个系统资源包
	 * @param pkgName
	 * @return
	 */
	private HbApplicationTheme makeSystemTheme(String pkgName){
		if(SYSTEM_RES_ANDROID.equals(pkgName)){
			return HbSystemTheme.getInstance(mRes, pkgName);
		}else if(SYSTEM_RES_HUMMINGBIRD.equals(pkgName)){
			return HbFrameworkTheme.getInstance(mRes, pkgName);
		}
		return null;
	}
	
	public Drawable getThemeDrawable(Resources res,TypedValue value,String srcName,int id){
		Drawable dr = null;
		
		if(srcName != null && ! "".equals(srcName)){
    		ThemeFileInfo themeFile = getThemeFileInfo(srcName, id);
    		if(themeFile != null){
    			
    			if(srcName.endsWith(NINE_PATCH_SUFFIX)){
    				try {
						dr = HbDrawableUtils.decodeDrawableFromStream(themeFile.inputStream,res);
						
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}finally{
						try{
							if(themeFile.inputStream != null){
								themeFile.inputStream.close();
							}
						}catch(Exception ex){
							
						}
					}
    			}else{
    				if(value.density != themeFile.density){
						value.density = themeFile.density;
					}
					
					dr = Drawable.createFromResourceStream(res, value, themeFile.inputStream, themeFile.srcName, null);
					try {
						if(themeFile.inputStream != null){
							themeFile.inputStream.close();
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}finally{
						try{
							if(themeFile.inputStream != null){
								themeFile.inputStream.close();
							}
						}catch(Exception ex){
							
						}
					}
    			}
    		}
    	}
		return dr;
	}
	
	/**
	 * 每个应用都包含了两个系统资源包，一个是android，一个是hb
	 * @param id
	 * @return  根据目标id返回不同的系统资源包
	 */
	private HbApplicationTheme getSystemTheme(int id){
		HbApplicationTheme systemTheme = null;
		if(id != 0){
			String pkgName = mRes.getResourcePackageName(id);
			systemTheme = mSystemTheme.get(pkgName);
		}
		
		return systemTheme;
	}
	
	
	
	public boolean hasThemeValue(){
		return mThemeZipFile.hasThemeValues();
	}
	
	/**
	 * get resource values from theme(e.g color,dimen
	 * @param id
	 * @return
	 */
	public Integer getThemeValue(int id){
		HbApplicationTheme systemTheme = getSystemTheme(id);
		if(systemTheme != null){
			return systemTheme.getThemeValue(id);
		}
		return mThemeZipFile.getThemeInteger(id);
	}
	
	public String getVectorPath(int id) {
		HbApplicationTheme systemTheme = getSystemTheme(id);
		if(systemTheme != null){
			return systemTheme.getVectorPath(id);
		}
		return mThemeZipFile.getVectorPath(id);
	}
	
	
	/**
	 * get resource values from theme(e.g color,dimen)
	 * @param id
	 * @return
	 */
	public Float getFloatThemeValue(int id){
		HbApplicationTheme systemTheme = getSystemTheme(id);
		if(systemTheme != null){
			return systemTheme.getFloatThemeValue(id);
		}
		return mThemeZipFile.getThemeFloat(id);
	}
	
	/**
	 * 获取主题文件对象，因为每一个APP都可以获取系统内置资源
	 * 和HummingBird控件资源以及APP自己内部的资源，所以在获取
	 * 资源文件对象时要区分资源的来源，并创建不同的资源管理对象
	 * @param themePath
	 * @param id
	 * @return
	 */
	public ThemeFileInfo getThemeFileInfo(String themePath, int id) {
		if(themePath.endsWith(".9.png")){
			themePath = themePath.substring(0, themePath.indexOf(".9.png"));
		}else{
			int indexPoint = themePath.lastIndexOf(".");
			if(indexPoint != -1){
				themePath = themePath.substring(0, themePath.lastIndexOf('.'));
			}
		}
		
		
		HbApplicationTheme systemTheme = getSystemTheme(id);
		if (systemTheme != null) {
			return systemTheme.getThemeFileInfo(themePath, id);
		}
		if(TextUtils.isEmpty(mPackageName)){
			mPackageName = mRes.getResourcePackageName(id);
		}
		return mThemeZipFile.getThemeFileInfo(mPackageName, themePath);
	}
	
	
	private ThemeFileInfo getIcon(String themePath,int id){
		String pkgName = mRes.getResourcePackageName(id);
		if(LAUNCHER_ICON_PACKAGE_NAME.equals(pkgName)){
			
			return mThemeZipFile.getThemeFileInfo(pkgName, themePath);
		}
		return null;
	}
	
	public static  class ThemeResourceData{
		public String themePath;
	}


	public void freeCaches() {
		// TODO Auto-generated method stub
		update();
		 Collection<HbApplicationTheme> systemThemes = mSystemTheme.values();
		 if(systemThemes != null && systemThemes.size() > 0){
			Iterator<HbApplicationTheme> iterator = systemThemes.iterator();
			while(iterator.hasNext()){
				final HbApplicationTheme theme = iterator.next();
				if(theme != null){
					theme.update();
				}
			}
		 }
	}

	public InputStream getAssetsFile(String assetPath) {
		// TODO Auto-generated method stub
		return mThemeZipFile.getAssetsFile(mPackageName,assetPath);
	}



	
	
	
	
	
}
