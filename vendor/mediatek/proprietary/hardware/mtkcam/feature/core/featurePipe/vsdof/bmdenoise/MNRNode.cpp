/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
#include <featurePipe/vsdof/vsdof_common.h>
#include <featurePipe/vsdof/vsdof_data_define.h>

#include "MNRNode.h"

#define PIPE_MODULE_TAG "BMDeNoise"
#define PIPE_CLASS_TAG "MNRNode"
#define PIPE_LOG_TAG PIPE_MODULE_TAG PIPE_CLASS_TAG

// MET tags
#define DO_BM_MNR "doMultiPassNR"

// buffer alloc size
#define BUFFER_ALLOC_SIZE 1
#define TUNING_ALLOC_SIZE 1

// debug settings
#define USE_DEFAULT_ISP 0
#define USE_DEFAULT_SHADING_GAIN 0

#include <PipeLog.h>

#include <DpBlitStream.h>
#include "../util/vsdof_util.h"

#include <camera_custom_nvram.h>
#include <common/vsdof/hal/stereo_common.h>

using namespace NSCam::NSCamFeature::NSFeaturePipe;
using namespace VSDOF::util;
using namespace NS3Av3;
/*******************************************************************************
 *
 ********************************************************************************/
MNRNode::BufferSizeConfig::
BufferSizeConfig()
{
    StereoSizeProvider* sizePrvider = StereoSizeProvider::getInstance();
    StereoArea area;

    {
        area = sizePrvider->getBufferSize(E_BM_DENOISE_HAL_OUT);
        BMDENOISE_BMDN_RESULT_SIZE = MSize(area.size.w, area.size.h);
    }

    debug();
}


MVOID
MNRNode::BufferSizeConfig::debug() const
{
    MY_LOGD("MNRNode debug size======>\n");
    #define DEBUG_MSIZE(sizeCons) \
        MY_LOGD("size: " #sizeCons " : %dx%d\n", sizeCons.w, sizeCons.h);

    DEBUG_MSIZE( BMDENOISE_BMDN_RESULT_SIZE);
}

MNRNode::BufferSizeConfig::
~BufferSizeConfig()
{}
/*******************************************************************************
 *
 ********************************************************************************/
MNRNode::BufferPoolSet::
BufferPoolSet()
{}

MNRNode::BufferPoolSet::
~BufferPoolSet()
{}

MBOOL
MNRNode::BufferPoolSet::
init(const BufferSizeConfig& config)
{
    MY_LOGD("BufferPoolSet init +");

    int allocateSize = BUFFER_ALLOC_SIZE;
    int allocateSize_tuning = TUNING_ALLOC_SIZE;

    CAM_TRACE_NAME("MNRNode::BufferPoolSet::init");
    CAM_TRACE_BEGIN("MNRNode::BufferPoolSet::init=>create buffer pools");

    // MNR only support YUY2
    mpMNR_Input_bufPool = ImageBufferPool::create(
        "mpMNR_Input_bufPool",
        config.BMDENOISE_BMDN_RESULT_SIZE.w,
        config.BMDENOISE_BMDN_RESULT_SIZE.h,
        eImgFmt_YUY2, ImageBufferPool::USAGE_HW, MTRUE);
    if(mpMNR_Input_bufPool == nullptr){
        MY_LOGE("create mpMNR_Input_bufPool failed!");
        return MFALSE;
    }

    mpTuningBuffer_bufPool = TuningBufferPool::create("VSDOF_TUNING_DENOISE", sizeof(dip_x_reg_t));
    if(mpTuningBuffer_bufPool == nullptr){
        MY_LOGE("create mpTuningBuffer_bufPool failed!");
        return MFALSE;
    }
    CAM_TRACE_END();

    CAM_TRACE_BEGIN("MNRNode::BufferPoolSet::init=>allocate buffer pools");
    mpMNR_Input_bufPool->allocate(allocateSize);
    mpTuningBuffer_bufPool->allocate(allocateSize_tuning);
    CAM_TRACE_END();

    MY_LOGD("BufferPoolSet init -");
    return MTRUE;
}

MBOOL
MNRNode::BufferPoolSet::
uninit()
{
    MY_LOGD("+");

    ImageBufferPool::destroy(mpMNR_Input_bufPool);
    TuningBufferPool::destroy(mpTuningBuffer_bufPool);

    MY_LOGD("-");
    return MTRUE;
}
/*******************************************************************************
 *
 ********************************************************************************/
/*******************************************************************************
 *
 ********************************************************************************/
MNRNode::
MNRNode(const char *name,
    Graph_T *graph,
    MINT32 openId)
    : BMDeNoisePipeNode(name, graph)
    , miOpenId(openId)
{
    MY_LOGD("ctor(0x%x)", this);
    this->addWaitQueue(&mRequests);

    miEnableMNR = ::property_get_int32("debug.bmdenoise.mnr", 1);
}
/*******************************************************************************
 *
 ********************************************************************************/
MNRNode::
~MNRNode()
{
    MY_LOGD("dctor(0x%x)", this);
}
/*******************************************************************************
 *
 ********************************************************************************/
MBOOL
MNRNode::
onData(
    DataID id,
    EffectRequestPtr &request)
{
    FUNC_START;
    TRACE_FUNC_ENTER();

    MBOOL ret = MFALSE;
    switch(id)
    {
        case BMDENOISE_RESULT:
            mRequests.enque(request);
            ret = MTRUE;
            break;
        default:
            ret = MFALSE;
            MY_LOGE("unknown data id :%d", id);
            break;
    }

    TRACE_FUNC_EXIT();
    FUNC_END;
    return ret;
}
/*******************************************************************************
 *
 ********************************************************************************/
MBOOL
MNRNode::
onData(
    DataID id,
    ImgInfoMapPtr& pImgInfo)
{
    FUNC_START;
    MY_LOGE("not implemented!");
    FUNC_END;
    return MFALSE;
}
/*******************************************************************************
 *
 ********************************************************************************/
MBOOL
MNRNode::
onInit()
{
    if(!BMDeNoisePipeNode::onInit()){
        MY_LOGE("BMDeNoisePipeNode::onInit() failed!");
        return MFALSE;
    }

    // normal stream
    MY_LOGD("BMMNRNode::onInit=>create normalStream");
    CAM_TRACE_BEGIN("BMMNRNode::onInit=>create normalStream");
    mpINormalStream = NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(miOpenId);

    if (mpINormalStream == NULL)
    {
        MY_LOGE("mpINormalStream create instance for BMMNRNode Node failed!");
        cleanUp();
        return MFALSE;
    }
    mpINormalStream->init(PIPE_LOG_TAG);
    CAM_TRACE_END();

    // BufferPoolSet init
    MY_LOGD("BMMNRNode::onInit=>BufferPoolSet::init");
    CAM_TRACE_BEGIN("BMMNRNode::mBufPoolSet::init");

    mBufPoolSet.init(mBufConfig);
    CAM_TRACE_END();

    MY_LOGD("BMMNRNode::onInit=>create_3A_instance senosrIdx:(%d)", mSensorIdx_Main1);
    CAM_TRACE_BEGIN("BMMNRNode::onInit=>create_3A_instance");
    mp3AHal_Main1 = IHal3A::createInstance(IHal3A::E_Camera_3, mSensorIdx_Main1, "BMMNR_3A_MAIN1");
    MY_LOGD("3A create instance, Main1: %x", mp3AHal_Main1);
    CAM_TRACE_END();

    FUNC_END;
    TRACE_FUNC_EXIT();
    return MTRUE;
}
/*******************************************************************************
 *
 ********************************************************************************/
MBOOL
MNRNode::
onUninit()
{
    CAM_TRACE_NAME("MNRNode::onUninit");
    FUNC_START;
    cleanUp();
    FUNC_END;
    return MTRUE;
}
/*******************************************************************************
 *
 ********************************************************************************/
MVOID
MNRNode::
cleanUp()
{
    FUNC_START;
    if(mpINormalStream != nullptr)
    {
        mpINormalStream->uninit(PIPE_LOG_TAG);
        mpINormalStream->destroyInstance();
        mpINormalStream = nullptr;
    }
    if(mp3AHal_Main1)
    {
        mp3AHal_Main1->destroyInstance("BMMNR_3A_MAIN1");
        mp3AHal_Main1 = NULL;
    }
    mBufPoolSet.uninit();
    FUNC_END;
}
/*******************************************************************************
 *
 ********************************************************************************/
MVOID
MNRNode::
doDataDump(IImageBuffer* pBuf, BMDeNoiseBufferID BID, MUINT32 iReqIdx)
{
    VSDOF_LOGD("doDataDump: BID:%d +", BID);

    char filepath[1024];
    snprintf(filepath, 1024, "/sdcard/bmdenoise/%d/%s", iReqIdx, getName());

    // make path
    VSDOF_LOGD("makePath: %s", filepath);
    makePath(filepath, 0660);

    const char* writeFileName = onDumpBIDToName(BID);

    char writepath[1024];
    snprintf(writepath,
        1024, "%s/%s_%dx%d.yuv", filepath, writeFileName,
        pBuf->getImgSize().w, pBuf->getImgSize().h);

    pBuf->saveToFile(writepath);

    VSDOF_LOGD("doDataDump: BID:%d -", BID);
}
/*******************************************************************************
 *
 ********************************************************************************/
MVOID
MNRNode::
doInputDataDump(EffectRequestPtr request)
{
    MUINT32 iReqIdx = request->getRequestNo();
}
/*******************************************************************************
 *
 ********************************************************************************/
MVOID
MNRNode::
doOutputDataDump(EffectRequestPtr request, EnquedBufPool* pEnqueBufPool)
{
    MUINT32 iReqIdx = request->getRequestNo();

    if(miTuningDump == 0){
        if(iReqIdx < miDumpStartIdx || iReqIdx >= miDumpStartIdx + miDumpBufSize)
            return;

        FrameInfoPtr framePtr_denoise_final_result = request->vOutputFrameInfo.valueFor(BID_DENOISE_FINAL_RESULT);
        sp<IImageBuffer> frameBuf_denoise_final_result = nullptr;
        framePtr_denoise_final_result->getFrameBuffer(frameBuf_denoise_final_result);
        doDataDump(
                frameBuf_denoise_final_result.get(),
                BID_DENOISE_AND_MNR_OUT,
                iReqIdx
            );
    }else{
        MY_LOGD("do tuning dump for req(%d) inputs", iReqIdx);
        FrameInfoPtr framePtr_denoise_final_result = request->vOutputFrameInfo.valueFor(BID_DENOISE_FINAL_RESULT);
        sp<IImageBuffer> frameBuf_denoise_final_result = nullptr;
        framePtr_denoise_final_result->getFrameBuffer(frameBuf_denoise_final_result);
        doDataDump(
                frameBuf_denoise_final_result.get(),
                BID_DENOISE_AND_MNR_OUT,
                iReqIdx
            );
    }
}
/*******************************************************************************
 *
 ********************************************************************************/
MBOOL
MNRNode::
doMultiPassNR(EffectRequestPtr request)
{
    if(miEnableMNR != 1){
        MY_LOGD("skip MNR");
        return MTRUE;
    }

    CAM_TRACE_NAME("MNRNode::doMultiPassNR");
    MY_LOGD("+, reqID=%d, %d", request->getRequestNo());

    // input metadata
    FrameInfoPtr framePtr_inAppMeta = request->vInputFrameInfo.valueFor(BID_META_IN_APP);
    FrameInfoPtr framePtr_inHalMeta_main1 = request->vInputFrameInfo.valueFor(BID_META_IN_HAL);

    if(framePtr_inAppMeta == nullptr){
        MY_LOGE("framePtr_inAppMeta == nullptr!");
        return MFALSE;
    }

    if(framePtr_inHalMeta_main1 == nullptr){
        MY_LOGE("framePtr_inHalMeta_main1 == nullptr!");
        return MFALSE;
    }

    IMetadata* pMeta_InApp = getMetadataFromFrameInfoPtr(framePtr_inAppMeta);
    IMetadata* pMeta_InHal_main1 = getMetadataFromFrameInfoPtr(framePtr_inHalMeta_main1);

    if(pMeta_InApp == nullptr){
        MY_LOGE("pMeta_InApp == nullptr!");
        return MFALSE;
    }

    if(pMeta_InHal_main1 == nullptr){
        MY_LOGE("pMeta_InHal_main1 == nullptr!");
        return MFALSE;
    }

    // output buffer
    FrameInfoPtr framePtr_denoise_final_result = request->vOutputFrameInfo.valueFor(BID_DENOISE_FINAL_RESULT);
    sp<IImageBuffer> frameBuf_denoise_final_result = nullptr;
    framePtr_denoise_final_result->getFrameBuffer(frameBuf_denoise_final_result);
    if(frameBuf_denoise_final_result == nullptr){
        MY_LOGE("frameBuf_denoise_final_result == nullptr!");
        return MFALSE;
    }

    // tuning buffer
    SmartTuningBuffer tuningBuf_main1 = mBufPoolSet.mpTuningBuffer_bufPool->request();

    if(tuningBuf_main1 == nullptr){
        MY_LOGE("tuningBuf_main1 == nullptr!");
        return MFALSE;
    }

    // setup ISP profile to be MNR
    TuningParam rTuningParam_main1;
    {
        trySetMetadata<MUINT8>(pMeta_InHal_main1, MTK_3A_ISP_PROFILE, NSIspTuning::EIspProfile_Capture_MultiPass_ANR_1);
        ISPTuningConfig ispConfig = {framePtr_inAppMeta, framePtr_inHalMeta_main1, mp3AHal_Main1, MFALSE};
        rTuningParam_main1 = applyISPTuning(tuningBuf_main1, ispConfig);
    }

    // prepare input buffer
    SmartImageBuffer pImgInput = mBufPoolSet.mpMNR_Input_bufPool->request();
    if(pImgInput == nullptr){
        MY_LOGE("pImgInput == nullptr!");
        return MFALSE;
    }

    {
        Timer localTimer;
        localTimer.start();

         // convert format
        if (!formatConverter(frameBuf_denoise_final_result.get(), pImgInput->mImageBuffer.get())) {
            MY_LOGE("MNR format convert failed");
            return MFALSE;
        }

        localTimer.stop();
        VSDOF_LOGD("BMDeNoise_Profile: featurePipe MNRNode convert input buffer format time(%d ms) reqID=%d",
            localTimer.getElapsed(),
            request->getRequestNo()
        );
    }

    QParams enqueParams, dequeParams;
    {
        int frameGroup = 0;

        enqueParams.mvStreamTag.push_back(ENormalStreamTag_Normal);
        enqueParams.mvSensorIdx.push_back(mSensorIdx_Main1);
        enqueParams.mvTuningData.push_back(tuningBuf_main1->mpVA);

        Input src;
        src.mPortID = PORT_IMGI;
        src.mPortID.group = frameGroup;
        src.mBuffer = pImgInput->mImageBuffer.get();
        enqueParams.mvIn.push_back(src);

        Input LSCIn;
        LSCIn.mPortID = PORT_DEPI;
        LSCIn.mPortID.group = frameGroup;
        LSCIn.mBuffer = static_cast<IImageBuffer*>(rTuningParam_main1.pLsc2Buf);
        enqueParams.mvIn.push_back(LSCIn);

        MCrpRsInfo crop1, crop2, crop3;
        // CRZ
        crop1.mGroupID = 1;
        crop1.mFrameGroup = frameGroup;
        crop1.mCropRect.p_fractional.x=0;
        crop1.mCropRect.p_fractional.y=0;
        crop1.mCropRect.p_integral.x=0;
        crop1.mCropRect.p_integral.y=0;
        crop1.mCropRect.s.w=src.mBuffer->getImgSize().w;
        crop1.mCropRect.s.h=src.mBuffer->getImgSize().h;
        crop1.mResizeDst.w=src.mBuffer->getImgSize().w;
        crop1.mResizeDst.h=src.mBuffer->getImgSize().h;
        enqueParams.mvCropRsInfo.push_back(crop1);

        // WROT
        crop3.mGroupID = 3;
        crop3.mFrameGroup = frameGroup;
        crop3.mCropRect.p_fractional.x=0;
        crop3.mCropRect.p_fractional.y=0;
        crop3.mCropRect.p_integral.x=0;
        crop3.mCropRect.p_integral.y=0;
        crop3.mCropRect.s.w=src.mBuffer->getImgSize().w;
        crop3.mCropRect.s.h=src.mBuffer->getImgSize().h;
        crop3.mResizeDst.w=src.mBuffer->getImgSize().w;
        crop3.mResizeDst.h=src.mBuffer->getImgSize().h;
        enqueParams.mvCropRsInfo.push_back(crop3);

        Output out;
        out.mPortID = PORT_WROT;
        out.mPortID.group = frameGroup;
        out.mBuffer = frameBuf_denoise_final_result.get();
        out.mTransform = 0;
        enqueParams.mvOut.push_back(out);
    }

    enqueParams.mpfnCallback = NULL;
    enqueParams.mpfnEnQFailCallback = NULL;
    enqueParams.mpCookie = NULL;

    MY_LOGD("mpINormalStream enque start! reqID=%d", request->getRequestNo());
    CAM_TRACE_BEGIN("MNRNode::NormalStream");
    Timer localTimer;
    localTimer.start();

    if(!mpINormalStream->enque(enqueParams))
    {
        MY_LOGE("mpINormalStream enque failed! reqID=%d", request->getRequestNo());
        return UNKNOWN_ERROR;
    }

    if(!mpINormalStream->deque(dequeParams))
    {
        MY_LOGE("mpINormalStream deque failed! reqID=%d", request->getRequestNo());
        return UNKNOWN_ERROR;
    }

    CAM_TRACE_END();
    MY_LOGD("mpINormalStream deque end! reqID=%d", request->getRequestNo());
    localTimer.stop();
    VSDOF_LOGD("BMDeNoise_Profile: featurePipe MNRNode doMultiPassNR time(%d ms) reqID=%d",
        localTimer.getElapsed(),
        request->getRequestNo()
    );

    MY_LOGD("-, reqID=%d", request->getRequestNo());
    return MTRUE;
}
/*******************************************************************************
 *
 ********************************************************************************/
TuningParam
MNRNode::
applyISPTuning(
        SmartTuningBuffer& targetTuningBuf,
        const ISPTuningConfig& ispConfig,
        MBOOL useDefault
)
{
    CAM_TRACE_NAME("MNRNode::applyISPTuning");
    VSDOF_LOGD("+, reqID=%d bIsResized=%d", ispConfig.pInAppMetaFrame->getRequestNo(), ispConfig.bInputResizeRaw);

    TuningParam tuningParam = {NULL, NULL};
    tuningParam.pRegBuf = reinterpret_cast<void*>(targetTuningBuf->mpVA);

    MetaSet_T inMetaSet;
    IMetadata* pMeta_InApp  = getMetadataFromFrameInfoPtr(ispConfig.pInAppMetaFrame);
    IMetadata* pMeta_InHal  = getMetadataFromFrameInfoPtr(ispConfig.pInHalMetaFrame);

    inMetaSet.appMeta = *pMeta_InApp;
    inMetaSet.halMeta = *pMeta_InHal;

    // USE resize raw-->set PGN 0
    if(ispConfig.bInputResizeRaw)
        updateEntry<MUINT8>(&(inMetaSet.halMeta), MTK_3A_PGN_ENABLE, 0);
    else
        updateEntry<MUINT8>(&(inMetaSet.halMeta), MTK_3A_PGN_ENABLE, 1);

    if(useDefault){
        MY_LOGD("Test mode, use default tuning");
        SetDefaultTuning((dip_x_reg_t*)tuningParam.pRegBuf, (MUINT32*)tuningParam.pRegBuf, tuning_tag_G2G, 0);
        SetDefaultTuning((dip_x_reg_t*)tuningParam.pRegBuf, (MUINT32*)tuningParam.pRegBuf, tuning_tag_G2C, 0);
        SetDefaultTuning((dip_x_reg_t*)tuningParam.pRegBuf, (MUINT32*)tuningParam.pRegBuf, tuning_tag_GGM, 0);
        SetDefaultTuning((dip_x_reg_t*)tuningParam.pRegBuf, (MUINT32*)tuningParam.pRegBuf, tuning_tag_UDM, 0);
    }else{
        MetaSet_T resultMeta;
        ispConfig.p3AHAL->setIsp(0, inMetaSet, &tuningParam, &resultMeta);

        // write ISP resultMeta to input hal Meta
        // (*pMeta_InHal) += resultMeta.halMeta;
    }

    VSDOF_LOGD("-, reqID=%d", ispConfig.pInAppMetaFrame->getRequestNo());
    return tuningParam;
}
/*******************************************************************************
 *
 ********************************************************************************/
MBOOL
MNRNode::
onThreadStart()
{
    return MTRUE;
}
/*******************************************************************************
 *
 ********************************************************************************/
MBOOL
MNRNode::
onThreadStop()
{
    return MTRUE;
}
/*******************************************************************************
 *
 ********************************************************************************/
MBOOL
MNRNode::
onThreadLoop()
{
    FUNC_START;
    EffectRequestPtr effectRequest = nullptr;

    if( !waitAllQueue() )// block until queue ready, or flush() breaks the blocking state too.
    {
        return MFALSE;
    }
    if( !mRequests.deque(effectRequest) )
    {
        MY_LOGD("mRequests.deque() failed");
        return MFALSE;
    }

    CAM_TRACE_NAME("MNRNode::onThreadLoop");

    // get request type
    const sp<EffectParameter> params = effectRequest->getRequestParameter();
    MINT32 requestType = params->getInt(BMDENOISE_REQUEST_TYPE_KEY);

    // doInputDataDump(effectRequest);

    MY_LOGD("MNRNode get requestType:%d", requestType);

    if(!doMultiPassNR(effectRequest)){
        MY_LOGE("do MultiPassNR failed, please check error msgs!");
    }

    doOutputDataDump(effectRequest, nullptr);

    handleData(MNR_RESULT, effectRequest);

    FUNC_END;
    return MTRUE;
}