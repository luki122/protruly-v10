#include "stdio.h"
#include "signal.h"
#include "sensors.h"
#include "sensor_manager.h"
#include "init_learning.h"
#include "feature_setting.h"
#include "feature_struct.h"
#include "extract_feature.h"
#include "algo_adaptor.h"
#include "activity_algorithm.h"
#include "stationary.h"
#include "context_setting.h"

#ifdef _PC_VERSION_
#define LOGE(fmt, args...)    printf("[Stationary] ERR: "fmt, ##args)
#define LOGD(fmt, args...)    printf("[Stationary] DBG: "fmt, ##args)
#else
#define LOGE(fmt, args...)    PRINTF_D("[Stationary] ERR: "fmt, ##args)
#define LOGD(fmt, args...)    PRINTF_D("[Stationary] DBG: "fmt, ##args)
#endif

//#define _STATIONARY_DEBUG_

#define STATIONARY_VARIANCE_THRESHOLD       2000    // average variance of Acc (unit: 10^5 * m/s^2)
#define TIME_TO_STATIONARY                  3000    // timer for NONSTATIONARY -> STATIONARY (unit: ms)
#define TIME_TO_NONSTATIONARY               8000    // timer for STATIONARY -> NONSTATIONARY (unit: ms)

#define GRAVITY_COS15_SECTION               29400   // 9.8 (Gravity) * 0.03 (1 - cos 15 degress) * 10^5 [Note: need concern sensor bias]
#define GRAVITY_COS30_SECTION               131300  // 9.8 (Gravity) * 0.13 (1 - cos 30 degress) * 10^5 [Note: need concern sensor bias]

struct input_list_t input_comp_acc;
static resampling_t stationary_resampling = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

struct stationary_adaptor_t {
    uint64_t time_stamp_ns;     // unit: ns
    uint32_t last_set_time;     // unit: ms
    int state;
};
static struct stationary_adaptor_t stationary_adaptor;
static int start_notify_stationary = 0;

static INT32 run_stationary(struct data_t *const output)
{
    output->data_exist_count = 1;
    output->data->time_stamp = stationary_adaptor.time_stamp_ns;
    output->data->sensor_type = SENSOR_TYPE_STATIONARY_GESTURE;
    output->data->stationary_event.state = get_stationary_result();
    return 1;
}

static void run_stationary_algorithm(uint32_t input_time_stamp_ms)
{
    if (stationary_adaptor.state == INPUT_STATE_INIT) {
        stationary_adaptor.last_set_time = input_time_stamp_ms;
        stationary_adaptor.state = INPUT_STATE_PREPARE_DATA;
    }

    INT32 time_diff;
    time_diff = input_time_stamp_ms - stationary_adaptor.last_set_time;
    if (((stationary_adaptor.state == INPUT_STATE_PREPARE_DATA) && (time_diff >= MIN_SAMPLES_TIME)) || \
            ((stationary_adaptor.state == INPUT_STATE_UPDATE) && (time_diff >= STATIONARY_FEATURE_UPDATE_TIME))) {
        stationary_adaptor.state = INPUT_STATE_UPDATE;
        stationary_adaptor.last_set_time = input_time_stamp_ms;
        stationary_set_ts(input_time_stamp_ms);
        stationary_alg_enter_point();

        if (start_notify_stationary && get_stationary_notify() && get_device_flat()) {
            sensor_subsys_algorithm_notify(SENSOR_TYPE_STATIONARY_GESTURE);
            PRINTF_D("[Stationary] stationary + flat at %d", input_time_stamp_ms);
        } else if (start_notify_stationary && get_stationary_notify()) {
            PRINTF_D("[Stationary] stationary - flat at %d", input_time_stamp_ms);
        }
    }
}

static INT32 set_stationary_data(const struct data_t *input_list, void *reserve)
{
    struct data_unit_t *data_start = input_list->data;
    uint32_t input_time_stamp_ms = data_start->time_stamp / 1000000;
    stationary_adaptor.time_stamp_ns = data_start->time_stamp;

    int count = input_list->data_exist_count;
    if (data_start->sensor_type == SENSOR_TYPE_ACCELEROMETER) {
        if (!stationary_resampling.init_flag) {
            stationary_resampling.last_time_stamp = input_time_stamp_ms;
            stationary_resampling.init_flag = 1;
            return 0;
        }

        Activity_Accelerometer_Ms2_Data_t acc_input = {0};
        while (count != 0) {
#ifdef _STATIONARY_DEBUG_
            PRINTF_D("[Stationary_Acc_I]%u, %d, %d, %d, %d\n", input_time_stamp_ms, count, \
                     data_start->accelerometer_t.x, data_start->accelerometer_t.y, data_start->accelerometer_t.z);
#endif
            input_time_stamp_ms = data_start->time_stamp / 1000000;
            sensor_vec_t *data_acc_t = &data_start->accelerometer_t;

            stationary_resampling.current_time_stamp = input_time_stamp_ms;
            sensor_subsys_algorithm_resampling_type(&stationary_resampling);

            acc_input.time_stamp = stationary_resampling.last_time_stamp;
            acc_input.x = data_acc_t->x;
            acc_input.y = data_acc_t->y;
            acc_input.z = data_acc_t->z;

            while (stationary_resampling.input_count > 0) {
#ifdef _STATIONARY_DEBUG_
                PRINTF_D("[Stationary_Alg_I]%u, %u, %d, %d, %d\n", acc_input.time_stamp, stationary_resampling.last_time_stamp, \
                         data_acc_t->x, data_acc_t->y, data_acc_t->z);
#endif
                acc_input.time_stamp = acc_input.time_stamp + stationary_resampling.input_sample_delay;
                get_signal_acc(acc_input); /* using address acc_buff to update acc measurements */
                run_stationary_algorithm(acc_input.time_stamp);
                stationary_resampling.input_count--;
                stationary_resampling.last_time_stamp = stationary_resampling.current_time_stamp;
            }
            data_start++;
            count--;
        }
    } else {
        return 0;
    }
    return 1;
}

static INT32 stationary_operate(Sensor_Command command, void* buffer_in, INT32 size_in, \
                                void* buffer_out, INT32 size_out)
{
    int err = 0;
    int value = 0;
    if (NULL == buffer_in) {
        return -1;
    }
    switch (command) {
        case ACTIVATE:
            if ((buffer_in == NULL) || (size_in < sizeof(int))) {
                err = -1;
            } else {
                value = *(int *)buffer_in;
                if (0 == value) {
                    stationary_original_stop();
                    start_notify_stationary = 0;
                } else {
                    stationary_original_init(STATIONARY_VARIANCE_THRESHOLD, TIME_TO_STATIONARY, TIME_TO_NONSTATIONARY,
                                             GRAVITY_COS30_SECTION);
                    start_notify_stationary = 1;
                }
            }
            break;
        default:
            break;
    }
    PRINTF_D("[Stationary] start_notify_stationary = %d", start_notify_stationary);
    return err;
}

static int stationary_register(void)
{
    int ret = 0;
    stationary_adaptor.time_stamp_ns = 0;
    stationary_adaptor.last_set_time = 0;
    stationary_adaptor.state = INPUT_STATE_INIT;

    input_comp_acc.input_type = SENSOR_TYPE_ACCELEROMETER;
    input_comp_acc.sampling_delay = STATIONARY_INPUT_SAMPLE_DELAY * ACC_EVENT_COUNT_PER_FIFO_LOOP;
    input_comp_acc.next_input = NULL;

    struct SensorDescriptor_t stationary_desp = {
        SENSOR_TYPE_STATIONARY_GESTURE, 1, one_shot, {20, 0},
        &input_comp_acc, stationary_operate, run_stationary,
        set_stationary_data, STATIONARY_INPUT_ACCUMULATE
    };

    ret = sensor_subsys_algorithm_register_type(&stationary_desp);
    if (ret < 0)
        LOGE("fail to register Stationary check \r\n");

    ret = sensor_subsys_algorithm_register_data_buffer(SENSOR_TYPE_STATIONARY_GESTURE, 1);
    if (ret < 0)
        LOGE("fail to register buffer for Stationary \r\n");
    return ret;
}

int stationary_init(void)
{
    uint32_t init_time = read_xgpt_stamp_ns() / 1000000;
    stationary_set_ts(init_time);
    stationary_original_init(STATIONARY_VARIANCE_THRESHOLD, TIME_TO_STATIONARY, TIME_TO_NONSTATIONARY,
                             GRAVITY_COS30_SECTION);

    stationary_resampling.init_flag = 0;
    stationary_resampling.input_sample_delay = STATIONARY_INPUT_SAMPLE_DELAY;

    stationary_algorithm_init(init_time);
    stationary_register();
    return 1;
}
#ifdef _EVEREST_MODULE_DECLARE_
MODULE_DECLARE(virt_stationary_init, MOD_VIRT_SENSOR, stationary_init);
#endif
