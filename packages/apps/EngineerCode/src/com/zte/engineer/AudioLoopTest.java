
package com.zte.engineer;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.AudioSystem;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.view.WindowManager;
import android.view.KeyEvent;
import android.provider.Settings;
import android.view.View.OnClickListener;
import android.os.Handler;
import android.os.Message;
import android.Manifest;
import android.content.pm.PackageManager;
//import com.mediatek.audioprofile.AudioProfile;
//import com.mediatek.audioprofile.AudioProfileImpl;
//import com.mediatek.audioprofile.AudioProfileManagerImpl;

public class AudioLoopTest extends ZteActivity {
    /*
     * Define some aliases to make these debugging flags easier to refer to.
     */
    private final static String LOGTAG = "ZTEAudioLoopTest";

    private int isHeadsetConnect;
    private AudioManager mAudioManager = null;
    // private AudioProfileImpl mProfile;
    private boolean soundeffect = false;
    private boolean running = false;
	private boolean isRecord = true;
	private AudioRecAndPlay mAudioControl;
	private Button recordAndPlayBtn;
	private static final int startRecord = 103, stopAndPlay = 104;
	

	
    public void onCreate(Bundle savedInstanceState) {
        // Turn screen on and show above the keyguard for emergency alert
        getWindow().addFlags(
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                        | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                        | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        super.onCreate(savedInstanceState);
        // mProfile =
        // (AudioProfileImpl)AudioProfileManagerImpl.getInstance(this).getActiveProfile();
        // soundeffect = mProfile.getSoundEffectEnabled();
        // if(soundeffect == true)
        // {
        // mProfile.setSoundEffectEnabled(false);
        // }
        // hide title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.singlebuttonview);

        TextView mTextView = (TextView) findViewById(R.id.singlebutton_textview);
        mTextView.setText(R.string.audio_loop);
		
		recordAndPlayBtn = (Button)findViewById(R.id.singlebutton_play_button);
		recordAndPlayBtn.setOnClickListener(recordAndPlayListener);
		recordAndPlayBtn.setVisibility(View.VISIBLE);
		if(checkSelfPermission(Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED){
			requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, 1000);
		} else{
			mAudioControl = new AudioRecAndPlay(this, AudioRecAndPlay.MODE_MAIN_MIC);
		}

        mAudioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);

        ((Button) findViewById(R.id.btnPass)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnFail)).setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
		Settings.System.putInt(getContentResolver(), "AEON_KEY_TEST", 1);
        /*running = true;
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (java.lang.InterruptedException e) {
                }
                if (running) {
                    if (soundeffect == true) {
                        // mProfile.setSoundEffectEnabled(false);
                    }
                    mAudioManager.setParameters("SET_LOOPBACK_TYPE=21,1");
                }
            }
        }.start();*/
    }

    @Override
    public void onPause() {
        super.onPause();
		Settings.System.putInt(getContentResolver(), "AEON_KEY_TEST", 0);
		if (mAudioControl != null) {
			mAudioControl.stopRecord();
			mAudioControl.stopPlay();
		}
        /*running = false;
        mAudioManager.setParameters("SET_LOOPBACK_TYPE=0");*/
        // mProfile.setSoundEffectEnabled(soundeffect);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //running = false;
        //mAudioManager.setParameters("SET_LOOPBACK_TYPE=0");
        // mProfile.setSoundEffectEnabled(soundeffect);
    }

    @Override
    public void finishSelf(int result) {
        //running = false;
        //mAudioManager.setParameters("SET_LOOPBACK_TYPE=0");
        // mProfile.setSoundEffectEnabled(soundeffect);
        super.finishSelf(result);
    }

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mAudioControl != null) {
			mAudioControl.destroy();
		}
	}
	
	OnClickListener recordAndPlayListener = new OnClickListener(){
		 @Override
         public void onClick(View arg0) {
			 if(isRecord){
				 recordAndPlayBtn.setText(R.string.play_music);
				 Message msg = mHandler.obtainMessage();
			     msg.what = startRecord;
			     mHandler.sendMessageDelayed(msg, 10);
				 isRecord = false;
			 } else{
				 recordAndPlayBtn.setText(R.string.record_music);
				 Message msg = mHandler.obtainMessage();
			     msg.what = stopAndPlay;
			     mHandler.sendMessageDelayed(msg, 10);
				 isRecord = true;
			 }
		 }
	};
	
	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case startRecord:
			    if(mAudioControl != null)
					mAudioControl.stopPlay();
				    mAudioControl.startRecord();
				break;
			case stopAndPlay:
				if (mAudioControl != null) {
					mAudioControl.stopRecord();
					mAudioControl.startPlay();
				}
				break;
			}

		};
	};
}
