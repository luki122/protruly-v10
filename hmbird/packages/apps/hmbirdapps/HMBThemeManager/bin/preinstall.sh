#!/system/bin/sh

Hmbird="/data/cust"
Hmbird_Ex="/data/cust/exist"
VR_SDCARD="/sdcard/DCIM/CameraVr"

CopyFlag=0

if [ ! -d ${Hmbird_Ex} ]; then
        CopyFlag=1
fi


while true; do

    CurrentYear=`date +%Y`

    Cust_Ex="/sdcard/try"

    if [ ! -d ${Cust_Ex} ]; then
        echo "Mkdir Cust_Ex. "
        mkdir -p ${Cust_Ex}
        sleep 1
        continue
    fi

    rm -rf ${Cust_Ex}

    echo "Copy Target Source"

    if [ -d ${Hmbird} ]; then

        if [ $CopyFlag -eq 0 ]; then
             echo "CopyFlag Done."
             break;
        fi
    fi

    mkdir ${Hmbird}
    mkdir ${Hmbird}/xml
    mkdir -p ${VR_SDCARD}

    chmod 755 ${Hmbird}
    chmod 755 ${Hmbird}/xml 
    chmod 755 ${VR_SDCARD}

    cp system/hummingbird/config/browser_config.xml /data/cust/xml/
    cp system/backup/vrcamera/* ${VR_SDCARD}

    mkdir -p ${Hmbird_Ex}

    CopyFlag=0

    sync

done
