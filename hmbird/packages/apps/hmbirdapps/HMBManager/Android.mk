LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := HMBManager
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

LOCAL_MULTILIB := 64

LOCAL_PREBUILT_JNI_LIBS :=  \
        lib/arm64/libams-1.2.5-mfr.so \
        lib/arm64/libdce-1.1.15-mfr.so \
        lib/arm64/libTmsdk-2.0.10-mfr.so \
        lib/arm64/libbuffalo-1.0.0-mfr.so \
        lib/arm64/libbumblebee-1.0.4-mfr.so


include $(BUILD_PREBUILT)
