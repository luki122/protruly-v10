
LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

ifeq ($(PLATFORM_SDK_VERSION),21)
  $(shell cp -af $(LOCAL_PATH)/overlay/L/SDcardTest.java $(LOCAL_PATH)/src/com/zte/engineer/SDcardTest.java)
else
  ifeq ($(PLATFORM_SDK_VERSION),22)
    $(shell cp -af $(LOCAL_PATH)/overlay/L/SDcardTest.java $(LOCAL_PATH)/src/com/zte/engineer/SDcardTest.java)
  else
    ifeq ($(PLATFORM_SDK_VERSION),23)
      $(shell cp -af $(LOCAL_PATH)/overlay/M/SDcardTest.java $(LOCAL_PATH)/src/com/zte/engineer/SDcardTest.java)
    endif
  endif
endif

LOCAL_SRC_FILES := $(call all-java-files-under, src)

LOCAL_PACKAGE_NAME := EngineerCode
LOCAL_CERTIFICATE := platform
LOCAL_PRIVILEGED_MODULE := true
LOCAL_JNI_SHARED_LIBRARIES := libcom_zte_engineer_jni libcom_zte_engineer_jni_temp libcom_example_tempreture_jni
LOCAL_JAVA_LIBRARIES += mediatek-framework telephony-common

# For IC DC6010
ifeq ($(strip $(MTK_IRTX_SUPPORT)),yes)
    LOCAL_REQUIRED_MODULES := IRTest
else
    ifeq ($(strip $(CUSTOM_IR_DC6010)), yes)
        LOCAL_REQUIRED_MODULES := IRTest
    endif
endif

LOCAL_DEX_PREOPT := false
include $(BUILD_PACKAGE)

# Use the folloing include to make our test apk.
include $(call all-makefiles-under,$(LOCAL_PATH))
