
package com.zte.engineer;

import android.app.Activity;
import android.content.Context;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Vector;
import android.util.Log;

public class DevInfoActivity extends Activity {
    private static final String TAG = "DevInfoActivity";

    public static final String PROC_PATH_ALPS = "/proc/AEON_ALPS";
    public static final String PROC_PATH_MAGNETOMETER
        = "/proc/AEON_MAGNETOMETER";
    public static final String PROC_PATH_ACCELEROMETER
        = "/proc/AEON_ACCELEROMETER";
    public static final String PROC_PATH_LCM = "/proc/AEON_LCM";
    public static final String PROC_PATH_TPD = "/proc/AEON_TPD";
    public static final String PROC_PATH_TP_FW = "/proc/AEON_TP_FW";
    public static final String PROC_PATH_CAMERA0 = "/proc/AEON_CAMERA0";
    public static final String PROC_PATH_CAMERA1 = "/proc/AEON_CAMERA1";
    public static final String PROC_PATH_CAMERA0_TUNING_VERSION
        = "/proc/AEON_CAMERA0_TUNING_VERSION";
    public static final String PROC_PATH_CAMERA1_TUNING_VERSION
        = "/proc/AEON_CAMERA1_TUNING_VERSION";

    private TextView mTextViewDeviceInfo = null;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.devinfo_layout);

        mTextViewDeviceInfo = (TextView) findViewById(R.id.showTextView_DeviceInfo);

        String mTextProcAlps = Util.getDeviceProc(PROC_PATH_ALPS);
        String mTextProcMsensor = Util.getDeviceProc(PROC_PATH_MAGNETOMETER);
        String mTextProcGsensor = Util.getDeviceProc(PROC_PATH_ACCELEROMETER);
        String mTextProcLcm = Util.getDeviceProc(PROC_PATH_LCM);
        String mTextProcTpd = Util.getDeviceProc(PROC_PATH_TPD);
        String mTextProcTpdFW = Util.getDeviceProc(PROC_PATH_TP_FW);
        String mTextProcCamera0 = Util.getDeviceProc(PROC_PATH_CAMERA0);
        String mTextProcCamera1 = Util.getDeviceProc(PROC_PATH_CAMERA1);
        String mTextProcCamera0Tuning = Util.getDeviceProc(PROC_PATH_CAMERA0_TUNING_VERSION);
        String mTextProcCamera1Tuning = Util.getDeviceProc(PROC_PATH_CAMERA1_TUNING_VERSION);

        mTextViewDeviceInfo.setSingleLine(false);

        final String separator = System.getProperty("line.separator");

        StringBuffer strDeviceInfo = new StringBuffer();

        strDeviceInfo.append("LCD: ").append(mTextProcLcm).append(separator);
        strDeviceInfo.append("TP: ").append(mTextProcTpd).append(separator);
        strDeviceInfo.append("TP_FW: ").append(mTextProcTpdFW).append(separator);
        strDeviceInfo.append("ALSPS: ").append(mTextProcAlps).append(separator);
        strDeviceInfo.append("ACCELEROMETER: ").append(mTextProcGsensor).append(separator);
        strDeviceInfo.append("MAGNETOMETER: ").append(mTextProcMsensor).append(separator);
        strDeviceInfo.append("CAMERA0: ").append(mTextProcCamera0).append(separator);
        strDeviceInfo.append("Tuning Version: ").append(mTextProcCamera0Tuning).append(separator);
        strDeviceInfo.append("CAMERA1: ").append(mTextProcCamera1).append(separator);
        strDeviceInfo.append("Tuning Version: ").append(mTextProcCamera1Tuning).append(separator);

        mTextViewDeviceInfo.setText(strDeviceInfo);
    }

}
