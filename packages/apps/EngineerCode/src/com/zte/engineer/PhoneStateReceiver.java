
package com.zte.engineer;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;

public class PhoneStateReceiver extends BroadcastReceiver {

    private static final String TAG = "yuanwenchao";

    private TelephonyManager telephonyManager = null;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "PhoneStateChange");
        telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (TelephonyManager.CALL_STATE_OFFHOOK == telephonyManager.getCallState()) {
            Log.d(TAG, "offhook");
        } else if (TelephonyManager.CALL_STATE_RINGING == telephonyManager.getCallState()) {
            Log.d(TAG, "ringing");
        } else if (TelephonyManager.CALL_STATE_IDLE == telephonyManager.getCallState()) {
            // context.startService(new Intent(SERVICE_ACTION));

            Intent intents = new Intent();
            intents.setAction("Intent.receiver_call_status");
            context.sendBroadcast(intents);

            Log.d(TAG, "idle");
        }
    }
}
